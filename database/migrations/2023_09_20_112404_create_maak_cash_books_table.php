<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaakCashBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maak_cash_books', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('accounts_code')->nullable();
            $table->string('accounts_descriptoin')->nullable();
            $table->string('voucher_number');
            $table->double('cash_debit')->nullable();
            $table->double('cash_credit')->nullable();
            $table->string('bank_name')->nullable();
            $table->double('bank_debit')->nullable();
            $table->double('bank_credit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maak_cash_books');
    }
}