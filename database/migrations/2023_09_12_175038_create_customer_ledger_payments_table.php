<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerLedgerPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_ledger_payments', function (Blueprint $table) {
            $table->id();
            $table->string('payment_date');
            $table->integer('customer_id');
            $table->string('payment_type');
            $table->string('bank_name')->nullable();
            $table->string('check_number')->nullable();
            $table->integer('payment_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_ledger_payments');
    }
}
