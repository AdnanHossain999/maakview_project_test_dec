<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductWarrentiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_warrenties', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id');
            $table->string('product_serial');
            $table->string('problem_details');
            $table->string('comments')->nullable();
            $table->string('warrenty_numbers');
            $table->integer('purchase_details_id');
            $table->integer('order_id');
            $table->integer('new_product_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_warrenties');
    }
}