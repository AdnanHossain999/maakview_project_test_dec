<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaakviewAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maakview_assets', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->double('current_hand_cash')->default(0);
            $table->string('accounts_receivable_source')->nullable();
            $table->double('accounts_receivable')->nullable();
            $table->double('fixed_assets')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maakview_assets');
    }
}