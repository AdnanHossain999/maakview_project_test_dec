<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaakviewExpencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maakview_expences', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->double('office_rent')->nullable();
            $table->double('warehouse_rent')->nullable();
            $table->double('office_utilites')->nullable();
            $table->double('marketing_online')->nullable();
            $table->double('advertisement')->nullable();
            $table->double('business_promotion')->nullable();
            $table->double('ip_phone_bill')->nullable();
            $table->double('mobile_bill')->nullable();
            $table->double('festibal_allowance')->nullable();
            $table->double('bridge_toll_and_parking')->nullable();
            $table->double('gift_and_donation')->nullable();
            $table->double('electric_goods')->nullable();
            $table->double('entertainment_expenses')->nullable();
            $table->double('fuel_oil')->nullable();
            $table->double('inernet_and_telephone_line')->nullable();
            $table->double('license_and_fees')->nullable();
            $table->double('medical_expenses')->nullable();
            $table->double('office_maintenance')->nullable();
            $table->double('printing_expenses')->nullable();
            $table->double('statinonary_expneses')->nullable();
            $table->double('electricity_bill')->nullable();
            $table->double('repair_and_maintenance_others')->nullable();
            $table->double('accessories_expenses')->nullable();
            $table->double('carring_expenses')->nullable();
            $table->double('product_damage_and_lost_expenses')->nullable();
            $table->double('fine_and_penalty')->nullable();
            $table->double('load_and_unload_labour_bill')->nullable();
            $table->double('lunch_tiffin')->nullable();
            $table->double('postage_and_courier')->nullable();
            $table->double('employee_loan_and_other_payments')->nullable();
            $table->double('legal_cost')->nullable();
            $table->double('monthly_incentive_payments')->nullable();
            $table->double('website_expenses')->nullable();
            $table->double('bank_interest_all')->nullable();
            $table->double('miscellaneous_expenses')->nullable();
            $table->double('vat_tax')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maakview_expences');
    }
}
