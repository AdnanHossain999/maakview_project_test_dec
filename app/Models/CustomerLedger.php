<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerLedger extends Model
{
    use HasFactory;

    public function combinedOrder()
    {
        return $this->belongsTo(CombinedOrder::class, 'combined_order_id', 'id');
    }

    
}