<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DemoChallan extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'customer_name',
        'customer_phone',
        'date',
        'product_name',
        'product_serial',
        'product_quantity',
       
    ];


    public function products()
    {
        return $this->hasMany(Product::class);
    }
}