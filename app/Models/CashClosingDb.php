<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashClosingDb extends Model
{
    use HasFactory;

    public function cashClosingLocation()
    {
        return $this->belongsTo(CashClosingLocation::class)->withTrashed();
    }
}