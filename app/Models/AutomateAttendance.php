<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutomateAttendance extends Model
{
    use HasFactory;

      // added bu Bijoy

      public function payslip()
      {
          return $this->hasOne(Payslip::class, 'automated_user_id', 'user_id');
      }
}