<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaakviewExpences extends Model
{
    use HasFactory;

    protected $fillable = [
        'date', 'office_rent', 'warehouse_rent', 'office_utilites', 'marketing_online',
        'advertisement', 'business_promotion', 'ip_phone_bill', 'mobile_bill',
        'festibal_allowance', 'bridge_toll_and_parking', 'gift_and_donation', 'electric_goods',
        'entertainment_expenses', 'fuel_oil', 'inernet_and_telephone_line', 'license_and_fees',
        'medical_expenses', 'office_maintenance', 'printing_expenses', 'statinonary_expneses',
        'electricity_bill', 'repair_and_maintenance_others', 'accessories_expenses',
        'carring_expenses', 'product_damage_and_lost_expenses', 'fine_and_penalty',
        'load_and_unload_labour_bill', 'lunch_tiffin', 'postage_and_courier',
        'employee_loan_and_other_payments', 'legal_cost', 'monthly_incentive_payments',
        'website_expenses', 'bank_interest_all', 'miscellaneous_expenses', 'vat_tax',
    ];
}
