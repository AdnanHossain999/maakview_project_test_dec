<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DemoMrtChallan extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'customer_name',
        'customer_phone',
        'date',
        // 'product_id',
        'product_name',
        'product_serial',
        'quantity',
        
       
    ];


    public function products()
    {
        return $this->hasMany(Product::class);
    }
}