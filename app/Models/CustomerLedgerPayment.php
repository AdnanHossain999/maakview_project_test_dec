<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerLedgerPayment extends Model
{
    use HasFactory;

    public function purchase_transaction()
    {
        return $this->belongsTo(PurchaseTransaction::class, 'purchase_transaction_id', 'id');
    }

    
}