<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashClosingLocation extends Model
{
    use HasFactory;

    public function maak_cashbook()
    {
        return $this->belongsTo(MaakCashBook::class)->withTrashed();
    }
}