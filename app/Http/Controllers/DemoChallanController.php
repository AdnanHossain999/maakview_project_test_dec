<?php

namespace App\Http\Controllers;

use App\Models\DemoChallan;
use App\Models\DemoMrtChallan;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DemoChallanController extends Controller
{
    public function index()
    {

        return view('backend.demo_challan.index');
    }

    public function view(Request $request)
    {

        $customerName = request('customerName');
        $customerPhone = request('P');
        $selectedDate = request('D');
        $formatted_date = Carbon::parse($selectedDate)->format('j-n-Y');



        $productsParam = $request->input('Pro');
        $products = $productsParam !== null ? json_decode($productsParam, true) : null;

        $totalQuantity = array_sum(array_column($products, 'quantity'));



        foreach ($products as $product) {

            $demoChallan = DemoChallan::create([
                'customer_name' => $customerName,
                'customer_phone' => $customerPhone,
                'date' => $selectedDate,
                'product_name' => $product['productName'],
                'product_serial' => $product['productSerial'],
                'product_quantity' => $product['quantity'],
                // Add other fields as needed
            ]);
        }


        return view('backend.demo_challan.view', compact('customerName', 'customerPhone', 'products', 'formatted_date', 'totalQuantity'));
    }



    public function list()
    {

        $get_all_demo_Challan_receipt = DemoChallan::paginate(1000);



        // dd($get_all_demo_Challan_receipt);
        return view('backend.demo_challan.demo_challan_list', compact('get_all_demo_Challan_receipt'));
    }

    public function delete($id)
    {

        $delete_demo_challan_receipt = DemoChallan::where('id', $id)->delete();
        return redirect()->route('demo_challan.list')->with('delete_status', 'Successfully Deleted Money Receipt');
    }

    // Added by Bijoy

    public function mrt_challan_index()
    {

        return view('backend.demo_challan.mrt_index');
    }

    public function mrt_challan_view(Request $request)
    {

        $customerName = request('customerName');
        $customerPhone = request('P');
        $selectedDate = request('D');
        $formatted_date = Carbon::parse($selectedDate)->format('d-m-Y');



        $productsParam = $request->input('Pro');
        $products = $productsParam !== null ? json_decode($productsParam, true) : null;

        $totalQuantity = array_sum(array_column($products, 'quantity'));



        $products_Name = [];
        $products_Serial = [];
        $products_Quantity = [];

        foreach ($products as $product) {
            $products_Name[] =
                $product['productName'];
            $products_Serial[] =

                $product['productSerial'];
            $products_Quantity[] =

                $product['quantity'];
        }

        $demoChallan = DemoMrtChallan::create([
            'customer_name' => $customerName,
            'customer_phone' => $customerPhone,
            'date' => $selectedDate,
            'product_name' => json_encode($products_Name),
            'product_serial' => json_encode($products_Serial),
            'quantity' => json_encode($products_Quantity),
        ]);


        // dd($products_Quantity);



        return view('backend.demo_challan.mrt_view', compact('customerName', 'customerPhone', 'products', 'formatted_date', 'totalQuantity'));
    }







    public function mrt_challan_list()
    {

        $get_all_mrt_demo_Challan_receipt = DemoMrtChallan::paginate(10000);



        // dd($get_all_mrt_demo_Challan_receipt);
        return view('backend.demo_challan.mrt_list', compact('get_all_mrt_demo_Challan_receipt'));
    }

    public function mrt_challan_print($id)
    {
        $get_single_mrt_challan_print = DemoMrtChallan::findOrFail($id);

        // dd($get_single_mrt_challan_print);

        return view('backend.demo_challan.mrt_view_print', compact('get_single_mrt_challan_print'));
    }

    public function mrt_challan_delete($id)
    {

        $delete_demo_challan_receipt = DemoMrtChallan::where('id', $id)->delete();
        return redirect()->route('mrt_demo_challan.list')->with('delete_status', 'Successfully Deleted Money Receipt');
    }
}