<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Supplier;
use App\Models\PurchaseDetail;
use App\Models\AutomateAttendance;
use App\Models\MaakviewExpences;
use App\Models\MaakviewAsset;
use App\Models\AccountsLiabilities;
use App\Models\AccountsPaymentLiablity;
use App\Models\AccountsReceiptLiablity;
use App\Models\CashClosingDb;
use App\Models\CashClosingLocation;
use App\Models\CashOpeningDb;
use App\Models\MaakCashBook;
use App\Models\MaakviewExpensesNew;
use App\Models\MaakviewIncome;
use App\Models\MaakviewPaymentLoan;
use App\Models\MaakviewReceiptLoan;
use DB;
use Carbon\Carbon;
use Log;
use Session;

class AccountsController extends Controller
{
    public function get_maakview_ledger_receipts_home()
    {
        return view('backend.accounts.accounts_ledger_receipts_home');
    }

    public function get_maakview_ledger_payments_home()
    {
        return view('backend.accounts.accounts_ledger_payments_home');
    }

    public function get_maakview_receipts_report(Request $request)
    {

        $currentMonth = $request->month_value;
        $currentYear = $request->year_value;
        $currentDate_req = $request->date_value;
        // $currentDate_converted = Carbon::parse($currentDate_req);
        // $day = $currentDate_converted->day;

        // dd( $currentDate_req);



        $currentDate = Carbon::create($currentYear, $currentMonth, 1, 0, 0, 0);


        // Calculation Previous Month for CashOpenningDb
        $firstDayOfPreviousMonth = $currentDate->copy()->subMonth()->startOfMonth();

        // Calculate the date for the last day of the previous month
        $lastDayOfPreviousMonth = $firstDayOfPreviousMonth->copy()->endOfMonth();

        // Retrieve the closing balance record for the previous month
        $recordsForPreviousMonth = CashOpeningDb::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();




        // Calculation Current month for Openning Db
        $startOfMonth = $currentDate->copy()->startOfMonth();
        $endOfMonth = $currentDate->copy()->endOfMonth();

        $recordsForCurrentMonth = CashOpeningDb::whereBetween('date', [$startOfMonth, $endOfMonth])->get();


        // Calculation Upto date for Openning Db
        $startOfMonth = Carbon::create($currentYear, $currentMonth, 1)->startOfMonth();
        $endOfMonth = $startOfMonth->copy()->endOfMonth();

        $recordsUpToDate = CashOpeningDb::whereDate('date', '<=',  $currentDate_req)->get();

        // dd($recordsUpToUptoDate);

        //  Capital Receipts = Asset + Liability + Loan Recovery

        // Asset Calculation
        $customer_wise_total_assets_prev = MaakviewAsset::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_assets_current = MaakviewAsset::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_assets_upto_date = MaakviewAsset::whereDate('date', '<=',  $currentDate_req)->get();

        // Liability Calculation
        $customer_wise_total_liabilties_prev = AccountsReceiptLiablity::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_liabilties_current = AccountsReceiptLiablity::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_liabilties_upto_date = AccountsReceiptLiablity::whereDate('date', '<=',  $currentDate_req)->get();

        // dd($customer_wise_total_liabilties_prev);

        // Loan Calculation

        $customer_wise_total_loan_prev = MaakviewReceiptLoan::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_loan_current = MaakviewReceiptLoan::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_loan_upto_date = MaakviewReceiptLoan::whereDate('date', '<=',  $currentDate_req)->get();
        // // end Capital Receipt


        // // Income

        $customer_wise_total_sales_income_prev = MaakviewIncome::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_sales_income_current = MaakviewIncome::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_sales_income_upto_date = MaakviewIncome::whereDate('date', '<=',  $currentDate_req)->get();


        // dd($customer_wise_total_assets_upto_date)->first();

        // foreach ($customer_wise_total_assets_prev as $recordPrevMonth){

        //     $matchedRecord = $customer_wise_total_assets_upto_date->first(function ($value) use ($recordPrevMonth) {

        //         // return (float) $value->amount === (float) $recordPrevMonth->amount;
        //         return $value->description === $recordPrevMonth->description;
        //     });



        // }

        // dd($customer_wise_total_assets_prev);




        return view('backend.accounts.receipt_payments.accounts_receipts_report', compact('recordsForPreviousMonth', 'recordsForCurrentMonth', 'recordsUpToDate', 'customer_wise_total_assets_prev', 'customer_wise_total_assets_current', 'customer_wise_total_assets_upto_date', 'customer_wise_total_liabilties_prev', 'customer_wise_total_liabilties_current', 'customer_wise_total_liabilties_upto_date', 'customer_wise_total_loan_prev', 'customer_wise_total_loan_current', 'customer_wise_total_loan_upto_date', 'customer_wise_total_sales_income_prev', 'customer_wise_total_sales_income_current', 'customer_wise_total_sales_income_upto_date', 'currentMonth'));
    }





    public function get_maakview_payments_report_input()
    {

        return view('backend.accounts.receipt_payments.accounts_payments_report_input');
    }

    public function get_maakview_payments_report_data(Request $request)
    {

        $postData = $request->all();

        // You can also store the data in the session if needed
        $request->session()->put('post_data', $postData);

        return redirect()->route('accounts.maakview_payments_report');
    }

    public function get_maakview_payments_report_post(Request $request)
    {


        $currentMonth = $request->month_value;
        $currentYear = $request->year_value;
        // $currentDate_req = $request->date_value;
        // $startOfMonth = Carbon::create($currentYear, $currentMonth, 1)->startOfMonth();
        // $endOfMonth = $startOfMonth->copy()->endOfMonth();

        $currentDate = Carbon::create($currentYear, $currentMonth, 1)->startOfMonth();
        $nextMonth = $currentDate->copy()->addMonth(); // Calculate the next month
        $firstDayOfNextMonth = $nextMonth->copy()->startOfMonth();




        $description = $request->description;
        $amount = $request->amount;

        CashOpeningDb::create([
            'date' => $firstDayOfNextMonth,
            'description' => $description,
            'amount' => $amount,
        ]);
    }

    public function get_maakview_payments_report(Request $request)
    {

        $currentMonth = $request->month_value;
        $currentYear = $request->year_value;
        $currentDate_req = $request->date_value;


        $currentDate = Carbon::create($currentYear, $currentMonth, 1, 0, 0, 0);



        // Calculation Previous Month for CashOpenningDb
        $firstDayOfPreviousMonth = $currentDate->copy()->subMonth()->startOfMonth();

        // Calculate the date for the last day of the previous month
        $lastDayOfPreviousMonth = $firstDayOfPreviousMonth->copy()->endOfMonth();

        $startOfMonth = Carbon::create($currentYear, $currentMonth, 1)->startOfMonth();
        $endOfMonth = $startOfMonth->copy()->endOfMonth();

        //    total payments liabilities

        $customer_wise_total_liabilities_prev = AccountsPaymentLiablity::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_liabilities_current = AccountsPaymentLiablity::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_liabilities_upto_date = AccountsPaymentLiablity::whereDate('date', '<=',  $currentDate_req)->get();

        // dd($customer_wise_total_liabilities_prev);


        // Total Loans

        $customer_wise_total_loans_prev = MaakviewPaymentLoan::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_loans_current = MaakviewPaymentLoan::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_loans_upto_date = MaakviewPaymentLoan::whereDate('date', '<=',  $currentDate_req)->get();

        //  dd($customer_wise_total_loans_current);



        // total expense payments

        $customer_wise_total_revenue_payments_prev = MaakviewExpensesNew::whereBetween('date', [$firstDayOfPreviousMonth, $lastDayOfPreviousMonth])->get();
        $customer_wise_total_revenue_payments_current = MaakviewExpensesNew::whereBetween('date', [$startOfMonth, $endOfMonth])->get();
        $customer_wise_total_revenue_payments_upto_date = MaakviewExpensesNew::whereDate('date', '<=',  $currentDate_req)->get();

        // dd($customer_wise_total_revenue_payments_upto_date);




        // Total Receipt Calculation for catch Data 






        $postDataFromSession = $request->session()->get('post_data');

        // dd($postDataFromSession['totalUptoPrevMonth']);

        $total_UptoPrevMonth_Receipts = $postDataFromSession['totalUptoPrevMonth'];
        $total_CurrentMonth_Receipts = $postDataFromSession['totalCurrentMonth'];
        $total_UptoDate_Receipts = $postDataFromSession['totalUptoDate'];


        // dd($totalUptoDate);

        // End Total Receipt Calculation


        // Total Payment Calculation




        //    Previous month total Payment Calculation

        $liabilities_prev_total = 0;

        foreach ($customer_wise_total_liabilities_prev as $prev) {
            $liabilities_prev_total += $prev->amount;
        }

        $loans_prev_total = 0;

        foreach ($customer_wise_total_loans_prev as $prev) {

            $loans_prev_total += $prev->amount;
        }


        $revenue_prev_total = 0;

        foreach ($customer_wise_total_revenue_payments_prev as $prev) {

            $revenue_prev_total += $prev->amount;
        }


        $total_UptoPrevMonth_Payments = $liabilities_prev_total +  $loans_prev_total +  $revenue_prev_total;

        // dd($total_Payment_UptoPrevMonth);

        // end Previous month


        // start current month payment calculation

        $liabilities_current_total = 0;

        foreach ($customer_wise_total_liabilities_current as $current) {
            $liabilities_current_total += $current->amount;
        }

        $loans_current_total = 0;

        foreach ($customer_wise_total_loans_current as $current) {

            $loans_current_total += $current->amount;
        }


        $revenue_current_total = 0;

        foreach ($customer_wise_total_revenue_payments_current as $current) {

            $revenue_current_total += $current->amount;
        }


        $total_Current_Payments = $liabilities_current_total +  $loans_current_total +  $revenue_current_total;


        // end Current Month

        // start Upto_date calculation


        $liabilities_upto_date_total = 0;

        foreach ($customer_wise_total_liabilities_upto_date as $upto_date) {
            $liabilities_upto_date_total += $upto_date->amount;
        }

        $loans_upto_date_total = 0;

        foreach ($customer_wise_total_loans_upto_date as $upto_date) {

            $loans_upto_date_total += $upto_date->amount;
        }


        $revenue_upto_date_total = 0;

        foreach ($customer_wise_total_revenue_payments_upto_date as $upto_date) {

            $revenue_upto_date_total += $upto_date->amount;
        }


        $total_upto_date_Payments = $liabilities_upto_date_total +  $loans_upto_date_total +  $revenue_upto_date_total;


        // end Upto date calculation


        // Final closing balance calculation



        $totalUptoPrevMonth_Closing =  $total_UptoPrevMonth_Receipts - $total_UptoPrevMonth_Payments;
        $totalCurrentMonth_Closing =  $total_CurrentMonth_Receipts - $total_Current_Payments;
        $totalUptoDate_Closing = $total_UptoDate_Receipts - $total_upto_date_Payments;



        // dd($total_upto_date_Payments);




        //////// Cash in Bank calculation upto prev month 

        $nextMonth = $currentDate->copy()->addMonth(); // Calculate the next month
        $firstDayOfNextMonth = $nextMonth->copy()->startOfMonth();


        $CashInHand = CashOpeningDb::where('date', $firstDayOfNextMonth)
            ->where('description', 'Cash in Hand')->get();


        $firstCashInHandRecord = $CashInHand->first();

        // Check if the record is found
        if ($firstCashInHandRecord) {
            // Access the 'amount' attribute of the record
            $CashInHand_amount = $firstCashInHandRecord->amount;
        } else {
            // If the record is not found, set a default value (adjust as needed)
            $CashInHand_amount = 0;
        }

        // Perform the calculation
        $CashAtBank = $totalUptoDate_Closing - $CashInHand_amount;


        // dd($totalUptoDate_Closing);


        // Store Cash in Bank data to CashOpeningDb



        $cashAtBankRecord = CashOpeningDb::where('date', $firstDayOfNextMonth)
            ->where('description', 'Cash at Bank')
            ->first();

        // If no record is found, create a new one
        if (!$cashAtBankRecord) {
            CashOpeningDb::create([
                'date' => $firstDayOfNextMonth,
                'description' => 'Cash at Bank',
                'amount' => $CashAtBank,
            ]);
        } else {
            // The record already exists, handle it accordingly
            // You might want to update the existing record or skip creating a new one
            // For now, let's assume you want to update the existing record's amount
            echo "Records already exist for Current Month";
        }









        // Closing balance adjusted in next month

        // $nextMonth = $currentDate->copy()->addMonth(); // Calculate the next month
        // $firstDayOfNextMonth = $nextMonth->copy()->startOfMonth();

        // CashOpeningDb::create([
        //     'date' => $firstDayOfNextMonth,
        //     'description' => 'Cash in Hand',
        //     'amount' => $CashInHand,
        // ]);

        // CashOpeningDb::create([
        //     'date' => $firstDayOfNextMonth,
        //     'description' => 'Cash at Bank',
        //     'amount' =>  $CashAtBank,
        // ]);



        // Cash at Bank calculation



































        return view('backend.accounts.receipt_payments.accounts_payments_report', compact('customer_wise_total_liabilities_prev', 'customer_wise_total_liabilities_current', 'customer_wise_total_liabilities_upto_date', 'customer_wise_total_loans_prev', 'customer_wise_total_loans_current', 'customer_wise_total_loans_upto_date', 'customer_wise_total_revenue_payments_prev', 'customer_wise_total_revenue_payments_current', 'customer_wise_total_revenue_payments_upto_date', 'totalUptoPrevMonth_Closing', 'totalCurrentMonth_Closing', 'totalUptoDate_Closing', 'currentMonth'));











































        // // Closing balance adjusted in next month

        $nextMonth = $currentDate->copy()->addMonth(); // Calculate the next month
        $firstDayOfNextMonth = $nextMonth->copy()->startOfMonth();


        // // Retrieve CashClosingDb records for the current month
        // $cashClosingDbRecordsForCurrentMonth = CashClosingDb::whereMonth('date', $currentDate->month)
        //     ->whereYear('date', $currentDate->year)
        //     ->get();







        // foreach ($cashClosingDbRecordsForCurrentMonth as $closingDbRecord) {
        //     // Check if a CashOpeningDb record already exists for the next month
        //     $existingRecord = CashOpeningDb::whereMonth('date', $firstDayOfNextMonth->month)
        //         ->whereYear('date', $firstDayOfNextMonth->year)
        //         ->where('description', $closingDbRecord->description) // Assuming description is a unique identifier
        //         ->first();

        //     // TEST 
        //     // Cash in Hand Calculation

        //     // Step 1: Retrieve Cash in Hand from cash_opening_db table
        //     $cashOpeningDb_first = CashOpeningDb::where('date', $currentDate)->first();



        //     // Calculate the opening balance for the specified month
        //     // $cashInHand = $cashOpeningDb_first->upto_prev_month + $cashOpeningDb_first->current_month + $cashOpeningDb_first->upto_date;

        //     // Retrieve Cash Debit and Cash Credit from maak_cash_books table for the specified period
        //     $cashDebit = MaakCashBook::whereBetween('date', [$startOfMonth, $endOfMonth])->sum('cash_debit');
        //     $cashCredit = MaakCashBook::whereBetween('date', [$startOfMonth, $endOfMonth])->sum('cash_credit');


        //     // // Calculate Cash in Hand for Closing Balance
        //     $closingCashInHand_prev = $cashOpeningDb_first->upto_prev_month  + ($cashDebit - $cashCredit);
        //     $closingCashInHand_current = $cashOpeningDb_first->current_month + ($cashDebit - $cashCredit);
        //     $closingCashInHand_upto_date = $cashOpeningDb_first->upto_date + ($cashDebit - $cashCredit);



        //     // Store $closingCashInHand in the database or use it as needed



        //     // Cash in Bank Calculation


        //     $cashOpeningDb_second = CashOpeningDb::where('date', $currentDate)
        //         ->orderBy('date', 'asc')
        //         ->skip(1)
        //         ->take(1)
        //         ->first();

        //     $cashAtBank = $cashOpeningDb_second->upto_prev_month + $cashOpeningDb_second->current_month + $cashOpeningDb_second->upto_date;

        //     // Retrieve Cash Debit and Cash Credit from maak_cash_books table for the specified period
        //     $BankDebit = MaakCashBook::whereBetween('date', [$startOfMonth, $endOfMonth])->sum('bank_debit');
        //     $BankCredit = MaakCashBook::whereBetween('date', [$startOfMonth, $endOfMonth])->sum('bank_credit');


        //     // // Calculate Cash in Hand for Closing Balance
        //     $closingCashAtBank = $cashAtBank  + ($BankDebit - $BankCredit);

        //     $closingCashAtBank_prev = $cashOpeningDb_second->upto_prev_month  + ($BankDebit - $BankCredit);
        //     $closingCashAtBank_current = $cashOpeningDb_second->current_month + ($BankDebit - $BankCredit);
        //     $closingCashAtBank_upto_date = $cashOpeningDb_second->upto_date + ($BankDebit - $BankCredit);


        //     // dd($closingCashAtBank_prev);

        //     // Closing Balance store into cash_closing_dbs table

        //     $cashOpeningDbRecord = new CashClosingLocation();
        //     $cashOpeningDbRecord->cash_closing_id = $closingDbRecord->id;
        //     $cashOpeningDbRecord->description = "Cash in HAND";
        //     $cashOpeningDbRecord->date = $currentDate;
        //     $cashOpeningDbRecord->upto_prev_month = $closingCashInHand_prev;
        //     $cashOpeningDbRecord->current_month = $closingCashInHand_current;
        //     $cashOpeningDbRecord->upto_date = $closingCashInHand_upto_date;
        //     $cashOpeningDbRecord->total_upto_prev_month = $closingDbRecord->upto_prev_month;
        //     $cashOpeningDbRecord->total_current_month = $closingDbRecord->current_month;
        //     $cashOpeningDbRecord->total_upto_date = $closingDbRecord->upto_date;
        //     $cashOpeningDbRecord->save();


        //     $cashOpeningDbRecord = new CashClosingLocation();
        //     $cashOpeningDbRecord->cash_closing_id = $closingDbRecord->id;
        //     $cashOpeningDbRecord->description = "Cash at BANK";
        //     $cashOpeningDbRecord->date = $currentDate;
        //     $cashOpeningDbRecord->upto_prev_month = $closingCashAtBank_prev;
        //     $cashOpeningDbRecord->current_month = $closingCashAtBank_current;
        //     $cashOpeningDbRecord->upto_date = $closingCashAtBank_upto_date;
        //     $cashOpeningDbRecord->total_upto_prev_month = $closingDbRecord->upto_prev_month;
        //     $cashOpeningDbRecord->total_current_month = $closingDbRecord->current_month;
        //     $cashOpeningDbRecord->total_upto_date = $closingDbRecord->upto_date;
        //     $cashOpeningDbRecord->save();

        // end Test




        // if (!$existingRecord) {

        // $cashOpeningDbRecord = new CashClosingLocation();
        // $cashOpeningDbRecord->cash_closing_id = $closingDbRecord->id;
        // $cashOpeningDbRecord->description = $request->description_1;
        // $cashOpeningDbRecord->date =  $currentDate;
        // $cashOpeningDbRecord->upto_prev_month = $request->upto_prev_month_1;
        // $cashOpeningDbRecord->current_month = $request->current_month_1;
        // $cashOpeningDbRecord->upto_date = $request->upto_date_1;
        // $cashOpeningDbRecord->total_upto_prev_month = $closingDbRecord->upto_prev_month;
        // $cashOpeningDbRecord->total_current_month = $closingDbRecord->current_month;
        // $cashOpeningDbRecord->total_upto_date = $closingDbRecord->upto_date;
        // $cashOpeningDbRecord->save();



        // $cashOpeningDbRecord = new CashClosingLocation();
        // $cashOpeningDbRecord->cash_closing_id = $closingDbRecord->id;
        // $cashOpeningDbRecord->description = $request->description_2;
        // $cashOpeningDbRecord->date =  $currentDate;
        // $cashOpeningDbRecord->upto_prev_month = $request->upto_prev_month_2;
        // $cashOpeningDbRecord->current_month = $request->current_month_2;
        // $cashOpeningDbRecord->upto_date = $request->upto_date_2;
        // $cashOpeningDbRecord->total_upto_prev_month = $closingDbRecord->upto_prev_month;
        // $cashOpeningDbRecord->total_current_month = $closingDbRecord->current_month;
        // $cashOpeningDbRecord->total_upto_date = $closingDbRecord->upto_date;
        // $cashOpeningDbRecord->save();





        // $cashOpeningDbRecord = new CashOpeningDb();
        // $cashOpeningDbRecord->date = $firstDayOfNextMonth;
        // $cashOpeningDbRecord->description = $request->description_1;
        // $cashOpeningDbRecord->upto_prev_month = $request->upto_prev_month_1;
        // $cashOpeningDbRecord->current_month = $request->current_month_1;
        // $cashOpeningDbRecord->upto_date = $request->upto_date_1;
        // $cashOpeningDbRecord->save();


        // $cashOpeningDbRecord = new CashOpeningDb();
        // $cashOpeningDbRecord->date = $firstDayOfNextMonth;
        // $cashOpeningDbRecord->description = $request->description_2;
        // $cashOpeningDbRecord->upto_prev_month = $request->upto_prev_month_2;
        // $cashOpeningDbRecord->current_month = $request->current_month_2;
        // $cashOpeningDbRecord->upto_date = $request->upto_date_2;
        // $cashOpeningDbRecord->save();








        // }


    }

    // return view('backend.accounts.receipt_payments.accounts_payments_report', compact('customer_wise_total_liabilities_current', 'customer_wise_total_revenue_payments_current', 'totalUptoPrevMonth_Closing', 'totalCurrentMonth_Closing', 'totalUptoDate_Closing', 'currentMonth', 'customer_wise_total_loan_current'));






    // public function get_maakview_payments_report_input_update(Request $request){

    //     $cashOpeningDbRecord = new CashClosingLocation();
    //     $cashOpeningDbRecord->description = $request->description_1;
    //     $cashOpeningDbRecord->upto_prev_month = $request->upto_prev_month_1;
    //     $cashOpeningDbRecord->current_month = $request->current_month_1;
    //     $cashOpeningDbRecord->upto_date = $request->upto_date_1;
    //     $cashOpeningDbRecord->save();


    //     $cashOpeningDbRecord = new CashClosingLocation();
    //     $cashOpeningDbRecord->description = $request->description_2;
    //     $cashOpeningDbRecord->upto_prev_month = $request->upto_prev_month_2;
    //     $cashOpeningDbRecord->current_month = $request->current_month_2;
    //     $cashOpeningDbRecord->upto_date = $request->upto_date_2;
    //     $cashOpeningDbRecord->save();


    //     return redirect()->route('accounts.maakview_payments_report');
    // }





    public function maakview_ledger_input()
    {

        return view('backend.accounts.maakview_ledger_input_monthly_select');
    }

    public function maakview_ledger_monthly_input(Request $request)
    {
        $month_value = $request->month_value;
        $year_value = $request->year_value;

        return view('backend.accounts.maakview_ledger_input_home', compact('month_value', 'year_value'));
    }

    public function maakview_ledger_monthly_input_store(Request $request)
    {


        $validatedData = $request->validate([
            'office_rent' => 'nullable|numeric',
            'warehouse_rent' => 'nullable|numeric',
            'office_utilites' => 'nullable|numeric',
            'marketing_online' => 'nullable|numeric',
            'advertisement' => 'nullable|numeric',
            'business_promotion' => 'nullable|numeric',
            'ip_phone_bill' => 'nullable|numeric',
            'mobile_bill' => 'nullable|numeric',
            'festibal_allowance' => 'nullable|numeric',
            'bridge_toll_and_parking' => 'nullable|numeric',
            'gift_and_donation' => 'nullable|numeric',
            'electric_goods' => 'nullable|numeric',
            'entertainment_expenses' => 'nullable|numeric',
            'fuel_oil' => 'nullable|numeric',
            'inernet_and_telephone_line' => 'nullable|numeric',
            'license_and_fees' => 'nullable|numeric',
            'medical_expenses' => 'nullable|numeric',
            'office_maintenance' => 'nullable|numeric',
            'printing_expenses' => 'nullable|numeric',
            'statinonary_expneses' => 'nullable|numeric',
            'electricity_bill' => 'nullable|numeric',
            'repair_and_maintenance_others' => 'nullable|numeric',
            'accessories_expenses' => 'nullable|numeric',
            'carring_expenses' => 'nullable|numeric',
            'product_damage_and_lost_expenses' => 'nullable|numeric',
            'fine_and_penalty' => 'nullable|numeric',
            'load_and_unload_labour_bill' => 'nullable|numeric',
            'lunch_tiffin' => 'nullable|numeric',
            'postage_and_courier' => 'nullable|numeric',
            'employee_loan_and_other_payments' => 'nullable|numeric',
            'legal_cost' => 'nullable|numeric',
            'monthly_incentive_payments' => 'nullable|numeric',
            'website_expenses' => 'nullable|numeric',
            'bank_interest_all' => 'nullable|numeric',
            'miscellaneous_expenses' => 'nullable|numeric',
            'vat_tax' => 'nullable|numeric',
        ]);



        $date = Carbon::parse("1/" . $request->month . "/" . $request->year);

        // Merge the hardcoded date with the validated data
        $dataToSave = array_merge($validatedData, ['date' => $date]);
        $save_maakview_accoutns_input = MaakviewExpences::create($dataToSave);

        if ($save_maakview_accoutns_input) {
            return redirect()->route('accounts.maakview_ledger_monthly_input_list');
        }
    }

    public function maakview_ledger_monthly_input_list()
    {
        $get_all_ledger_report = MaakviewExpences::get();

        return view('backend.accounts.account_ledger_input_list', compact('get_all_ledger_report'));
    }

    public function maakview_expenses_daily_input_edit($id)
    {
        $get_single_month_specefic_ledger_data = MaakviewExpences::where('id', $id)->first();
        return view('backend.accounts.account_ledger_input_edit', compact('get_single_month_specefic_ledger_data'));
    }

    public function maakview_expenses_daily_input_update(Request $request, $id)
    {

        $validate_updated_data = $request->validate([
            'office_rent' => 'nullable|numeric',
            'warehouse_rent' => 'nullable|numeric',
            'office_utilites' => 'nullable|numeric',
            'marketing_online' => 'nullable|numeric',
            'advertisement' => 'nullable|numeric',
            'business_promotion' => 'nullable|numeric',
            'ip_phone_bill' => 'nullable|numeric',
            'mobile_bill' => 'nullable|numeric',
            'festibal_allowance' => 'nullable|numeric',
            'bridge_toll_and_parking' => 'nullable|numeric',
            'gift_and_donation' => 'nullable|numeric',
            'electric_goods' => 'nullable|numeric',
            'entertainment_expenses' => 'nullable|numeric',
            'fuel_oil' => 'nullable|numeric',
            'inernet_and_telephone_line' => 'nullable|numeric',
            'license_and_fees' => 'nullable|numeric',
            'medical_expenses' => 'nullable|numeric',
            'office_maintenance' => 'nullable|numeric',
            'printing_expenses' => 'nullable|numeric',
            'statinonary_expneses' => 'nullable|numeric',
            'electricity_bill' => 'nullable|numeric',
            'repair_and_maintenance_others' => 'nullable|numeric',
            'accessories_expenses' => 'nullable|numeric',
            'carring_expenses' => 'nullable|numeric',
            'product_damage_and_lost_expenses' => 'nullable|numeric',
            'fine_and_penalty' => 'nullable|numeric',
            'load_and_unload_labour_bill' => 'nullable|numeric',
            'lunch_tiffin' => 'nullable|numeric',
            'postage_and_courier' => 'nullable|numeric',
            'employee_loan_and_other_payments' => 'nullable|numeric',
            'legal_cost' => 'nullable|numeric',
            'monthly_incentive_payments' => 'nullable|numeric',
            'website_expenses' => 'nullable|numeric',
            'bank_interest_all' => 'nullable|numeric',
            'miscellaneous_expenses' => 'nullable|numeric',
            'vat_tax' => 'nullable|numeric',
            'date' => 'required | date'
        ]);

        $update_maakview_accoutns_input = MaakviewExpences::where('id', $id)->update($validate_updated_data);

        if ($update_maakview_accoutns_input) {
            return redirect()->route('accounts.maakview_ledger_expenses_report_home')->with('update_ledger_data', 'Updaetd Successfully Ledger Data');
        }
    }

    public function maakview_ledger_daily_input_delete($id)
    {
        $delete_monthly_input_data = MaakviewExpences::where('id', $id)->delete();
        if ($delete_monthly_input_data) {
            return redirect()->route('accounts.maakview_ledger_expenses_report_home')->with('expenses_statsu', 'Deleted Successfully Ledger Data');
        }
    }

    public function maakview_ledger_expenses_input_home()
    {
        return view('backend.accounts.maakview_expneses_home');
    }

    public function maakview_ledger_expenses_input_store(Request $request)
    {


        $date = $request->expenses_date;
        $expense_type = $request->expenses_type;

        $check_previous_expenses_date = MaakviewExpences::whereDate('date', $request->expenses_date)->exists();
        if ($check_previous_expenses_date) {
            $check_previous_expenses_value = MaakviewExpences::whereDate('date', $request->expenses_date)->whereNotNull($expense_type)->exists();
            if ($check_previous_expenses_value) {
                return redirect()->route('accounts.maakview_ledger_expenses_input_home')->with('expenses_statsu', 'Already stored values!');
            } else {
                MaakviewExpences::whereDate('date', $request->expenses_date)->update([
                    $expense_type  => $request->expenses_value
                ]);
                return redirect()->route('accounts.maakview_ledger_expenses_report_home');
            }
        } else {

            $maakview_expenses = new MaakviewExpences();
            $maakview_expenses->date = $request->expenses_date;
            $maakview_expenses->{$expense_type} = $request->expenses_value;
            $maakview_expenses->save();
            return redirect()->route('accounts.maakview_ledger_expenses_report_home');
        }
    }

    public function maakview_ledger_expenses_report_home()
    {
        return view('backend.accounts.maakview_expenses_report_home');
    }

    public function maakview_ledger_expenses_report(Request $request)
    {

        $maakview_expenses_report = MaakviewExpences::whereDate('date', $request->expenses_date)->get();
        $report_date = $request->expenses_date;

        if ($maakview_expenses_report->isEmpty()) {
            return redirect()->route('accounts.maakview_ledger_expenses_report_home')->with('expenses_statsu', 'Data Not Found For This Date!');
        } else {
            return view('backend.accounts.maakview_expense_report', compact('maakview_expenses_report', 'report_date'));
        }
    }

    public function maakview_monthly_assets()
    {
        return view('backend.accounts.accounts_maakview_monthly_assets_home');
    }

    public function maakview_monthly_assets_store(Request $request)
    {

        $date = Carbon::parse("1/" . $request->month_value . "/" . $request->year_value);
        $formattedDate = $date->format('Y/d/m');

        //check value is already stored or not
        $check_previous_assets_date = MaakviewAsset::whereDate('date', $date)->exists();

        if (!$check_previous_assets_date) {
            $maakview_asset_instance = new MaakviewAsset();
            $maakview_asset_instance->date = $formattedDate;
            $maakview_asset_instance->description = $request->description;
            $maakview_asset_instance->upto_prev_month = $request->upto_prev_month;
            $maakview_asset_instance->current_month = $request->current_month;
            $maakview_asset_instance->upto_date = $request->upto_date;
            $maakview_asset_instance->accounts_receivable_source = $request->accounts_receivable_name;
            $maakview_asset_instance->accounts_receivable = $request->accounts_receivable_amount;
            $maakview_asset_instance->save();
            return redirect()->route('accounts.maakview_monthly_assets_list');
        } else {
            return redirect()->route('accounts.maakview_monthly_assets')->with('asset_status', 'Already stored on this date!');
        }
    }

    public function maakview_monthly_assets_list()
    {
        $maakview_assets_data = MaakviewAsset::all();
        return view('backend.accounts.accounts_maakview_assets_list', compact('maakview_assets_data'));
    }

    public function maakview_monthly_assets_list_edit($id)
    {
        $maakview_assets_data = MaakviewAsset::where('id', $id)->first();

        return view('backend.accounts.account_maakview_asset_edit', compact('maakview_assets_data'));
    }

    public function maakview_monthly_assets_list_update(Request $request, $id)
    {

        $asset_data_validation = $request->validate([
            'current_hand_cash'  => 'required| numeric',
            'fixed_assets'  => 'required| numeric',
        ]);

        if ($asset_data_validation) {
            MaakviewAsset::where('id', $id)->update([
                'current_hand_cash' => $request->current_hand_cash,
                'accounts_receivable_source' => $request->accounts_receivable_source ?? null,
                'accounts_receivable' => $request->accounts_receivable ?? null,
                'fixed_assets' => $request->fixed_assets,
                'owners_capital' => $request->owners_capital,
            ]);

            return redirect()->route('accounts.maakview_monthly_assets_list')->with('udpate_status', 'assets updated successfully');
        }
    }

    public function maakview_monthly_assets_list_delete($id)
    {
        $delete_maakview_asset = MaakviewAsset::where('id', $id)->delete();
        return redirect()->route('accounts.maakview_monthly_assets_list')->with('delete_status', 'assets deleted successfully');
    }


    public function maakview_monthly_account_libilites()
    {
        return view('backend.accounts.accounts_maakview_monthly_account_libilites_home');
    }

    public function maakview_monthly_accounts_libilites_store(Request $request)
    {

        $request->validate([
            'accounts_payable' => 'required',
            'Loans_payable' => 'required',
            'accured_expenses' => 'required',
        ]);

        $date = Carbon::parse("1/" . $request->month_value . "/" . $request->year_value);
        $formattedDate = $date->format('Y/d/m');
        //check value is already stored or not
        $check_previous_assets_date = AccountsLiabilities::whereDate('date', $date)->exists();

        if (!$check_previous_assets_date) {
            $maakview_accounts_liabilites = new AccountsLiabilities();
            $maakview_accounts_liabilites->date = $formattedDate;
            $maakview_accounts_liabilites->accounts_payable = $request->accounts_payable;
            $maakview_accounts_liabilites->Loans_payable = $request->Loans_payable;
            $maakview_accounts_liabilites->accured_expenses = $request->accured_expenses;
            $maakview_accounts_liabilites->save();
            return redirect()->route('accounts.maakview_monthly_accounts_libilites_list');
        } else {
            return redirect()->route('accounts.maakview_monthly_account_libilites')->with('liabilites_status', 'Already stored on this date!');
        }
    }

    public function maakview_monthly_accounts_libilites_list()
    {
        $all_accountsLiabilities = AccountsLiabilities::all();
        return view('backend.accounts.accounts_maakview_liabilites_list', compact('all_accountsLiabilities'));
    }


    public function maakview_monthly_accounts_libilites_edit($id)
    {
        $maakview_single_accountsLiabilities = AccountsLiabilities::where('id', $id)->first();

        return view('backend.accounts.accounts_maakview_liabilites_edit', compact('maakview_single_accountsLiabilities'));
    }

    public function maakview_monthly_accounts_libilites_update(Request $request, $id)
    {

        $validation_liabilites = $request->validate([
            'accounts_payable' => 'required',
            'Loans_payable' => 'required',
            'accured_expenses' => 'required',
        ]);

        if ($validation_liabilites) {
            AccountsLiabilities::where('id', $id)->update([
                'accounts_payable' => $request->accounts_payable,
                'Loans_payable' => $request->Loans_payable,
                'accured_expenses' => $request->accured_expenses,
            ]);

            return redirect()->route('accounts.maakview_monthly_accounts_libilites_list')->with('udpate_status', 'Accounts Liabilites updated successfully');
        }
    }

    public function maakview_monthly_accounts_libilites_delete($id)
    {
        $delete_maakview_accountsLiabilities = AccountsLiabilities::where('id', $id)->delete();
        return redirect()->route('accounts.maakview_monthly_accounts_libilites_list')->with('delete_status', 'Accounts Liabilites deleted successfully');
    }









    public function cash_report()
    {

        //$order_value =  Order::select(DB::raw('SUM(grand_total) as total_amount'),DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status','=','shipped')->orWhere('delivery_status','=','delivered')->get();
        $order_value1 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->whereDate('updated_at', Carbon::today())->where('delivery_status', 'shipped')->get();
        $order_value2 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->whereDate('updated_at', Carbon::today())->where('delivery_status', 'delivered')->get();
        $order_value = ($order_value1[0]->total_amount - $order_value1[0]->total_discount) + ($order_value2[0]->total_amount - $order_value2[0]->total_discount);

        $total_received_today1 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->whereDate('updated_at', Carbon::today())->where('delivery_status', 'shipped')->where('payment_status', 'paid')->get();
        $total_received_today2 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->whereDate('updated_at', Carbon::today())->where('delivery_status', 'delivered')->where('payment_status', 'paid')->get();
        $total_received_today = ($total_received_today1[0]->total_amount - $total_received_today1[0]->total_discount) + ($total_received_today2[0]->total_amount - $total_received_today2[0]->total_discount);

        $total_receivable_due_1 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status', 'shipped')->where('payment_status', 'unpaid')->get();
        $total_receivable_due_2 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status', 'delivered')->where('payment_status', 'unpaid')->get();
        $total_receivable_due = ($total_receivable_due_1[0]->total_amount - $total_receivable_due_1[0]->total_discount) + ($total_receivable_due_2[0]->total_amount - $total_receivable_due_2[0]->total_discount);

        $total_receivable1 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status', 'shipped')->get();
        $total_receivable2 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status', 'delivered')->get();
        $total_receivable = ($total_receivable1[0]->total_amount - $total_receivable1[0]->total_discount) + ($total_receivable2[0]->total_amount - $total_receivable2[0]->total_discount);

        $total_received_all1 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status', 'shipped')->where('payment_status', 'paid')->get();
        $total_received_all2 =  Order::select(DB::raw('SUM(grand_total) as total_amount'), DB::raw('SUM(special_discount) as total_discount'))->where('delivery_status', 'delivered')->where('payment_status', 'paid')->get();
        $total_received_all = ($total_received_all1[0]->total_amount - $total_received_all1[0]->total_discount) + ($total_received_all2[0]->total_amount - $total_received_all2[0]->total_discount);

        //   dd($order_value2);
        return view('backend.product.report.cash_report', compact('order_value', 'total_received_today', 'total_receivable_due', 'total_receivable', 'total_received_all'));
    }

    public function get_maakview_balance_sheet()
    {
        return view('backend.accounts.accounts_balance_sheet');
    }
}
