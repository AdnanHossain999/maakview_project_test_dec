<?php

namespace App\Http\Controllers;

use App\Models\DemoMoneyReceipt;
use App\Models\DemoMrtMoneyReceipt;
use App\Models\MoneyReceipt;
use App\Models\Supplier;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use NumberFormatter;

class DemoMoneyReceiptController extends Controller
{
    public function __construct()
    {
      
        
        $this->middleware(['permission:view_demo_money_receipt'])->only('index');
        
    }


    
    public function get_demo_money_receipt()
    {
        return view('backend.demo_money_receipt.index');
    }

    public function get_mrt_money_receipt()
    {
        return view('backend.demo_money_receipt.mrt_index');
    }

    
    


    public function view(Request $request)
    
    {

        $customerName = request('customerName');
        $supplierName = request('supplierName');
        
        $amount = $request->input('stash');

        $account_name = $request->input('name');

        $checque_number = request('CNumber');

        $checque_date = request('CDate');
        $formatted_checque_date = Carbon::parse($checque_date)->format('j-n-Y');
        
        $bill_info =  request('info');
        $bill_date =  request('info_date');
        $formatted_bill_date = Carbon::parse($bill_date)->format('j-n-Y');
        
        // dd($checque_number);
        

        $customer_Name = $request->filled('customerName') ? $request->input('customerName') : null;
        
        $supplier_Name = $request->filled('supplierName') ? $request->input('supplierName') : null;

        $totalRecords = DemoMoneyReceipt::count();
        

        $serialNumber = $totalRecords;

        $nextSerialNumber = $totalRecords + 1;
       

            DemoMoneyReceipt::create([
                
                'serial_no' => $serialNumber,
                'customer_account_name' => $customer_Name,
                'supplier_account_name' => $supplier_Name,
                'total_payment_in_number' => $amount,
                'account_name' => $account_name,
                'checque_number' => $checque_number,
                'checque_date' =>  $checque_date,
                'bill_info' => $bill_info,
                'bill_date' => $bill_date
                
            ]);
    
        
            
           
        

        return view('backend.demo_money_receipt.view',compact('customerName','supplierName','amount','serialNumber','account_name','checque_number','formatted_checque_date','bill_info','formatted_bill_date'));
    }

    public function mrt_view(Request $request)
    
    {

        $customerName = request('customerName');
        $supplierName = request('supplierName');
        
        $amount = $request->input('stash');

        $account_name = $request->input('name');

        $checque_number = request('CNumber');

        $checque_date = request('CDate');
        $formatted_checque_date = Carbon::parse($checque_date)->format('j-n-Y');
        
        $bill_info =  request('info');
        $bill_date =  request('info_date');
        $formatted_bill_date = Carbon::parse($bill_date)->format('j-n-Y');
        
        // dd($checque_number);
        

        $customer_Name = $request->filled('customerName') ? $request->input('customerName') : null;
        
        $supplier_Name = $request->filled('supplierName') ? $request->input('supplierName') : null;

        $totalRecords = DemoMoneyReceipt::count();
        

        $serialNumber = $totalRecords;

        $nextSerialNumber = $totalRecords + 1;
       

            DemoMrtMoneyReceipt::create([
                
                'serial_no' => $serialNumber,
                'customer_account_name' => $customer_Name,
                'supplier_account_name' => $supplier_Name,
                'total_payment_in_number' => $amount,
                'account_name' => $account_name,
                'checque_number' => $checque_number,
                'checque_date' =>  $checque_date,
                'bill_info' => $bill_info,
                'bill_date' => $bill_date
                
            ]);
    
        
            
           
        

        return view('backend.demo_money_receipt.mrt_view',compact('customerName','supplierName','amount','serialNumber','account_name','checque_number','formatted_checque_date','bill_info','formatted_bill_date'));
    }

    

    public function list() {
        
        $get_all_demo_money_receipt =  DemoMoneyReceipt::paginate(10);

        // dd($get_all_demo_money_receipt);
        return view('backend.demo_money_receipt.demo_money_receipt_list', compact('get_all_demo_money_receipt'));

        
    }

    public function mrt_list() {
        
        $get_all_demo_money_receipt =  DemoMrtMoneyReceipt::paginate(10);

        // dd($get_all_demo_money_receipt);
        return view('backend.demo_money_receipt.mrt_list', compact('get_all_demo_money_receipt'));

        
    }

    public function delete($id){
        
        $delete_demo_money_receipt = DemoMoneyReceipt::where('id', $id)->delete();
        return redirect()->route('demo_money_receipt.list')->with('delete_status', 'Successfully Deleted Money Receipt');
    }

    public function mrt_delete($id){
        
        $delete_demo_money_receipt = DemoMrtMoneyReceipt::where('id', $id)->delete();
        return redirect()->route('mrt_money_receipt.list')->with('delete_status', 'Successfully Deleted Money Receipt');
    }

    public function mrt_print_view($id) {

        $get_mrt_print = DemoMrtMoneyReceipt::findOrFail($id);

        // dd($get_mrt_print);

        return view('backend.demo_money_receipt.mrt_print_view', compact('get_mrt_print'));
    }
    
    public function print_view($id) {

        $get_print = DemoMoneyReceipt::findOrFail($id);

        // dd($get_mrt_print);

        return view('backend.demo_money_receipt.print_view', compact('get_print'));
    }








}