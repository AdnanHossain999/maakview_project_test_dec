<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CustomerLedger;
use App\Models\CustomerLedgerPayment;
use App\Models\Order;
use App\Models\Supplier;
use App\Models\PurchaseTransaction;
use App\Models\Address;
use App\Models\CombinedOrder;
use App\Models\MaakCashBook;
use App\Models\OrderDetail;
use App\Models\User;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Combinations;
use Razorpay\Api\Customer;

class CustomerLedgerController extends Controller
{
    public function index()
    {

        return view('backend.customer_ledger.customer_ledger_input');
    }


    public function customer_ledger_insert(Request $request)
    {



        $ledger_instance = new CustomerLedger;
        $ledger_instance->payment_date = $request->datepicker;
        $ledger_instance->customer_id = $request->customer;
        $ledger_instance->combined_order_id = $request->voucher_code;

        $ledger_instance->payment_type = $request->payment_type;
        $ledger_instance->bank_name = $request->bank_name;
        $ledger_instance->check_number = $request->check_number;

        $ledger_instance->payment_value = $request->amount;
        $save_instance = $ledger_instance->save();






        $url = route('customer_ledger_input_list.view');
        if ($save_instance) {

            return redirect($url)->with('success', 'Successfully Updated');
        } else {
            return redirect($url)->with('failed', 'Something wrong');
        }
    }


    public function customer_ledger_input_list_view()
    {
        $customerLedgerData = CustomerLedger::all();
        return view('backend.customer_ledger.customer_ledger_input_list', compact('customerLedgerData'));
    }



    public function customer_ledger_delete($id)
    {
        try {
            $ledger = CustomerLedger::findOrFail($id);
            $ledger->delete();

            return redirect()->back()->with('success', 'Record deleted successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Unable to delete the record');
        }
    }


    public function customer_ledger_edit($id)
    {

        $ledger = CustomerLedger::findOrFail($id);
        return view('backend.customer_ledger.customer_ledger_edit', compact('ledger'));
    }

    public function customer_ledger_update(Request $request, $id)
    {

        $ledger_instance = CustomerLedger::findOrFail($id);
        $ledger_instance->payment_date = $request->date;
        $ledger_instance->customer_id = $request->customer_id;
        $ledger_instance->payment_type = $request->payment_type;
        $ledger_instance->bank_name = $request->payment_type == 2 ? $request->bank_name : null;
        $ledger_instance->check_number = $request->payment_type == 2 ? $request->check_number : null;
        $ledger_instance->payment_value = $request->amount;
        $update_instance = $ledger_instance->save();

        $url = route('customer_ledger_input_list.view');
        if ($update_instance) {

            return redirect($url)->with('success', 'Successfully Updated');
        } else {
            return redirect($url)->with('failed', 'Something wrong');
        }
    }

    public function customer_payment_history()
    {
        $customerPaymentHistoryData = CustomerLedger::distinct('customer_id')->pluck('customer_id');
        return view('backend.customer_ledger.customer_payment_history', compact('customerPaymentHistoryData'));
    }

    public function customer_payment_view($customer_id)
    {
        $customerData = CustomerLedger::where('customer_id', $customer_id)->with('combinedOrder')->get();

        return view('backend.customer_ledger.customer_payment_idwise', compact('customerData', 'customer_id'));
    }


    public function customer_ledger_summary_report_view(Request $request, $id)
    {
        $customer_total_sale_history = Order::where('user_id', $id)
            ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
            ->first();

        $currentMonth = Carbon::now()->month;
        $current_month_total_sale =  Order::where('user_id', $id)
            ->whereMonth('created_at', $currentMonth)
            ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
            ->first();
        $currentDate = Carbon::now();

        // Calculate the start and end dates for the last month
        $startDate = $currentDate->copy()->subMonth()->startOfMonth();
        $endDate = $currentDate->copy()->subMonth()->endOfMonth();

        $last_month_total_sale =  Order::where('user_id', $id)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
            ->first();

        $get_customer_address = Address::where('user_id', $id)->select('address')->first();
        $get_customer_name = User::select('name')->where('id', $id)->first();

        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();

        if (!$get_supplier_id_for_this_customer) {
            return redirect()->route('customer_payment_history.view')->with('status', 'Please, match with supplier id first');
        }
        $get_total_purchase_amount_for_this_customer = PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
            ->SelectRaw('SUM(payable) AS total_purchase_amount')
            ->first();
        $current_month_total_purchase =   PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
            ->whereMonth('created_at', $currentMonth)
            ->SelectRaw('SUM(payable) AS total_purchase_amount')
            ->first();

        $last_month_total_purchase =   PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->SelectRaw('SUM(payable) AS total_purchase_amount')
            ->first();

        $customer_total_payment = CustomerLedger::where('customer_id', $id)
            ->selectRaw('SUM(payment_value) as total_payment_value')
            ->first();

        $maakview_payment_to_customer = CustomerLedgerPayment::where('customer_id', $id)
            ->selectRaw('SUM(payment_value) as total_maakview_payment_value')
            ->first();


        //written by Adnan

        $user_create_data = User::where('id', $id)->first();

        // dd($user_create_data);

        // $customer_order_date = CombinedOrder::where('user_id',$id)->first();

        // dd($customer_order_date);

        return view('backend.customer_ledger.customer_ledger_report_view', compact('customer_total_sale_history', 'get_total_purchase_amount_for_this_customer', 'get_customer_name', 'get_customer_address', 'customer_total_payment', 'maakview_payment_to_customer', 'current_month_total_sale', 'last_month_total_sale', 'current_month_total_purchase', 'last_month_total_purchase', 'current_month_total_purchase', 'last_month_total_purchase', 'user_create_data'));
    }

    // Created by Adnan





    public function customer_ledger_sales_history_report_view(Request $request, $id)
    {
        $debitRecords = [];
        $creditRecords = [];

        $debitSum = 0.00;
        $creditSum = 0.00;

        // Fetch and merge debit records from the 'orders' table

             
     

        // Getting records from Order table

        $customer_sale_history = Order::where('user_id', $id)->get();
        foreach ($customer_sale_history as $order) {
            $amount_debit = $order->grand_total - $order->special_discount;
            $paid = $order->paid;
            $due = $order->due;

            // $description = [];

            $description_sales = "Sales from Order"; // Set a default description value

            $description_purchase = "Cash Paid";

            $debitRecords[] = [
                'date' => $order->created_at,
                'voucher' => $order->combined_order->code,
                'description' => $description_sales,
                'amount' => $amount_debit,
                'type' => 'debit',
            ];

            // Determine the credit based on paid and due
            $creditAmount = $paid === $amount_debit ? 0 : $paid;

            // extra code for fullfill the logic originaly it was without if condition

            if($creditAmount != 0) {
                
                $creditRecords[] = [
                    'date' => $order->created_at,
                    'voucher' => $order->combined_order->code,
                    'description' => $description_purchase,
                    'amount' => $creditAmount,
                    'type' => 'credit',
                ];
            }
           
            
        }





        // Fetch and merge records from 'maak_cash_books'

        // Getting Debit and Credit records from Voucher based on customer_id

        $cashBookRecords_debit = MaakCashBook::where('customer_id', $id)->get();

        foreach ($cashBookRecords_debit as $cashBookRecord) {
            $debitAmount = $cashBookRecord->cash_debit;
            $creditAmount = $cashBookRecord->cash_credit;



            // Now you can use $debitAmount and $creditAmount for each record
            // Add these amounts to $debitRecords and $creditRecords arrays as needed

            // Example:
            $debitRecords[] = [
                'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
                'voucher' => $cashBookRecord->voucher_number,
                'description' => $cashBookRecord->accounts_descriptoin,
                'amount' => $debitAmount,
                'type' => 'debit',
            ];

            // $creditRecords[] = [
            //     'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
            //     'voucher' => $cashBookRecord->voucher_number,
            //     'description' => $cashBookRecord->accounts_descriptoin,
            //     'amount' => $creditAmount,
            //     'type' => 'credit',
            // ];
        }












        // Fetch and merge credit records for purchases
        // Getting Credit records from Purchase table

        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();

        if (!$get_supplier_id_for_this_customer) {
            return redirect()->route('customer_payment_history.view')->with('status', 'Please, match with supplier id first');
        }

        $get_total_purchase_amount_for_this_customer = PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)->get();

        foreach ($get_total_purchase_amount_for_this_customer as $purchase) {
            $amount_credit = $purchase->payable;
            $paid = $purchase->paid;
            $due = $purchase->due;


            $debitAmount = $paid === $amount_credit ? 0 : $paid;
            
            $description_sales = "Purchase Received";
            $description_purchase = "Purchase paid";
            
            // extra code for fullfill the logic originaly it was without if condition
            
            if ($debitAmount != 0) {
                
                $debitRecords[] = [
                    'date' => $purchase->created_at,
                    'voucher' => $purchase->purchase_invoice,
                    'description' => $description_sales,
                    'amount' => $debitAmount,
                    'type' => 'debit',
                ];
            }
           

            $creditRecords[] = [
                'date' => $purchase->created_at,
                'voucher' => $purchase->purchase_invoice,
                'description' => $description_purchase,
                'amount' => $amount_credit,
                'type' => 'credit',
            ];
        }



        // Getting Credit records From voucher based on supplier_id

        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();

        if (!$get_supplier_id_for_this_customer) {
            return redirect()->route('customer_payment_history.view')->with('status', 'Please, match with supplier id first');
        }

        $cashBookRecords_credit = MaakCashBook::where('supplier_id', $get_supplier_id_for_this_customer->id)->get();


        // dd($cashBookRecords_credit);
        foreach ($cashBookRecords_credit as $cashBookRecord) {
            $debitAmount = $cashBookRecord->cash_debit;
            $creditAmount = $cashBookRecord->cash_credit;



            // Now you can use $debitAmount and $creditAmount for each record
            // Add these amounts to $debitRecords and $creditRecords arrays as needed

            // Example:
            // $debitRecords[] = [
            //     'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
            //     'voucher' => $cashBookRecord->voucher_number,
            //     'description' => $cashBookRecord->accounts_descriptoin,
            //     'amount' => $debitAmount,
            //     'type' => 'debit',
            // ];

            $creditRecords[] = [
                'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
                'voucher' => $cashBookRecord->voucher_number,
                'description' => $cashBookRecord->accounts_descriptoin,
                'amount' => $creditAmount,
                'type' => 'credit',
            ];
        }








        // Merge debit and credit records
        $allRecords = array_merge($debitRecords, $creditRecords,);


        // Sort the merged records by date
        usort($allRecords, function ($a, $b) {
            return $a['date'] <=> $b['date'];
        });

        // pass customer name to blade

        $order_id = Order::where('user_id', $id)->first()->user_id;

        $id = $order_id;

        // dd($order_id);

        $customer_name = User::where('id', $order_id)->first()->name;

        // pass customer creation date

        $order_id = Order::where('user_id', $id)->first()->user_id;

        $customer_date = User::where('id', $order_id)->first()->created_at;



        // Pass $allRecords, debitSum, and creditSum to the view

        return view('backend.customer_ledger.customer_ledger_history_report_view', compact('allRecords', 'debitSum', 'creditSum', 'customer_name', 'customer_date','id'));
    }


    public function customer_ledger_sales_history_report_dateFilter(Request $request,$id) {
       

          // Get the 'from' and 'to' dates from the request
          $start_date = $request->input('start_date');
          $end_date = $request->input('end_date');


         

        //   $customer_sale_history = Order::whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();

        $debitRecords = [];
        $creditRecords = [];

        $debitSum = 0.00;
        $creditSum = 0.00;

        // Fetch and merge debit records from the 'orders' table

             
     

        // Getting records from Order table

        $customer_sale_history = Order::where('user_id', $id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
        foreach ($customer_sale_history as $order) {
            $amount_debit = $order->grand_total - $order->special_discount;
            $paid = $order->paid;
            $due = $order->due;

            // $description = [];

            $description_sales = "Sales from Order"; // Set a default description value



            $debitRecords[] = [
                'date' => $order->created_at,
                'voucher' => $order->combined_order->code,
                'description' => $description_sales,
                'amount' => $amount_debit,
                'type' => 'debit',
            ];

            // Determine the credit based on paid and due
            $creditAmount = $paid === $amount_debit ? 0 : $paid;

            // extra code for fullfill the logic originaly it was without if condition

            if($creditAmount != 0) {
                
                $creditRecords[] = [
                    'date' => $order->created_at,
                    'voucher' => $order->combined_order->code,
                    'description' => $description_sales,
                    'amount' => $creditAmount,
                    'type' => 'credit',
                ];
            }
           
            
        }





        // Fetch and merge records from 'maak_cash_books'

        // Getting Debit and Credit records from Voucher based on customer_id

        $cashBookRecords_debit = MaakCashBook::where('customer_id', $id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();

        foreach ($cashBookRecords_debit as $cashBookRecord) {
            $debitAmount = $cashBookRecord->cash_debit;
            $creditAmount = $cashBookRecord->cash_credit;



            // Now you can use $debitAmount and $creditAmount for each record
            // Add these amounts to $debitRecords and $creditRecords arrays as needed

            // Example:
            $debitRecords[] = [
                'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
                'voucher' => $cashBookRecord->voucher_number,
                'description' => $cashBookRecord->accounts_descriptoin,
                'amount' => $debitAmount,
                'type' => 'debit',
            ];

            // $creditRecords[] = [
            //     'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
            //     'voucher' => $cashBookRecord->voucher_number,
            //     'description' => $cashBookRecord->accounts_descriptoin,
            //     'amount' => $creditAmount,
            //     'type' => 'credit',
            // ];
        }












        // Fetch and merge credit records for purchases
        // Getting Credit records from Purchase table

        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();

        // if (!$get_supplier_id_for_this_customer) {
        //     return redirect()->route('customer_payment_history.view')->with('status', 'Please, match with supplier id first');
        // }

        $get_total_purchase_amount_for_this_customer = PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();

       

        foreach ($get_total_purchase_amount_for_this_customer as $purchase) {
            $amount_credit = $purchase->payable;
            $paid = $purchase->paid;
            $due = $purchase->due;


            $debitAmount = $paid === $amount_credit ? 0 : $paid;
            $description_purchase = "Purchase paid";
            
            // extra code for fullfill the logic originaly it was without if condition
            
            if ($debitAmount != 0) {
                
                $debitRecords[] = [
                    'date' => $purchase->created_at,
                    'voucher' => $purchase->purchase_invoice,
                    'description' => $description_purchase,
                    'amount' => $debitAmount,
                    'type' => 'debit',
                ];
            }
           

            $creditRecords[] = [
                'date' => $purchase->created_at,
                'voucher' => $purchase->purchase_invoice,
                'description' => $description_purchase,
                'amount' => $amount_credit,
                'type' => 'credit',
            ];
        }



        // Getting Credit records From voucher based on supplier_id

        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();

        // if (!$get_supplier_id_for_this_customer) {
        //     return redirect()->route('customer_payment_history.view')->with('status', 'Please, match with supplier id first');
        // }

        $cashBookRecords_credit = MaakCashBook::where('supplier_id', $get_supplier_id_for_this_customer->id)->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();


        // dd($cashBookRecords_credit);
        foreach ($cashBookRecords_credit as $cashBookRecord) {
            $debitAmount = $cashBookRecord->cash_debit;
            $creditAmount = $cashBookRecord->cash_credit;



            // Now you can use $debitAmount and $creditAmount for each record
            // Add these amounts to $debitRecords and $creditRecords arrays as needed

            // Example:
            // $debitRecords[] = [
            //     'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
            //     'voucher' => $cashBookRecord->voucher_number,
            //     'description' => $cashBookRecord->accounts_descriptoin,
            //     'amount' => $debitAmount,
            //     'type' => 'debit',
            // ];

            $creditRecords[] = [
                'date' => $cashBookRecord->created_at,  // You might need to adjust this based on your table structure
                'voucher' => $cashBookRecord->voucher_number,
                'description' => $cashBookRecord->accounts_descriptoin,
                'amount' => $creditAmount,
                'type' => 'credit',
            ];
        }








        // Merge debit and credit records
        $allRecords = array_merge($debitRecords, $creditRecords,);


        // Sort the merged records by date
        usort($allRecords, function ($a, $b) {
            return $a['date'] <=> $b['date'];
        });

        // pass customer name to blade

        $order_id = Order::where('user_id', $id)->first()->user_id;

        // dd($order_id);

        $customer_name = User::where('id', $order_id)->first()->name;

        // pass customer creation date

        $order_id = Order::where('user_id', $id)->first()->user_id;

        $customer_date = User::where('id', $order_id)->first()->created_at;

        

        



        // Pass $allRecords, debitSum, and creditSum to the view

        return view('backend.customer_ledger.customer_ledger_history_report_view_byDate', compact('allRecords', 'debitSum', 'creditSum', 'customer_name', 'customer_date'));
                                                    

          
        
    }






    public function customer_ledger_payment_input_view()
    {
        return view('backend.customer_ledger.customer_ledger_payment_input');
    }

    public function customer_ledger_payment_input(Request $request)
    {
        $ledger_instance = new CustomerLedgerPayment;
        $ledger_instance->payment_date = $request->datepicker;
        $ledger_instance->customer_id = $request->customer;
        $ledger_instance->purchase_transaction_id = $request->voucher_code;





        $ledger_instance->payment_type = $request->payment_type;
        $ledger_instance->bank_name = $request->bank_name;
        $ledger_instance->check_number = $request->check_number;
        $ledger_instance->payment_value = $request->amount;
        $save_instance = $ledger_instance->save();

        $url = route('customer_ledger_payment_history_list.view');
        if ($save_instance) {

            return redirect($url)->with('success', 'Successfully Updated');
        } else {
            return redirect($url)->with('failed', 'Something wrong');
        }
    }

    public function customer_ledger_payment_history_list_view()
    {
        $customerLedgerPaymentData = CustomerLedgerPayment::all();
        return view('backend.customer_ledger.customer_ledger_payment_list', compact('customerLedgerPaymentData'));
    }

    public function customer_ledger_payment_delete($id)
    {
        try {
            $ledger = CustomerLedgerPayment::findOrFail($id);
            $ledger->delete();

            return redirect()->back()->with('success', 'Record deleted successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Unable to delete the record');
        }
    }

    public function customer_ledger_payment_edit($id)
    {
        $ledger = CustomerLedgerPayment::findOrFail($id);
        return view('backend.customer_ledger.customer_ledger_payment_edit', compact('ledger'));
    }

    public function customer_ledger_payment_update(Request $request, $id)
    {

        $ledger_instance = CustomerLedgerPayment::findOrFail($id);
        $ledger_instance->payment_date = $request->date;
        $ledger_instance->customer_id = $request->customer_id;
        $ledger_instance->payment_type = $request->payment_type;
        $ledger_instance->bank_name = $request->payment_type == 2 ? $request->bank_name : null;
        $ledger_instance->check_number = $request->payment_type == 2 ? $request->check_number : null;
        $ledger_instance->payment_value = $request->amount;
        $update_instance = $ledger_instance->save();

        $url = route('customer_ledger_payment_history_list.view');
        if ($update_instance) {

            return redirect($url)->with('success', 'Successfully Updated');
        } else {
            return redirect($url)->with('failed', 'Something wrong');
        }
    }

    public function maakview_payment_history()
    {
        $paymentHistoryData = CustomerLedgerPayment::distinct('customer_id')->pluck('customer_id');
        return view('backend.customer_ledger.maakview_ledger_payment_history', compact('paymentHistoryData'));
    }

    public function maakview_ledger_payment_view($customer_id)
    {
        $customerData = CustomerLedgerPayment::where('customer_id', $customer_id)->get();
        return view('backend.customer_ledger.maakview_ledger_payment_idwise', compact('customerData', 'customer_id'));
    }


    public function customer_ledger_final_report(Request $request)
    {






        $customers = User::whereNotIn('user_type', ['staff', 'employee'])->orderBy('created_at', 'desc');
        if ($request->has('search')) {
            $sort_search = $request->search;
            $customers = $customers->where('name', 'like', '%' . $sort_search . '%')->orWhere('email', 'like', '%' . $sort_search . '%')->orWhere('phone', 'like', '%' . $sort_search . '%');
        }
        $customers = $customers->paginate(15);


        return view('backend.customer_ledger.customer_ledger_final_report_list', compact('customers'));






        // $sort_search = null;
        // $startDate = null;
        // $endDate = null;

        // // Check if the date range is provided in the request
        // if ($request->has('date_range')) {
        //     list($startDate, $endDate) = explode(' - ', $request->input('date_range'));

        //     // You can add additional validation here to ensure $startDate and $endDate are valid dates.
        // }

        // // Initialize query for customers
        // $customers = User::whereNotIn('user_type', ['staff', 'employee'])->orderBy('created_at', 'desc');

        // // Apply date range filter if the dates are provided
        // if ($startDate && $endDate) {
        //     $customers->whereBetween('created_at', [$startDate, $endDate]);
        // }

        // // Apply search filter
        // if ($request->has('search')) {
        //     $sort_search = $request->input('search');
        //     $customers->where(function ($query) use ($sort_search) {
        //         $query->where('name', 'like', '%' . $sort_search . '%')
        //             ->orWhere('email', 'like', '%' . $sort_search . '%')
        //             ->orWhere('phone', 'like', '%' . $sort_search . '%');
        //     });
        // }

        // // Retrieve paginated results
        // $customers = $customers->paginate(15);

        // return view('backend.customer_ledger.customer_ledger_final_report_list', compact('customers', 'sort_search', 'startDate', 'endDate'));













    }


    //test code 
    public function test_code_for_customer_ledger_view()
    {
        $id = 212;
        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();

        $customer_total_sale_history = Order::where('user_id', $id)
            ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
            ->first();
        $get_total_purchase_amount_for_this_customer = PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
            ->SelectRaw('SUM(payable) AS total_purchase_amount')
            ->first();

        $total_customer_payment = MaakCashBook::where('customer_id', $id)
            ->selectRaw('SUM(cash_debit) as total_customer_sales_payment')
            ->first();



        $total_maakview_payment = MaakCashBook::where('supplier_id', $get_supplier_id_for_this_customer->id)
            ->selectRaw('SUM(cash_credit) as total_customer_sales_payment')
            ->first();

        $debit_values = ((float)$customer_total_sale_history->total_grand_total - (float)$customer_total_sale_history->total_special_discount) + (float) $total_maakview_payment->total_customer_sales_payment;
        $credit_values = (float)$get_total_purchase_amount_for_this_customer->total_purchase_amount + (float)$total_customer_payment->total_customer_sales_payment;

        $total_balance = $debit_values - $credit_values;
        return $total_balance;
    }
}