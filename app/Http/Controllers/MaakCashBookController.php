<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MaakCashBook;
use App\Models\CashOpeningDb;
use App\Models\Order;
use App\Models\Supplier;
use App\Models\PurchaseTransaction;
use App\Models\User;
use App\Models\Address;
use DB;
use Illuminate\Support\Carbon;


class MaakCashBookController extends Controller
{
    public function create_maakview_voucher()
    {
        return view('backend.accounts.cashbook.create_maakview_voucher');
    }

    public function store_maakview_voucher(Request $request)
    {
     
        //validtion data
        $request->validate([
            'voucher_date' => 'required',
            'accounts_code' => 'required',
            'voucher_category' => 'required',
            'voucher_value' => 'required'
        ]);
        
        //store cashbook data
        $voucher_type = $request->voucher_category;

        $last_maakCashBook_id = MaakCashBook::orderBy('id', 'DESC')->first();
        if(!$last_maakCashBook_id){
            $last_maakCashBook_id = 0;
        }else{
            $last_maakCashBook_id = $last_maakCashBook_id->id;
        }

        $voucher_code = "MVV".date('is').($last_maakCashBook_id+1);
 
        $MaakCashBook = new MaakCashBook();
        $MaakCashBook->date = $request->voucher_date;
        $MaakCashBook->accounts_code = $request->accounts_code;
        $MaakCashBook->accounts_descriptoin = $request->accounts_descriptoin;
        $MaakCashBook->voucher_number = $voucher_code;
        
        if(isset($request->supplier_id)){
            $MaakCashBook->supplier_id = $request->supplier_id;
        }elseif(isset($request->customer_id)){
            $MaakCashBook->customer_id = $request->customer_id;
        }
        
        if($voucher_type == 'cash_credit'){
            $MaakCashBook->cash_credit = $request->voucher_value;
        }elseif($voucher_type == 'cash_debit'){
            $MaakCashBook->cash_debit = $request->voucher_value;
        }
        
        $MaakCashBook->bank_name = $request->bank_name ?? null;

        if( $request->accounts_code === 'maak137'){
            if($voucher_type == 'cash_debit'){
                $MaakCashBook->bank_credit = $request->voucher_value;
            }elseif($voucher_type == 'cash_credit'){
                $MaakCashBook->bank_debit = $request->voucher_value;
            }
        }

        $save_voucher = $MaakCashBook->save();

        
        return redirect()->route('accounts.cashbook.maakview_voucher_list');
         
    }

    public function maakview_voucher_list()
    {
        $get_all_voucher =  MaakCashBook::get();
        return view('backend.accounts.cashbook.voucher_list', compact('get_all_voucher'));
    }

    public function maakview_cashbook_list()
    {
        $get_cashbook_list_date_wise = MaakCashBook::select('date')->GroupBy('date')->paginate(10);
        return view('backend.accounts.cashbook.cashbook_list', compact('get_cashbook_list_date_wise'));
    }

    public function maakview_cashbook_details($date)
    {
        $_cashbookDate = $date;

        $_up_to_last_day_balance_for_cash = 0;
        $_up_to_last_day_balance_for_bank = 0;
        $_get_all_cashOpening = CashOpeningDb::all();

        $_opening_balance_for_cash = $_get_all_cashOpening[0]->cash_opening == null ? 0: $_get_all_cashOpening[0]->cash_opening;
        $_opening_balance_for_bank = $_get_all_cashOpening[0]->bank_opening == null ? 0: $_get_all_cashOpening[0]->bank_opening;
        
                                      
                                            
        $carbonDate = Carbon::parse($_cashbookDate);
        $previousDate = $carbonDate->subDay(); 
        $previousDateFormatted = $previousDate->format('Y-m-d');

        
        //calculate cashopening 
        // 1.find all prevous date sum of debit and credit cashbook values.
        $_all_previous_date_sum_cashvalue = MaakCashBook::where('date', '<', $_cashbookDate)
                                                        ->select(
                                                            DB::raw('SUM(cash_debit) as total_cash_debit'),
                                                            DB::raw('SUM(cash_credit) as total_cash_credit'),
                                                            DB::raw('SUM(bank_debit) as total_bank_debit'),
                                                            DB::raw('SUM(bank_credit) as total_bank_credit')
                                                        )
                                                        ->get();
        $_total_prvious_date_cash_debit =  $_all_previous_date_sum_cashvalue[0]->total_cash_debit == null ? 0: $_all_previous_date_sum_cashvalue[0]->total_cash_debit;                                               
        $_total_prvious_date_cash_credit = $_all_previous_date_sum_cashvalue[0]->total_cash_credit == null ? 0: $_all_previous_date_sum_cashvalue[0]->total_cash_credit;  
                                 
        $_previous_day_balanace_cash =  $_opening_balance_for_cash + ($_total_prvious_date_cash_debit - $_total_prvious_date_cash_credit);
        

        $_total_prvious_date_bank_debit =  $_all_previous_date_sum_cashvalue[0]->total_bank_debit == null ? 0: $_all_previous_date_sum_cashvalue[0]->total_bank_debit;                                               
        $_total_prvious_date_bank_credit = $_all_previous_date_sum_cashvalue[0]->total_bank_credit == null ? 0: $_all_previous_date_sum_cashvalue[0]->total_bank_credit;   
                                
        $_previous_day_balanace_bank =  $_opening_balance_for_bank + ($_total_prvious_date_bank_debit - $_total_prvious_date_bank_credit);
                                                       

        
        
        
        $_get_all_data_cashbook_date_wise_current = MaakCashBook::whereDate('date', $_cashbookDate)->get();
        $_get_all_sum_cashbook_date_wise_current = MaakCashBook::whereDate('date', $_cashbookDate)
                                                                ->select(
                                                                    DB::raw('SUM(cash_debit) as total_cash_debit'),
                                                                    DB::raw('SUM(cash_credit) as total_cash_credit'),
                                                                    DB::raw('SUM(bank_debit) as total_bank_debit'),
                                                                    DB::raw('SUM(bank_credit) as total_bank_credit')
                                                                )
                                                                ->get();
                                                        
                                                        
        
        return view('backend.accounts.cashbook.cashbook_details', compact('_get_all_data_cashbook_date_wise_current', '_get_all_sum_cashbook_date_wise_current', '_previous_day_balanace_cash', '_previous_day_balanace_bank', 'date'));
    }


    public function add_maakview_opening_balance()
    {
        $current_cash_opening = CashOpeningDb::all();
        return view('backend.accounts.cashbook.cashbook_opening_balanace', compact('current_cash_opening'));
    }

    public function store_maakview_opening_balance(Request $request)
    {
     
        $_cash_opening_instance = CashOpeningDb::firstOrNew([]);
        $_cash_opening_instance->date = $request->date;
        $_cash_opening_instance->description = $request->description;
        $_cash_opening_instance->upto_prev_month = $request->prev_month;
        $_cash_opening_instance->current_month = $request->current_month;
        $_cash_opening_instance->upto_date = $request->upto_date;
        $_cash_opening_instance->save();
        return redirect()->route('accounts.cashbook.add_maakview_opening_balance');
    }

        

    public function maakview_voucher_delete($id)
    {
        
        $delete_voucher = MaakCashBook::where('id', $id)->delete();
        return redirect()->route('accounts.cashbook.maakview_voucher_list')->with('delete_status', 'Successfully Deleted Voucher');
        
    }



    // customer ledger start 

    public function customer_ledger_home(Request $request)
    {
        $sort_search = null;
        $customers = User::whereNotIn('user_type', ['staff', 'employee'])->orderBy('created_at', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $customers = $customers->where('name', 'like', '%'.$sort_search.'%')->orWhere('email', 'like', '%'.$sort_search.'%')->orWhere('phone', 'like', '%'.$sort_search.'%');
        }
        $customers = $customers->paginate(15);
        
        return view('backend.accounts.customer_ledger.customer_home', compact('customers'));
    }

    public function customer_ledger_report($id)
    {
        $id = $id;
     
        $get_supplier_id_for_this_customer = Supplier::select('id')->where('customer_reference_id', $id)->first();
        
        if(!$get_supplier_id_for_this_customer){
            return redirect()->route('pos.inventory.supplier_list')->with('status', 'Please, match with supplier id first');
        }
        $customer_total_sale_history = Order::where('user_id', $id)
                                            ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
                                            ->first();
        $get_total_purchase_amount_for_this_customer = PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
                                                                            ->SelectRaw('SUM(payable) AS total_purchase_amount')
                                                                            ->first();
        
        $total_customer_payment = MaakCashBook::where('customer_id', $id)
                                                ->selectRaw('SUM(cash_debit) as total_customer_sales_payment')
                                                ->first();
                                                

        
        $total_maakview_payment = MaakCashBook::where('supplier_id', $get_supplier_id_for_this_customer->id)
                                                ->selectRaw('SUM(cash_credit) as total_customer_sales_payment')
                                                ->first();
        
        $debit_values = ((double)$customer_total_sale_history->total_grand_total - (double)$customer_total_sale_history->total_special_discount ) + (double) $total_maakview_payment->total_customer_sales_payment;
        $credit_values = (double)$get_total_purchase_amount_for_this_customer->total_purchase_amount + (double)$total_customer_payment->total_customer_sales_payment;

        $total_balance = $debit_values - $credit_values;




        $currentMonth = Carbon::now()->month;                                    
        $current_month_total_sale =  Order::where('user_id', $id)
                                            ->whereMonth('created_at', $currentMonth)
                                            ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
                                            ->first();
        $currentDate = Carbon::now();

        // Calculate the start and end dates for the last month
        $startDate = $currentDate->copy()->subMonth()->startOfMonth();
        $endDate = $currentDate->copy()->subMonth()->endOfMonth();

        $last_month_total_sale =  Order::where('user_id', $id)
                                    ->whereBetween('created_at', [$startDate, $endDate])
                                    ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
                                    ->first();
        $customer_total_sale_history = Order::where('user_id', $id)
                                        ->selectRaw('SUM(special_discount) as total_special_discount, SUM(grand_total) as total_grand_total')
                                        ->first();
                                        
        $current_month_total_purchase =  PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
                                                            ->whereMonth('created_at', $currentMonth)
                                                            ->SelectRaw('SUM(payable) AS total_purchase_amount')
                                                            ->first();
                                                            
        $last_month_total_purchase =   PurchaseTransaction::where('supplier_id', $get_supplier_id_for_this_customer->id)
                                                            ->whereBetween('created_at', [$startDate, $endDate])
                                                            ->SelectRaw('SUM(payable) AS total_purchase_amount')
                                                            ->first();                                                    
                                                                                                                   
        $get_customer_address = Address::where('user_id', $id)->select('address')->first();
        $get_customer_name = User::select('name')->where('id', $id)->first();

        
        return view('backend.accounts.customer_ledger.customer_ledger_print', compact('debit_values', 'credit_values', 'total_balance', 'get_customer_address','get_customer_name', 'current_month_total_sale', 'last_month_total_sale', 'customer_total_sale_history', 'current_month_total_purchase', 'get_total_purchase_amount_for_this_customer', 'last_month_total_purchase' ));
        
    }

    // customer ledger end

    // Receipt and Payments by Adnan
    
    


    
    // end Receipt and Payments
}