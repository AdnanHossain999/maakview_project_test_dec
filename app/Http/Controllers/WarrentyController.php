<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\PurchaseDetail;
use App\Models\ProductWarrenty;
use App\Models\CombinedOrder;


class WarrentyController extends Controller
{
    public function create_recived_paper()
    {
        return view('backend.warrenty.search_order_product');
    }

    public function search_warrenty_product(Request $request)
    {
   
        $_product_barcode_id = trim($request->product_serial);
        //purchase report
        $_get_products_details = OrderDetail::whereJsonContains('prod_serial_num', strval(trim($_product_barcode_id)))->first();
      
        if($_get_products_details){
            
            //order information
            $order_id = $_get_products_details->order_id;
            $order = Order::find($order_id);

            //purchase information
            $_get_purchase_details = PurchaseDetail::whereJsonContains('serial_numbers', strval(trim($_product_barcode_id)))->first();
            $purchase_details_id = $_get_purchase_details->id;

            // if ($_get_purchase_details) {
            //     $purchase_details_id = $_get_purchase_details->id;
            //     $supplier_name = $_get_purchase_details->supplier->name;
            //     dd($supplier_name);
            // } else {
            //     // Handle the case where no matching purchase detail is found
            //     // For example, you can set a default value or show an error message
            //     dd('Purchase detail not found for the given serial number.');
            // }

            $last_warrenty = ProductWarrenty::orderBy('id', 'DESC')->first();
            if(!$last_warrenty){
                $last_warrenty_id = 0;
            }else{
                $last_warrenty_id = $last_warrenty->id;
            }
            $warrenty_code = "MVW".date('is').($last_warrenty_id+1);

            
            // $supplier_name = $_get_purchase_details->supplier->name;
            // dd( $supplier_name);
            
           
         
            return view('backend.warrenty.create_received_product_paper', compact('_get_products_details', '_product_barcode_id', 'order','_get_purchase_details', 'order_id', 'purchase_details_id', 'warrenty_code'));
        }else{
            return redirect()->route('warrenty.create_recived_paper')->with('product_status', 'Product Not Found');
        }
        
        
    }

    public function store_warrenty_details(Request $request)
    {
        
        $request->validate([
            'purchase_details_id' => 'required',
            'order_id' => 'required',
            'order_id' => 'required',
            'product_id' => 'required',
            'product_serial' => 'required',
            'problem_details' => 'required',
            'warrenty_numbers' => 'required',
        ]);

        $warrenty_instance = new ProductWarrenty();
        $warrenty_instance->product_id = $request->product_id;
        $warrenty_instance->product_serial = $request->product_serial;
        $warrenty_instance->problem_details = $request->problem_details;
        $warrenty_instance->comments = $request->comments;
        $warrenty_instance->warrenty_numbers = $request->warrenty_numbers;
        $warrenty_instance->purchase_details_id = $request->purchase_details_id;
        $warrenty_instance->order_id = $request->order_id;
        $save_warrenty_details = $warrenty_instance->save();

        if($save_warrenty_details){
            return redirect()->route('warrenty.product_warrenty_list');
        }

        
    }

    public function product_warrenty_list()
    {
        
        $_get_product_warrenty_list = ProductWarrenty::all();
        
        return view('backend.warrenty.warrenty_list', compact('_get_product_warrenty_list'));
    }

    public function get_received_paper($id)
    {
        $get_all_warranty_data = ProductWarrenty::where('id', $id)->first();
        $purchase_details_id = $get_all_warranty_data->purchase_details_id;
        $order_id = $get_all_warranty_data->order_id;
        
        $_get_purchase_details = PurchaseDetail::find($purchase_details_id);
        $order = Order::find($order_id);
        $get_invoice_number = CombinedOrder::select('code')->where('id', $order->combined_order_id)->first();
        
        return view('backend.warrenty.print_received_paper', compact('get_all_warranty_data', '_get_purchase_details', 'order', 'get_invoice_number') );
    }

    public function view_received_paper($id)
    {
        $get_all_warranty_data = ProductWarrenty::where('id', $id)->first();
        $purchase_details_id = $get_all_warranty_data->purchase_details_id;
        $order_id = $get_all_warranty_data->order_id;
        
        $_get_purchase_details = PurchaseDetail::find($purchase_details_id);
        $order = Order::find($order_id);
        $get_invoice_number = CombinedOrder::select('code')->where('id', $order->combined_order_id)->first();
        
        return view('backend.warrenty.view_received_paper', compact('get_all_warranty_data', '_get_purchase_details', 'order', 'get_invoice_number'));
    }

    public function create_delivery_paper($id)
    {
        $warrenty_id = $id;
        return view('backend.warrenty.create_delivery_papaer', compact('warrenty_id') );
    }

    public function store_delivery_paper(Request $request)
    {
     
        $save_delivery_paper = ProductWarrenty::where('id', $request->warrenty_id)->update([
            'delivery_comments' =>  $request->delivery_comments ?? null,
            'new_product_id' =>  $request->replace_product_barcode ?? null,
            'delivery_date' =>  $request->delivery_date ?? null,
        ]);

        return redirect()->route('warrenty.product_warrenty_list');

        
        
    }

    public function print_delivery_paper($id)
    {

        $get_all_warranty_data = ProductWarrenty::where('id', $id)->first();
        $purchase_details_id = $get_all_warranty_data->purchase_details_id;
        $order_id = $get_all_warranty_data->order_id;
        
        $_get_purchase_details = PurchaseDetail::find($purchase_details_id);
        $order = Order::find($order_id);
        $get_invoice_number = CombinedOrder::select('code')->where('id', $order->combined_order_id)->first();

        
        return view('backend.warrenty.print_delivery_paper', compact('get_all_warranty_data', '_get_purchase_details', 'order', 'get_invoice_number'));
        
    }

    public function delete_warranty($id)
    {
        $delete_warrenty = ProductWarrenty::where('id', $id)->delete();
        return redirect()->route('warrenty.product_warrenty_list')->with('warranty_satatus', 'Warranty Product Delete Successfully');
    }

    public function edit_received_warranty($id)
    {
        $warrenty_id = $id;
        $received_warranty_data = ProductWarrenty::where('id', $warrenty_id)->first();
        return view('backend.warrenty.edit_received_warranty', compact('warrenty_id', 'received_warranty_data') );
    }

    public function update_received_warranty(Request $request)
    {

        $update_received_paper = ProductWarrenty::where('id', $request->warrenty_id)->update([
            'problem_details' =>  $request->problem_details ?? null,
            'comments' =>  $request->comments ?? null
        ]);

        return redirect()->route('warrenty.product_warrenty_list');
    }
}