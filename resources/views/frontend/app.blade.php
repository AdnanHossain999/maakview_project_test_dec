<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="facebook-domain-verification" content="ys6e4t3khmoxbcqsifaxm41pvat2w6" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GXYMN4D2SP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GXYMN4D2SP');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5S7J7XV');</script>
<!-- End Google Tag Manager -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- title -->
    <title>{!! $meta['meta_title'] !!}</title>

    <!-- meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="description" content="{!! $meta['meta_description'] !!}" />
    <meta name="keywords" content="{{ $meta['meta_keywords'] }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{!! $meta['meta_title'] !!}">
    <meta name="twitter:description" content="{!! $meta['meta_description'] !!}">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="{{ $meta['meta_image'] }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{!! $meta['meta_title'] !!}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->full() }}" />
    <meta property="og:image" content="{{ $meta['meta_image'] }}" />
    <meta property="og:description" content="{!! $meta['meta_description'] !!}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
    <meta property="fb:app_id" content="{{ env('FACEBOOK_PIXEL_ID') }}">
    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('web-assets/css/app.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ mix('web-assets/js/app.js') }}" defer></script>

    <style>
        body,
        .v-application {
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            line-height: 1.6;
            font-size: 14px;
        }

        :root {
            --primary: {{ get_setting('base_color', '#e62d04') }};
            --soft-primary: {{ hex2rgba(get_setting('base_color', '#e62d04'), 0.15) }};
        }
        
        .img-fit{
            object-fit: contain;
        }
        
        /*.d-flex:nth-of-type(1) a:nth-of-type(1){*/
        /*    display:none;*/
        /*}*/
        /*.me-4{*/
        /*    display:none !important;*/
        /*}*/
        /*.la-apple{*/
        /*    display:none !important;*/
        /*}*/
        /*.topbar .opacity-60:nth-child(2){*/
        /*    display:none !important;*/
        /*}*/


    </style>

    @include('frontend.inc.pwa')

    <script>

        window.shopSetting = @json($settings);
        
        setInterval(function () {
            var login_page = $(location). attr("href");
            if(login_page == "https://maakview.com/user/login" || login_page == "https://maakview.com/user/login?redirect=%2Fcheckout"){
                $('button img').css('width','60px');
                $('.text-decoration-underline').css('font-size','26px');
                $('.fs-13').css('font-size','26px');
            }
            if(login_page == "https://maakview.com/user/registration"){
                $('button img').css('width','60px');
                // $('.text-decoration-underline').css('font-size','26px');
                // $('.fs-13').css('font-size','26px');
            }
        }, 200);
        
        setTimeout(function () {
            $('#overlay').hide();
        }, 500)
        setTimeout(function () {
            // document.getElementById("input-220").value="demo@gmail.com";
            // $("#input-220").val("demo@gmail.com");
           
            var check  = function() {
              return $(window).width() <=800
            }


            $(window).scroll(function() {
                var position = $(window).scrollTop();
                var site = $(location). attr("href");

                if(position>10){
                    if (check()) {
                        if(site !== "https://maakview.com/"){
                        $('.logobar').append('<h3 class="call_center" style="z-index:99;color:rgb(85 62 218);padding-left:30px;box-shadow:2px 2px 2px 2px #e0e0e0;position:fixed;top:0;background:white;width:100%;"> <a style="color:rgb(85 62 218);" href="tel:+8801888012727" >অর্ডার করতে কল করুন : ০১৮৮৮০১২৭২৭</a> </h3>');
                        }else{
                             $('#overlay').show();
                        }
                    }
                }else{
                  $('.call_center').remove();
                  $('#overlay').hide();
                }
            });

        }, 1000)
        
    </script>

    @if (get_setting('google_analytics') == 1)
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>

        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', '{{ env('TRACKING_ID') }}');
        </script>
    @endif

    @if (get_setting('facebook_pixel') == 1)
        <!-- Facebook Pixel Code -->
        <script>
            ! function(f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function() {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '{{ env('FACEBOOK_PIXEL_ID') }}');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" style="display:none"
                src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}&ev=PageView&noscript=1" />
        </noscript>
        <!-- End Facebook Pixel Code -->
    @endif

    {!! get_setting('web_custom_css') !!}
    {!! get_setting('header_script') !!}
    
    <!-- Meta Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '305994514836967');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=305994514836967&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->
    <meta name="facebook-domain-verification" content="ys6e4t3khmoxbcqsifaxm41pvat2w6" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <meta name="google-site-verification" content="eXhhWWPVqIZadk3oCfb6fcGwzPH_-X5rcyyARbg2CIo" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="yandex-verification" content="39eb04cacfbda7ee" />
    
    <meta name="title" content="Maakview Q-commerce best price in Bangladesh">
    <meta name="description" content="Maakview.com is the largest online shopping marketplace in Bangladesh, where you can get your necessary products at the best prices in Bangladesh.">
    <meta name="keywords" content="Maakview, best price in bd, online shopping, online shop, best prices, best products, price in bd, online shop, price in Bangladesh, electronics, online store. ">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="author" content="maakview.com">

    <!-- Meta Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '650819246445211');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=650819246445211&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->

<!--For Apps pop-up suggestion code start-->
<style type="text/css">
#overlay {
position: fixed;
top: 0;
left: 0;
width: 100%;
/*height: 0%;*/
/*background-color: #000;*/
filter:alpha(opacity=100);
-moz-opacity:1;
-khtml-opacity: 1;
opacity: 1;
z-index: 100;
display: none;
}
.cnt223 a{
text-decoration: none;
}
.popup{
width: 100%;
margin: 0 auto;
display: none;
position: fixed;
z-index: 101;
}
.cnt223{
/*min-width: 600px;*/
/*width: 600px;*/
/*min-height: 150px;*/
/*min-height: 0px;*/
/*margin: 100px auto;*/
/*background: #f3f3f3;*/
position: relative;
z-index: 103;
/*padding: 15px 35px;*/
/*border-radius: 5px;*/
/*box-shadow: 0 2px 5px #000;*/
}
.cnt223 p{
clear: both;
color: #555555;
text-align: justify; 
font-size: 20px;
font-family: sans-serif;
}
.cnt223 p a{
color: #d91900;
font-weight: bold;
}
.cnt223 .x{
float: right;
/*height: 35px;*/
left: 22px;
position: relative;
top: -25px;
width: 34px;
}
.cnt223 .x:hover{
cursor: pointer;
}
</style>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
<script type='text/javascript'>
$(function(){
// var overlay = $('<div id="overlay"><div class="popup"><div class="cnt223"><a href="https://play.google.com/store/apps/details?id=com.maakview.app&hl=en&gl=US"><img src="https://maakview.com/public/uploads/all/rSgsGBp3LIQgr0dRQdEhQPU5kIlKsBS67RDkDIrT.jpg" height="40"></a><br/><br/><a href="" class="close">X</a></p></div></div></div>');
var overlay = $('<div id="overlay"><div class="popup"><div class="cnt223"><a href="https://play.google.com/store/apps/details?id=com.maakview.app&hl=en&gl=US"><img style="width:100%;height:100%;" src="https://maakview.com/public/uploads/all/rSgsGBp3LIQgr0dRQdEhQPU5kIlKsBS67RDkDIrT.jpg" height="40"></a><br/><br/></p></div></div></div>');
overlay.hide();
overlay.appendTo(document.body);
$('.popup').show();
$('.close').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});


 

// $('.x').click(function(){
// $('.popup').hide();
// overlay.appendTo(document.body).remove();
// return false;
// });
});
</script>
<!--For Apps pop-up suggestion code end-->

</head>

<body>
    <!-- Messenger Chat Plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat Plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "505368259639265");
      chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v15.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
    
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5S7J7XV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <noscript>To run this application, JavaScript is required to be enabled.</noscript>
    <div id="app">
        <theShop></theShop>
    </div>

    @if (get_setting('facebook_chat') == 1)
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({
                    xfbml: true,
                    version: 'v3.3'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            
        </script>
        <div id="fb-root"></div>
        <!-- Your customer chat code -->
        <div class="fb-customerchat" attribution=setup_tool page_id="{{ env('FACEBOOK_PAGE_ID') }}">
        </div>
    @endif

    {!! get_setting('footer_script') !!}
</body>

</html>
