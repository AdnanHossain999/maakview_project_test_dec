<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>

    <head>
        <style>
            .print_func {
                   display: block; /* Display it by default */
               }
   
               @media print {
                   .print_func {
                       display: none; /* Hide it when printing */
                   }
               }

               .custom-table {
                width: 100%; 
               
            }

            .custom-table th:nth-child(1),
                .custom-table td:nth-child(1) {
                    /* Style the first (Date) column */
                    width: 10%; /* Adjust the width as needed */
                }
   
       </style>
    </head>

    <body>
      
        <main>
            <div class="container">
                <div style="text-align: center">
                    <img class="m-0 p-0" src="https://maakview.com/public/logo/logo.png" width="200" height="60" alt="Logo">
                    
                    <p>Rahima Plaza(6th Floor),82/3 Laboratory Road, Dhaka-1205Phone: +8801888-012727,+880244613763</p>
                    <h1 style="font-size: 25px; font-weight:bold; margin-bottom:5px">Products List</h1>
                    <p>Date: {{ date('Y-m-d h:i A') }}</p>
                    {{-- <h1 style="font-size: 50px; font-weight:bold; margin-bottom:5px">Maakview Limited</h1> --}}
                    
                </div>
                <div class="print_func" style="text-align:center;margin-bottom:30px;">
                    <button class="btn btn-success no-print mt-1 " onclick="printFun()"><i class="fa fa-print" aria-hidden="true"></i> Print</button> 
                </div>
                {{-- @can('show_products') --}}
                <table class="table table-bordered custom-table">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Serial No</th>
                            <th>Products Name</th>
                            
                            <th style="text-align: center;">Brand</th>
                            <th style="text-align: center;">Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                        <tr>
                            <td style="text-align: center;">{{$key + 1 }}</td>
                            <td>{{ $product->name }}</td>
                            
                            
                            <td style="text-align: center;">{{ $product->brand->name ?? 'no-brand' }}</td>
                            <td>
                                {{-- <img src="{{($product->thumbnail_img) }}" alt="{{$product->name}}"> --}}
                                <div style="display: flex;justify-content: center;">
                                    <img src="{{ uploaded_asset($product->thumbnail_img) }}" alt="Image" style="width:30px;height:30px;"
                                        
                                        onerror="this.onerror=null;this.src='{{ static_asset('/assets/img/placeholder.jpg') }}';" />
                                    {{-- <span class="flex-grow-1 minw-0">
                                        <div class=" text-truncate-2 fs-12">
                                            {{ $product->getTranslation('name') }}</div>
                                    </span> --}}
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- @endcan --}}
    
                {{-- <div class="print_func">
                    {{ $products->appends(request()->input())->links() }}
                </div> --}}
                
            </div>
        </main>
        


        <script type="text/javascript">
            try {
                
                // this.print();
        
                this.printFun = () => {
                window.print();
                }
                
        
            } catch (e) {
                window.onload = window.print;
            }
            window.onbeforeprint = function() {
                setTimeout(function() {
                    window.close();
                }, 1500);
            }
        
        </script>
    </body>
</html>
