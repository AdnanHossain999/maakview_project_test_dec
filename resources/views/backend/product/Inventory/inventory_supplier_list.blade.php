@extends('backend.layouts.app')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css"/> 
@section('content')
<style>

</style>
<div id="divName">
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-12">
            <h2 class="bg-primary  text-center" style="color:white;">Supplier List </h2>
            </div>
        </div>

        <div class="icon-box1 text-center">
                {{-- <i class="fa fa-user-plus  ml-1 "  data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" aria-hidden="true" style="color:green; margin-top:14px;cursor: pointer"></i> --}}
                <button data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" aria-hidden="true" class="btn btn-info"><i class="fa-solid fa-user-plus"></i> Add Supplier</button>
        </div>
    </div>
    @if (session()->has('status'))
            <div class=" notification alert alert-danger col-md-12">
                {{ session('status') }}
            </div>
    @endif
    <div class="card">
     
        <div class="card-body"> 
        <div class="row">
        <div class="col-md-12 pr-3">
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm p-2 text-center" cellspacing="0" width="100%">
                <thead>
                    
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>customer_profile_id</th>
                        <th>Actions</th> {{-- Add Actions column --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($all_supplier_data as $supplier)
                    <tr>
                        <td>{{ $supplier->name }}</td>
                        <td>{{ $supplier->email }}</td>
                        <td>{{ $supplier->phone }}</td>
                        <td>{{ $supplier->customer_reference_id }}</td>
                        <td>
                            {{-- Add a link to the customer profile --}}
                            <a href="{{route('pos.inventory.supplier_customer_profile_match_home',$supplier->id )}}" class="btn btn-primary btn-sm">
                                Match Customer Profile
                            </a>
                            
                            <a href="{{route('pos.inventory.supplier_list_edit',$supplier->id )}}" class="btn btn-info btn-sm">Edit</a>

                            <form method="POST" action="{{ route('pos.inventory.supplier_list_delete', $supplier->id) }}" style="display: inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>

                        </td>

                    </tr>
                    @endforeach
                </tbody>
              </table>

              {{-- start  modal for adding suppliers --}}
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title text-success" id="exampleModalLabel">Add Suppliers</h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                      <span id="close_supplier_modal" aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="supplier-name" class="col-form-label">Supplier Name:</label>
                        <input type="text" class="form-control supplier_name" id="supplier-name" >
                      </div>
                      <div class="form-group">
                          <label for="supplier-phone" class="col-form-label">Supplier Phone:</label>
                          <input type="text" class="form-control supplier_phone" id="supplier-phone" >
                      </div>
                      <div class="form-group">
                        <label for="supplier-email" class="col-form-label">Supplier Email:</label>
                        <input type="text" class="form-control supplier_email" id="supplier-email" >
                      </div>
                      <div class="form-group">
                        <label for="supplier-address" class="col-form-label">Supplier Address:</label>
                        <input type="text" class="form-control supplier_address" id="supplier-address" >
                      </div>
                      <button type="button" id="add_supplier" class="btn btn-primary">Submit</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {{-- end  modal for adding suppliers --}}
        </div>
        </div>
        </div>
    </div>
</div>

@endsection




@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">

$(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');

});

// Create Supplier

$('#add_supplier').on('click', function (e) {
  e.preventDefault();
  var data = {
    'name': $('.supplier_name').val(),
    'phone': $('.supplier_phone').val(),
    'email': $('.supplier_email').val(),
    'address': $('.supplier_address').val(),
  }
  if(data.name != ""  && data.phone != ""  && data.email != ""  && data.address != "")
  {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      type: "POST",
      url: "{{route('pos.inventory.supplier_store')}}",
      data: data,
      success: function (response) {
        if(response.status == 200)
          {
              
              $('#close_supplier_modal').trigger('click');
              Swal.fire({
                  position: 'top',
                  icon: 'success',
                  title: response.message,
                  showConfirmButton: false,
                  timer: 1500
                  })
              
              $('.modal-body').find('input').val('');
          }
          else if(response.status == 403)
          {
            Swal.fire({
                  icon: 'error',
                  text: response.message
              });
          }
      }
    });
  }
});

// end by Adnan








</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
