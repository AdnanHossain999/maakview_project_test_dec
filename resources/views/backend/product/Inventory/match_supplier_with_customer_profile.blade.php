@extends('backend.layouts.app')

@section('content')
<style>
     #append_div {
            display: block;
            max-height: 220px;
            overflow-y: scroll;
            }

            table thead, table tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed;
            } 
        .main_div{
            background-color: #F0F1F7;
        }
        .serch_option input:hover{
            border: 1px solid #3498db;

        }
        #suggesstion-box, #suggesstion-box_customer li{
            list-style-type: none;
            cursor: pointer;
            padding: 5px;
            padding-left: 30px;
        }
        #suggesstion-box, #suggesstion-box_customer li:hover{
            color: #3498db;

        }

</style>
<div >
    <div class="category_search flex-fill p-1 mb-3">
    <form action="{{route('pos.inventory.supplier_customer_profile_match_store')}}" method="POST">
        @csrf
        <input type="hidden" name="supplier_primary_id" value="{{$supplier_primary_id}}">
        <div class="row">
         <div class="col-md-6 mx-auto">
            @error('customer_id')
                <div class="alert alert-danger" id="error-message">{{ $message }}</div>
            @enderror
            <div><h2 class="text-center">Supplier Name: {{$supplier_name->name}}</h2></div>
             <input type="text" class="form-control serch_customer" id="customer_serch_id" placeholder="Match Customer ">
            <table class="table table-striped">
                <tbody id="append_customer_data" class="mt-2">

                </tbody>
            </table>
         </div>
        </div>
    
        <div id="suggesstion-box_customer" class="search-box_popup text-center"></div>
        <div class="text-center">
            <button class=" btn btn-lg btn-primary">Match Supplier With Customer</button>
        </div>
     </div>
    </form>
</div>

@endsection




@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">

        var errorMessage = document.getElementById('error-message');
        if (errorMessage) {
            setTimeout(function() {
                errorMessage.style.display = 'none';
            }, 2000); // 3000 milliseconds = 3 seconds
        }
        //code start for customer section
        $(".serch_customer").on('keyup', function () {
            $("#suggesstion-box_customer").html("");
            var input_customer_value = $(this).val();
            var search_id_value = $(this).attr('id');
            if(search_id_value == 'staff_serch_id')
            {
                hit_url = "{{route('pos.staff_search')}}";
            }else if(search_id_value == 'customer_serch_id'){
                hit_url = "{{route('pos.customer_search')}}";
            }
            
            $.ajax({
                type: "GET",
                url: hit_url,
                data: {'search':input_customer_value, 'id':""},
                success: function (response) {
                   
                   var data = "";
                   var search_result_data_length = response.data.length;
                   for (let i = 0; i <search_result_data_length; i++) {
                      var customer_name = response.data[i]["name"];
                      var customer_phone = response.data[i]["phone"];
                      var phone_name = customer_name.concat(" (", customer_phone, ")")
                     
                      if(search_id_value == 'staff_serch_id'){
                        data += '<li onClick="selectStaffInfo('+response.data[i]["id"] +')">'+phone_name+'</li>'
                      }else{
                        data += '<li onClick="selectCustomerInfo('+response.data[i]["id"] +')">'+phone_name+'</li>'
                      }
                      

                   }

                   $("#suggesstion-box_customer").show();
                   if(input_customer_value !== "")
                   {
                    $("#suggesstion-box_customer").html(data);
                   }
                   else
                   {
                    $("#suggesstion-box_customer").html("");
                   }

                }
            });

        });


         //function for append data after select specefic customer
         var increments = 1;
        function selectCustomerInfo(id)
        {
            $.ajax({
                type: "GET",
                url: "{{route('pos.customer_search')}}",
                data:{'search':"",'id':id},
                success: function (response) {

                    var row = 
                        '<tr id="data-row-id_customer" class="customer_row_'+increments+' customer_common">\
                            <td class="text-bold"><input type="hidden" name="customer_id" id="customer_id" class="form-control" value="'+response.id+'">'+response.name+'</td>\
                            <td class="text-bold">'+response.phone+'</td>\
                            <td class="text-bold">Customer</td>\
                            <td class="text-center"><i class="fa fa-trash" onclick="deleteCustomer('+increments+')" style="color:red; cursor:pointer" aria-hidden="true"></i></td>\
                        </tr>';

                    if($('.customer_common').length <1){
                        $('#append_customer_data').append(row);
                        $('#customer_serch_id').val('');
                    }

                    $("#suggesstion-box_customer").hide();


                    increments++;
                }
            });

        }


        //delete customer function
        function deleteCustomer(customer_row_value)
        {


            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $('.customer_row_'+customer_row_value).remove();
                Swal.fire(
                'Deleted!',
                'Customer has been deleted.',
                'success'
                )
               
            }
            });
        }









</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
