@extends('backend.layouts.app')

@section('content')




<form method="POST" action="{{ route('pos.inventory.supplier_list_update', $id) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" name="name" class="form-control" value="{{ old('name', $id->name) }}" required>
    </div>

    <div class="form-group">
        <label for="name">Email:</label>
        <input type="email" name="email" class="form-control" value="{{ old('email', $id->email) }}" required>
    </div>


    {{-- <div class="form-group">
        <label for="name">Phone:</label>
        <input type="number" name="phone" class="form-control" value="{{ old('phone', $id->phone) }}" required>
    </div> --}}

    <div class="form-group">
        <label for="name">Phone:</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <!-- Country Code Input -->
                <span class="input-group-text">+88</span> <!-- Replace +1 with your desired country code -->
            </div>
            <!-- Phone Number Input -->
            <input type="text" name="phone" class="form-control" value="{{ old('phone', $id->phone) }}" required>
        </div>
    </div>


    <div class="form-group">
        <label for="name">Address:</label>
        <input type="text" name="address" class="form-control" value="{{ old('address', $id->address) }}" required>
    </div>

    <div class="form-group">
        <label for="name">Customer Reference id:</label>
        <input type="text" name="customer_reference_id" class="form-control" value="{{ old('customer_reference_id', $id->customer_reference_id) }}" required>
    </div>

    
    <button type="submit" class="btn btn-primary">Update Supplier</button>
</form>


@endsection