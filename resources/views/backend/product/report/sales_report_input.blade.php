@extends('backend.layouts.app')
@section('content')
<style>
    
</style>
<div class="container" style="font-weight:bold">
    {{-- <i class="fa fa-user-plus  ml-1 fa-2xl"  data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" aria-hidden="true" style="color:green; margin-top:14px; margin-bottom: 25px;cursor: pointer"></i><span style="padding: 10px;font-size:15px">Register Employee</span> --}}
  
    <div class="card">
        
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    <form action="{{route('sales_report_no_invoice_post.report')}}" method="POST" >
                        @csrf
                        <div class="form-group">
                           <input class="form-control" type="text" name="customer_name" placeholder="Enter customer name">
                        </div>
                        {{-- <div class="form-group">
                            <label for="date">Selected Date</label>
                            <input class="form-control" type="hidden" name="datepicker" value="{{ $Date->format('d-m-Y') }}" readonly>
                        </div> --}}

                        <div class="form-group">
                            <input class="form-control" type="number" name="total_amount" placeholder="Enter Total Amount Sold">
                         </div>

                         <div class="form-group">
                            <input class="form-control" type="hidden" name="user_id" value="{{ $user_id }}">
                         </div>

                         <div class="form-group">
                            {{-- <label for="sales_target">Sales Target</label> --}}
                            <input class="form-control" type="hidden" name="sales_target" value="{{ $sales_target }}" readonly>
                        </div>

                        <input type="submit" class="btn btn-primary" value="Generate Report">

                    </form>

                    {{-- @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif --}}


                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}



@endsection