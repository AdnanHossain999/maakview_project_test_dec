@extends('backend.layouts.app')

@section('content')
<section >
    <div class="container">
        <h1 class="bg-primary text-center text-white">SELECT MONTH AND YEAR to View Sales REPORT</h1>
        <form  action="{{route('view_sales_by_date_post.report')}}" method="POST">
            @csrf
            <div class="row mt-5">
                
                <div class="col-md-4">
                    <select id="month_select" class="form-control" name="month_value">
                        <option value="">Select a month</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                      </select>
                </div>
                <div class="col-md-4">
                    <select id="year_select" class="form-control" name="year_value">
                        <option value="">Select a Year</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
      
                      </select>
                </div>

                <div class="col-md-4">
                    
                    <select class="form-control aiz-selectpicker" id="employee_attendance_id" name="employee_attendance_id" data-live-search="true" required>
                        <option value="">Select Employee Name</option> 
                        @foreach ($get_all_employee as $employee)
                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>
                </div>



            </div>
            <div style="display: flex;justify-content:center;margin-top:30px;">
                <input id="submit_button" type="submit" target="_blank" class="btn btn-primary" disabled>
            </div>

        </form>
    </div>

   
</section>

@endsection


@section('script')
<script type="text/javascript">
$('#month_select').change(function (e) { 
    e.preventDefault();
    $month_value = e.target.value;
    $year_value = $('#year_select').val();
    $date_value = $('#date_value').val();

    if($month_value !== "" && $year_value !== "")
    {
        $('#submit_button').prop('disabled', false);
    }else{
        $('#submit_button').prop('disabled', true);
    }
    
});

$('#year_select').change(function (e) { 
    e.preventDefault();
    $month_value = e.target.value;
    $year_value = $('#year_select').val();
    $date_value = $('#date_value').val();

    if($month_value !== "" && $year_value !== "")
    {
        $('#submit_button').prop('disabled', false);
    }else{
        $('#submit_button').prop('disabled', true);
    }
    
});










</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
