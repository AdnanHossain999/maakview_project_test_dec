@extends('backend.layouts.app')
@section('content')
<style>
    
</style>
<div class="container" style="font-weight:bold">
    {{-- <i class="fa fa-user-plus  ml-1 fa-2xl"  data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" aria-hidden="true" style="color:green; margin-top:14px; margin-bottom: 25px;cursor: pointer"></i><span style="padding: 10px;font-size:15px">Register Employee</span> --}}
  
    <div class="card">
        
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    <form action="{{route('sales_by_date_post.report')}}" method="POST" >
                        @csrf
                        <div class="form-group">
                            <label for="employee">Select Employee Name</label>
                            <select class="form-control aiz-selectpicker" id="employee_attendance_id" name="employee_attendance_id" data-live-search="true" required>
                                <option value="">Select Employee Name</option> 
                                @foreach ($get_all_employee as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control aiz-selectpicker" id="month_select" class="form-control" name="month_value">
                                <option value="">Select a month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control aiz-selectpicker" id="year_select" class="form-control" name="year_value">
                                <option value="">Select a Year</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
              
                              </select>
                        </div>

                        <div class="form-group">
                            <label for="date">Sales target for this month</label>
                            <input class="form-control " type="number" name="target"> 
                            
                        </div>


                        <input type="submit" class="btn btn-primary" value="Generate Report">


                        @if(session('success'))
                            <div class="alert alert-success" style="margin-top: 20px;">
                                {{ session('success') }}
                            </div>
                        @endif

                    </form>


                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection