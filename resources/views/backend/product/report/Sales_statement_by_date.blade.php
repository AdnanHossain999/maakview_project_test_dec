<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Sales Statement</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f8f9fa;
            /* margin: 20px; */
        }

        .container {
           max-width: 800px;
           margin: 0 auto;
        }

        .select-container {
            margin-top: 20px;
        }

        .select-container select {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ced4da;
            border-radius: 4px;
            box-sizing: border-box;
        }

        #submit_button {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            background-color: #6610f2;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        #submit_button:hover {
        background-color: #6a4fe1;
        }

        .no-print button {
            width: 10%;
            padding: 10px;
            margin-bottom: 20px;
            background-color: rgba(49, 231, 3, 0.747);
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        /* h1 {
        font-size: 24px;
        } */

        .bg-primary {
            background-color: #6610f2;
            color: #fff;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 20px;
        }

        .text-center {
        text-align: center;
        }

        .text-white {
        color: #fff;
        }

        .row {
        display: flex;
        justify-content: center;       
        flex-wrap: wrap;
        }

        .col-md-4 {
        flex-basis: calc(33.333% - 10px);
        margin-bottom: 10px;
        }
  
        .col-md-2 {
        flex-basis: calc(16.666% - 10px);
        margin-bottom: 10px;
        }
        
    
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
            
        }
    
        th, td {
            border: 1px solid #000000;
            padding: 2px;
            text-align: center;
           
        }
    
        th {
            background-color: #f2f2f2;
            font-size: 16px;
            padding: 10px;
        }
    
        .total-row {
            font-weight: bold;
         
            font-size: 18px;
        }

        table tr, td:last-child {
            padding: 10px;
           
        }

        .footer {
            display: none;
        }

</style>

 <style media="print">

    .no-print {
        display: none; 
    } 

    .print-footer{
        display : block;
    }
   
</style> 
</head>
<body>
    
    
   

    <div>

        <div style="text-align:center">
            <button class="btn btn-success no-print mt-1 " onclick="printFun()"><i class="fa fa-print" aria-hidden="true"></i> Print</button> 
        </div>

        <div style="display: flex; justify-content: center;">
            <a href="{{ route('admin.dashboard') }}">
                <img src="https://maakview.com/public/logo/logo.png" width="200" height="50" alt="Logo">
            </a>        
        </div>
        <h1 style="margin-top:20px; display: flex; justify-content: center;font-weight:bold;font-size:40px;">
            Sales Statement Month of {{ DateTime::createFromFormat('!m', $month)->format('F') }} {{ $year }}
        </h1>
        <h1 style="margin-top:20px; display: flex; justify-content: center;font-weight:bold;font-size:25px;">
            Salesman : {{$user->name}}
        </h1>

    </div>

        

    <div>      
        <table>
                <thead style="vertical-align: top;">
                    <tr>
                        <th>S.N.</th>
                        <th>Date</th>
                        <th>Invoice Number</th>
                        <th>Party Name</th>
                        <th>Total Amount</th>
                       
                </thead>
                <tbody >
                    @php
                        $totalSales = 0;
                      
                    @endphp


                    {{-- @foreach($get_all_sales_data as $key => $sales_data)
                        <tr >

                           


                            <td>{{ $key + 1 }}</td>
                            <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ $sales_data->order->created_at }}</td>
                            <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ $sales_data->order->combined_order->code }}</td>
                            <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ $sales_data->order->combined_order->user->name }}</td>
                            <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ $sales_data->order->combined_order->grand_total }}</td>
                            
                           @php
                                $totalSales += $sales_data->order->combined_order->grand_total;
                                // $totalSalesTarget = $sales_data->sales_target
                            @endphp
                        </tr>

                        
                    @endforeach --}}


                    @php
                    $serialNumber = 1;
                    $previousOrderId = null;
                    @endphp
                
                @foreach ($get_all_sales_data as $sales_data)
                    @if ($previousOrderId !== null && $previousOrderId === $sales_data->order->id)
                        {{-- Skip rendering the row --}}
                        @continue
                    @endif
                
                    <tr>
                        <td>{{ $serialNumber++ }}</td>
                        <td style="text-align: center; white-space: nowrap; margin: 2px;">  {{ $sales_data->order->created_at->format('d-m-Y') }}</td>
                        <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ $sales_data->order->combined_order->code }}</td>
                        <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ $sales_data->order->combined_order->user->name }}</td>
                        <td style="text-align: center; white-space: nowrap; margin: 2px;">{{ number_format($sales_data->order->combined_order->grand_total, 2) }}</td>
                
                        @php
                            $totalSales += $sales_data->order->combined_order->grand_total;
                            // $totalSalesTarget = $sales_data->sales_target
                        @endphp
                    </tr>
                
                    @php
                        $previousOrderId = $sales_data->order->id;
                    @endphp
                @endforeach


                    
                   
                    
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold;">Total Sales:</td>
                        <td style="text-align: center; font-weight: bold;">{{ number_format($totalSales, 2) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold;">Total Sales Target:</td>
                        <td style="text-align: center; font-weight: bold;">
                            {{ number_format($sales_target->sales_target ?? 0,2) }}
                        </td>
                    </tr>
                </tbody>
        </table>

    </div>



    <footer class="footer print-footer" style=" width: 100%;text-align: center;">
        <div  style="margin-bottom: 0;margin-top:40px; padding-bottom:0; display:flex; justify-content:space-between;">
            <ul  style="margin-left: 0px;padding-left: 5px; list-style-type:none;">
                <li>..........................................</li>
                <li>Salesman</li>
                <li style="margin-top: 10px">Date: {{ date('d-m-y h:i A') }}</li>
            </ul>
            <ul style="margin-center: 0px;padding-left: 5px; list-style-type:none;">
                <li>..........................................</li>
                <li>Accounts Officer</li>                
            </ul>
           
                <ul style="list-style-type:none; margin-right:0px; padding-left:5px;">
                    <li>.....................................</li>
                    <li>Approved by C.E.O</li>
                </ul>            
        </div>
             
    </footer>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    



    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">
        try {
            
            if( $(document).height() > 1100 ){
                if( $(".check_img").attr("id") == 'unpaid'){
                    $('.footer').css('position','fixed');
                    $('.footer').css('bottom','0');
                    $('.footer').css('clear','both');
                }else{
                    $('.footer').css('position','inherit');
                    $('.footer').css('padding-top','50px');
                    $('.footer').css('clear','both');
                }
            }else{
                $('.footer').css('position','fixed');
                $('.footer').css('bottom','0');
                $('.footer').css('clear','both');
            }

            // this.print();

            this.printFun = () => {
            window.print();
            }
            
    
        } catch (e) {
            window.onload = window.print;
        }
        window.onbeforeprint = function() {
            setTimeout(function() {
                window.close();
            }, 1500);
        }

    </script>

</body>
</html> 