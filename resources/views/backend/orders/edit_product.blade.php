@extends('backend.layouts.app')

@section('content')

<form method="POST" action="{{ route('orders.update', ['order' => $order->id , 'product_id' => $product_id ]) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="date">Edit Order Date:</label>
        <input type="date" id="date" name="date" class="form-control" value="{{ $order->date }}" required>
    </div>

    <div class="form-group">
        <label for="price">Sell Price Per Product</label>
        <input type="number" id="price" name="price" class="form-control" required>
    </div>

    <div class="form-group">
        <label for="qty">Quantity</label>
        <input type="number" id="qty" name="quantity" class="form-control" required>
    </div>


    

    

    <button type="submit" class="btn btn-primary">Update Information</button>
</form>


@endsection