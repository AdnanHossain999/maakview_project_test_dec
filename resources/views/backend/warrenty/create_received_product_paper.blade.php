@extends('backend.layouts.app')

@section('content')
<style>
    body{
        margin: 0;
        box-sizing: border-box;
    }

    .customer_border{
        border: 3px dotted #000000;
        padding-top: 5px;
        margin-bottom: 0px;
    }
    .customer_information li, .supplier_info li{
        list-style-type: none;
    }
    table{
        border: 1px solid black;
        width: 100%;
        border-collapse: collapse;
        text-align: center;
    }
    table ,th ,td{
        border: 1px solid black;
        padding: 10px
    }
    textarea{
        width: 100%;
        height: 100%;
        padding: 10px;
        border: none;
        resize: none;
    }
</style>
<section >
<div class="container" >
 <h2 class="text-center bg-success" style="color: #ffffff">Product Information</h2>
 {{-- customer information  --}}
 <div class="customer-information" >
    <div class="customer_border" style="width:100%; margin-top:0;margin-bottom: 3px;font-size:20px;display: flex;  justify-content: space-between;">
        <div class=" mb-0 mt-0 pb-0  pt-0">
          <ul class="customer_information mb-0 mt-0  pb-0  pt-0" style="margin-left: 0px;padding-left: 5px">
              <li>{{ translate('Customer Name') }}: {{ $order->user->name}}</li>
               @if ($order->billing_address !== null  && !empty($order->billing_address))
                   @php
                   $user_info = json_decode($order->billing_address);
                   @endphp
                   <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                   <li>{{ translate('Address') }}: {{ $user_info->address}},{{ $user_info->city}},{{ $user_info->state}}-{{ $user_info->postal_code}}</li> 
               @else
                <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                <li >{{ translate('Address') }}: @php if(!empty($user_info[0]->address_info)){echo $user_info[0]->address_info->address;} @endphp</li>
              @endif
          </ul>
        </div>
        <div class="supplier-information">
            <ul class="supplier_info">
                <li>{{ translate('Supplier Name') }}: {{ $_get_purchase_details->supplier->name}}</li>
                <li>{{ translate('Supplier phone') }}: {{ $_get_purchase_details->supplier->phone}}</li>
                <li>{{ translate('Supplier address') }}: {{ $_get_purchase_details->supplier->address}}</li>
            </ul>
        </div>
    </div>
 </div>
 {{-- prdouct info --}}
 <div class="product-info" style="margin-top: 30px">
    <form action="{{route('warrenty.store_warrenty_details')}}" method="POST">
    @csrf
     <input type="hidden" name="purchase_details_id" value="{{$purchase_details_id}}">   
     <input type="hidden" name="order_id" value="{{$order_id}}">   
    <table >
        <thead>
            <tr>
                <th>SL</th>
                <th>product Name</th>
                <th>product Serial</th>
                <th>Probelm Details</th>
                <th>Warrenty Number</th>
                <th>Sales Date</th>
                <th>Purchase Date</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
            <tbody>
                <tr>
                    <td>1</td>
                    <input type="hidden" name="product_id" value="{{$_get_products_details->product_id}}">
                    <td>{{getProductName($_get_products_details->product_id)}}</td>
                    <input type="hidden" name="product_serial" value="{{$_product_barcode_id}}">
                    <td>{{$_product_barcode_id}}</td>
                    <td><textarea name="problem_details"  required></textarea></td>
                    <input type="hidden" name="warrenty_numbers" value="{{$warrenty_code}}">
                    <td>{{$warrenty_code}}</td>
                    <td>{{carbon\Carbon::parse($_get_products_details->created_at)->format('d-M-Y')}}</td>
                    <td>{{carbon\Carbon::parse($_get_purchase_details->created_at)->format('d-M-Y')}}</td>
                    <td><textarea name="comments"  ></textarea></td>
                </tr>
            </tbody>
        </tbody>
     </table>
     <button class="mt-2 btn btn-primary btn-lg btn-block">Submit</button>
    </form>
 </div>
</div>


</section>

@endsection


@section('script')
<script type="text/javascript">


</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



@endsection
