@extends('backend.layouts.app')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css"/> 
@section('content')
<style>

</style>
<div id="divName">
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-12">
            <h2 class="bg-primary  text-center" style="color:white;">Product Warrenty List</h2>
            </div>
        </div>
    </div>
    @if (session()->has('warranty_satatus'))
    <div class=" notification alert alert-danger col-md-12">
        {{ session('warranty_satatus') }}
    </div>
@endif
    <div class="card">
     
        <div class="card-body"> 
        <div class="row">
        <div class="col-md-12 pr-3">
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm p-2 text-center" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>product Name</th>
                        <th>product Serial</th>
                        <th>Warrenty Number</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($_get_product_warrenty_list as $key =>  $item)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{getProductName($item->product_id)}}</td>
                        <td>{{$item->product_serial}}</td>
                        <td>{{$item->warrenty_numbers}}</td>
                        <td>
                            <a href="{{route('warrenty.get_received_paper', $item->id)}}" target="_blank" class="btn btn-primary">Received Paper</a>
                            <a href="{{route('warrenty.view_received_paper', $item->id)}}" target="_blank" class="btn btn-primary">View</a>
                            <a href="{{route('warrenty.create_delivery_paper', $item->id)}}" class="btn btn-primary" target="_blank">create Delivery Paper</a>
                            @if ($item->delivery_date !== null)
                            <a href="{{route('warrenty.print_delivery_paper', $item->id)}}" class="btn btn-primary" target="_blank">print Delivery</a>
                            @endif
                            <a href="{{route('warrenty.edit_received', $item->id)}}" class="btn btn-success" >Edit Received</a>
                            <a href="{{route('warrenty.delete_warranty', $item->id)}}" class="btn btn-danger" >Delete warranty</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
        </div>
        </div>
        </div>
    </div>
</div>

@endsection




@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">

$(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');


    var $notification = $('.notification');
  
        setTimeout(function() {
        $notification.fadeOut('slow', function() {
            $notification.remove();
        });
        }, 2000); 

});







</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
