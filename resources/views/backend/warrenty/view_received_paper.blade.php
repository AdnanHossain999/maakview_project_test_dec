<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        .main-table-data{
            width: 80%;
            margin: 0 auto;

        }
        .table-content{
            width: 100%;
            border-collapse: collapse;
        }
        .table-content th, .table-content td{
            border: 1px solid black;
            text-align: center;
            padding: 10px;
        }
        .table-content tbody tr:hover {
            background-color: #ddd;
        }

        .customer_border{
        border: 3px dotted #000000;
        padding-top: 5px;
        margin-bottom: 0px;
    }
    .customer_information li, .supplier_info li{
        list-style-type: none;
    }
    table{
        border: 1px solid black;
        width: 100%;
        border-collapse: collapse;
        text-align: center;
    }
    table ,th ,td{
        border: 1px solid black;
        padding: 10px
    }
    textarea{
        width: 100%;
        height: 100%;
        padding: 10px;
        border: none;
        resize: none;
    }

        
    @media print{
        body{
            -webkit-print-color-adjust: exact;
        }
        .no-print{
            display: none;
        }
        

    }
    </style>
</head>
<body>


<header>
    <div class="maak-title">
        <div style="text-align: center">
            <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png" width="200" height="60" alt="Logo"> 
            <p style="font-size: 20px; font-weight:bold; margin-bottom:0px">www.maakview.com</p>
            <p>Rahima Plaza(6th Floor),82/3 Laboratory Road, Dhaka-1205Phone: +8801888-012727, +8801886-531777, +880244613763</p>
        </div>
    </div>

</header>

<main>
    <div class="container" >
        <h2 class="text-center bg-success" style="color: #ffffff">Warranty Recieved Paper</h2>
        {{-- customer information  --}}
        <div class="customer-information" >
           <div class="customer_border" style="width:100%; margin-top:0;margin-bottom: 3px;font-size:20px;display: flex;  justify-content: space-between;">
               <div class=" mb-0 mt-0 pb-0  pt-0">
                 <ul class="customer_information mb-0 mt-0  pb-0  pt-0" style="margin-left: 0px;padding-left: 5px">
                     <li>{{ translate('Customer Name') }}: {{ $order->user->name}}</li>
                      @if ($order->billing_address !== null  && !empty($order->billing_address))
                          @php
                          $user_info = json_decode($order->billing_address);
                          @endphp
                          <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                          <li>{{ translate('Address') }}: {{ $user_info->address}},{{ $user_info->city}},{{ $user_info->state}}-{{ $user_info->postal_code}}</li> 
                      @else
                       <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                       <li >{{ translate('Address') }}: @php if(!empty($user_info[0]->address_info)){echo $user_info[0]->address_info->address;} @endphp</li>
                     @endif
                 </ul>
               </div>
               <div class="supplier-information">
                   <ul class="supplier_info">
                       <li>{{ translate('Supplier Name') }}: {{ $_get_purchase_details->supplier->name}}</li>
                       <li>{{ translate('Supplier phone') }}: {{ $_get_purchase_details->supplier->phone}}</li>
                       <li>{{ translate('Supplier address') }}: {{ $_get_purchase_details->supplier->address}}</li>
                   </ul>
               </div>
           </div>
        </div>
        {{-- prdouct info --}}
        <div class="product-info" style="margin-top: 30px">

           <table >
               <thead>
                   <tr>
                       <th>SL</th>
                       <th>Product Name</th>
                       <th>Product Serial</th>
                       <th>Probelm Details</th>
                       <th>Warranty Number</th>
                       <th>Comments</th>
                       <th>Warranty Date</th>
                   </tr>
               </thead>
               <tbody>
                   <tbody>
                       <tr>
                           <td>1</td>
                   
                           <td>{{getProductName($_get_purchase_details->product_id)}}</td>
                           
                           <td>{{$get_all_warranty_data->product_serial}}</td>
                           <td>{{$get_all_warranty_data->problem_details}}</td>
                           <td>{{$get_all_warranty_data->warrenty_numbers}}</td>
                           <td>{{$get_all_warranty_data->comments}}</td>
                           <td>{{carbon\Carbon::parse($get_all_warranty_data->created_at)->format('d-M-Y')}}</td>
                       </tr>
                   </tbody>
               </tbody>
            </table>
            <p style="margin-top: 20px; border:3px dotted #000000; padding:10px ">Product Invoice: {{$get_invoice_number->code}} , Sales Date:{{carbon\Carbon::parse($order->created_at)->format('d-M-Y')}}</p>

           
        </div>
       </div>
</main>

    




    











    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">
     

    </script>
</body>
</html>