@extends('backend.layouts.app')

@section('content')
<section >
<div class="container" >
    <h2 class="bg-success  text-center" style="color: #ffffff">Search Warrenty Product Information</h2>
    @if (session()->has('product_status'))
        <div class=" notification alert alert-danger col-md-12 text-center">
            {{ session('product_status') }}
        </div>
    @endif
    <form action="{{route('warrenty.search_warrenty_product')}}" method="POST">
        @csrf
        <div>
            <input type="text" placeholder="Enter Barcode of product" name="product_serial" required class="form-control text-center">
            <button class="btn btn-primary btn-lg btn-block mt-3">Search Product</button>
        </div>
    </form>
</div>

</section>

@endsection


@section('script')
<script type="text/javascript">

$(document).ready(function () {
    var $notification = $('.notification');

    setTimeout(function() {
    $notification.fadeOut('slow', function() {
        $notification.remove();
    });
    }, 2000); 
});

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



@endsection
