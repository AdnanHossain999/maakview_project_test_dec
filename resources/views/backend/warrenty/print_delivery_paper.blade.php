<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        body{
            margin: 0;
            box-sizing: border-box;
        }

        .main-table-data{
            width: 80%;
            margin: 0 auto;

        }
        .table-content{
            width: 100%;
            border-collapse: collapse;
        }
        .table-content th, .table-content td{
            border: 1px solid black;
            text-align: center;
            padding: 10px;
        }
        .table-content tbody tr:hover {
            background-color: #ddd;
        }

        .customer_border{
        border: 3px dotted #000000;
        padding-top: 5px;
        margin-bottom: 0px;
    }
    .customer_information li, .supplier_info li{
        list-style-type: none;
    }
    table{
        border: 1px solid black;
        width: 100%;
        border-collapse: collapse;
        text-align: center;
    }
    table ,th ,td{
        border: 1px solid black;
        padding: 10px
    }
    textarea{
        width: 100%;
        height: 100%;
        padding: 10px;
        border: none;
        resize: none;
    }

        
    @media print{
        body{
            -webkit-print-color-adjust: exact;
        }
        .no-print{
            display: none;
        }
        

    }
    
       
    </style>
</head>
<body>
    <div style="text-align:center">
        <button class="btn btn-success no-print mt-1 " onclick="printFun()"><i class="fa fa-print" aria-hidden="true"></i> Print</button> 
    </div>
    

    <div style="border: 1px dotted #3a3a3a; margin-top:20px; margin-bottom:55px">
        <button style="margin-left: 87%; margin-top:5px; border:none;background:#6A4FE0;color:#fff">Customer Copy</button>
        <div class="logo" style="margin: auto; text-align:center">
            <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png"  height="75" alt="Logo"> 
        </div>
        <div class="text-body" style="margin: auto; text-align:center">
            <p style="margin-bottom: 0;">Address: Rahima Plaza(6th floor), 82/3 Laboratory Road, Dhaka-1205, Email: maakview.info@gmail.com</p>
            <p style="margin-top: 0; padding-top:0">Website: www.maakview.com, Phone: +8801888-012727, +8801886-531777, +880244613763</p>
        </div>
        <div class="money_receipt_title" style="margin: auto; text-align:center">
            <button style="border: none; padding:10px; background-color:#6A4FE0; border-radius:6px; font-weight:bold; color:#fff; width:280px;font-size:20px">Warranty Delivery paper</button>
        </div>
       

        <div class="main_contain" style="margin: auto;width:100%">
            <main style="margin-top: 10px">
                <div class="container" style="padding-left: 0">
                    {{-- customer information  --}}
                    <div class="customer-information" >
                       <div class="customer_border" style="width:100%; margin-top:0;margin-bottom: 3px;font-size:20px;display: flex;  justify-content: space-between;">
                           <div class=" mb-0 mt-0 pb-0  pt-0">
                             <ul class="customer_information mb-0 mt-0  pb-0  pt-0" style="margin-left: 0px;padding-left: 5px">
                                 <li>{{ translate('Customer Name') }}: {{ $order->user->name}}</li>
                                  @if ($order->billing_address !== null  && !empty($order->billing_address))
                                      @php
                                      $user_info = json_decode($order->billing_address);
                                      @endphp
                                      <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                                      <li>{{ translate('Address') }}: {{ $user_info->address}},{{ $user_info->city}},{{ $user_info->state}}-{{ $user_info->postal_code}}</li> 
                                  @else
                                   <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                                   <li >{{ translate('Address') }}: @php if(!empty($user_info[0]->address_info)){echo $user_info[0]->address_info->address;} @endphp</li>
                                 @endif
                             </ul>
                           </div>
                           
                       </div>
                    </div>
                    {{-- prdouct info --}}
                    <div class="product-info" style="margin-top: 30px">
            
                       <table >
                           <thead>
                               <tr>
                                   <th>SL</th>
                                   <th>Product Name</th>
                                   <th>{{$get_all_warranty_data->new_product_id ? 'Prev Product Serial': 'Product Serial'}}</th>
                                   <th>Replace Product Serial</th>
                                   <th>Warranty Number</th>
                                   <th>Comments</th>
                                   <th>Delivery Date</th>
                               </tr>
                           </thead>
                           <tbody>
                               <tbody>
                                   <tr>
                                       <td>1</td>
                               
                                       <td>{{getProductName($_get_purchase_details->product_id)}}</td>
                                       
                                       <td>{{$get_all_warranty_data->product_serial}}</td>
                                       <td>{{$get_all_warranty_data->new_product_id ?? 'No Replace'}}</td>
                                       <td>{{$get_all_warranty_data->warrenty_numbers}}</td>
                                       <td>{{$get_all_warranty_data->delivery_comments}}</td>
                                       <td>{{carbon\Carbon::parse($get_all_warranty_data->delivery_date)->format('d-M-Y')}}</td>
                                   </tr>
                               </tbody>
                           </tbody>
                        </table>
                        <p style="margin-top: 20px; border:3px dotted #000000; padding:10px ">Product Invoice: {{$get_invoice_number->code}} , Sales Date:{{carbon\Carbon::parse($order->created_at)->format('d-M-Y')}}</p>
            
                        <div class="div" style="display: flex; justify-content:space-between">
                            <div class="mb-2" style="margin-left: 0; margin-top:40px" >
                                <ul style="list-style-type:none; margin:0;padding-left:0">
                                    <li>........................................</li>
                                    <li style="font-weight: bold; padding-left:20px">Received By </li>
                                </ul>
                            </div>
                            <div class="mb-2" style="margin-left: 0; margin-top:40px" >
                                <ul style="list-style-type:none; margin:0;padding-left:0">
                                    <li>........................................</li>
                                    <li style="font-weight: bold; padding-left:20px">For Maakview </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   </div>
            </main>
    
        </div>
    </div>
    <hr style="border-top: 1px dashed black;">
    
    {{-- customer copy --}}
    <div  style="border: 1px dotted #3a3a3a; margin-top:60px; margin-bottom:0px">
        <button style="margin-left: 90%; margin-top:5px; border:none;background:#6A4FE0;color:#fff">Office Copy</button>
        <div class="logo" style="margin: auto; text-align:center">
            <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png"  height="75" alt="Logo"> 
        </div>
        <div class="text-body" style="margin: auto; text-align:center">
            <p style="margin-bottom: 0;">Address: Rahima Plaza(6th floor), 82/3 Laboratory Road, Dhaka-1205, Email: maakview.info@gmail.com</p>
            <p style="margin-top: 0; padding-top:0">Website: www.maakview.com, Phone: +8801888-012727, +8801886-531777, +880244613763</p>
        </div>
        <div class="money_receipt_title" style="margin: auto; text-align:center">
            <button style="border: none; padding:10px; background-color:#6A4FE0; border-radius:6px; font-weight:bold; color:#fff; width:280px;font-size:20px">Warranty Delivery paper</button>
        </div>
       

        <div class="main_contain" style="margin: auto;width:100%">
            
            <div class="main_body">
                <main style="margin-top: 10px">
                    <div class="container" style="padding-left: 0">
                        {{-- customer information  --}}
                        <div class="customer-information" >
                           <div class="customer_border" style="width:100%; margin-top:0;margin-bottom: 3px;font-size:20px;display: flex;  justify-content: space-between;">
                               <div class=" mb-0 mt-0 pb-0  pt-0">
                                 <ul class="customer_information mb-0 mt-0  pb-0  pt-0" style="margin-left: 0px;padding-left: 5px">
                                     <li>{{ translate('Customer Name') }}: {{ $order->user->name}}</li>
                                      @if ($order->billing_address !== null  && !empty($order->billing_address))
                                          @php
                                          $user_info = json_decode($order->billing_address);
                                          @endphp
                                          <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                                          <li>{{ translate('Address') }}: {{ $user_info->address}},{{ $user_info->city}},{{ $user_info->state}}-{{ $user_info->postal_code}}</li> 
                                      @else
                                       <li >{{ translate('Customer Phone') }}: {{ $order->user->phone}}</li>
                                       <li >{{ translate('Address') }}: @php if(!empty($user_info[0]->address_info)){echo $user_info[0]->address_info->address;} @endphp</li>
                                     @endif
                                 </ul>
                               </div>
                               
                           </div>
                        </div>
                        {{-- prdouct info --}}
                        <div class="product-info" style="margin-top: 20px">
                
                           <table >
                               <thead>
                                   <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        <th>{{$get_all_warranty_data->new_product_id ? 'Prev Product Serial': 'Product Serial'}}</th>
                                        <th>Replace Product Serial</th>
                                        <th>Warranty Number</th>
                                        <th>Comments</th>
                                        <th>Delivery Date</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tbody>
                                    <tr>
                                        <td>1</td>
                                
                                        <td>{{getProductName($_get_purchase_details->product_id)}}</td>
                                        
                                        <td>{{$get_all_warranty_data->product_serial}}</td>
                                        <td>{{$get_all_warranty_data->new_product_id ?? 'No Replace'}}</td>
                                        <td>{{$get_all_warranty_data->warrenty_numbers}}</td>
                                        <td>{{$get_all_warranty_data->delivery_comments}}</td>
                                        <td>{{carbon\Carbon::parse($get_all_warranty_data->delivery_date)->format('d-M-Y')}}</td>
                                    </tr>
                                   </tbody>
                               </tbody>
                            </table>
                            <p style="margin-top: 20px; border:3px dotted #000000; padding:10px ">Product Invoice: {{$get_invoice_number->code}} , Sales Date:{{carbon\Carbon::parse($order->created_at)->format('d-M-Y')}}</p>
                
                            <div class="div" style="display: flex; justify-content:space-between">
                                <div class="mb-2" style="margin-left: 0; margin-top:40px" >
                                    <ul style="list-style-type:none; margin:0;padding-left:0">
                                        <li>........................................</li>
                                        <li style="font-weight: bold; padding-left:20px">Received By </li>
                                    </ul>
                                </div>
                                <div class="mb-2" style="margin-left: 0; margin-top:40px" >
                                    <ul style="list-style-type:none; margin:0;padding-left:0">
                                        <li>........................................</li>
                                        <li style="font-weight: bold; padding-left:20px">For Maakview </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       </div>
                </main>
            </div>
    
        </div>
    </div>
  



    











    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">

    let printFun = () => {
                window.print();
                
            }

    </script>
</body>
</html>