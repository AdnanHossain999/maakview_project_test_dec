@extends('backend.layouts.app')

@section('content')
<style>
    body{
        margin: 0;
        box-sizing: border-box;
    }

</style>
<section >
<form action="{{route('warrenty.store_delivery_paper')}}" method="POST">
 @csrf
<div class="container" style="display: flex;flex-direction: column; justify-content:center;align-items:center" >
    <h2 class="bg-primary text-center" style="width: 50%; color:#ffffff">Create Delivery Papers</h2>
    <input type="hidden" name="warrenty_id" value="{{$warrenty_id}}">
    <div style="width: 50%; margin-bottom:10px">
        <Label>Add Delivery Date</Label>
        <input type="date" required name="delivery_date" class="form-control">
    </div>
    <div style="width: 50%">
        <Label>Add Replace Product Barcode</Label>
        <input type="text" name="replace_product_barcode" class="form-control">
    </div>
    <div style="width: 50%;margin-bottom:10px">
        <Label>Add Commetns</Label>
        <textarea style="resize: none" name="delivery_comments" id="" class="form-control"></textarea>
    </div>
    <button class="btn btn-primary btn-lg btn-block" style="width: 50%">Submit</button>
        
</div>
</form>


</section>

@endsection


@section('script')
<script type="text/javascript">


</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



@endsection
