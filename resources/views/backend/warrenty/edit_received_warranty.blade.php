@extends('backend.layouts.app')

@section('content')
<style>
    body{
        margin: 0;
        box-sizing: border-box;
    }

</style>
<section >
<form action="{{route('warrenty.update_received')}}" method="POST">
 @csrf
<div class="container" style="display: flex;flex-direction: column; justify-content:center;align-items:center" >
    <h2 class="bg-primary text-center" style="width: 50%; color:#ffffff">Update Received Papers</h2>
    <input type="hidden" name="warrenty_id" value="{{$warrenty_id}}">
    <div style="width: 50%;margin-bottom:10px">
        <Label>Product Problem Details</Label>
        <textarea style="resize: none" name="problem_details" id="" class="form-control">{{$received_warranty_data->problem_details}}</textarea>
    </div>
    <div style="width: 50%;margin-bottom:10px">
        <Label>Received Comments</Label>
        <textarea style="resize: none" name="comments" id="" class="form-control">{{$received_warranty_data->comments}}</textarea>
    </div>
    <button class="btn btn-primary btn-lg btn-block" style="width: 50%">Update</button>
        
</div>
</form>


</section>

@endsection


@section('script')
<script type="text/javascript">


</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



@endsection
