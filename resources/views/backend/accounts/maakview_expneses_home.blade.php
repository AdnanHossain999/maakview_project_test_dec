@extends('backend.layouts.app')

@section('content')
<style>
    .custom-width { 
        width: 400px !important; /* Adjust the width as needed */
    }
</style>
<section >
    <div class="container">
        <form action="{{route('accounts.maakview_ledger_expenses_input_store')}}" method="POST">
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div >
                        @if (session()->has('expenses_statsu'))
                        <div class=" notification alert alert-danger text-center col-md-12">
                            {{ session('expenses_statsu') }}
                        </div>
                        @endif
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="mb-3">
                            <label for="date" class="form-label">Date</label>
                            <input type="date" class="form-control custom-width" id="date" name="expenses_date" required>
                        </div>
                       
                        <div class="mb-3">
                            <label for="expenseCategory" class="form-label">Select Expense Category</label><br>
                            <select  class="form-control custom-width aiz-selectpicker" id="expenseCategory"  data-live-search="true" name="expenses_type" required >
                                <option value="">Select Expenses</option>
                                <option value="office_rent">Office Rent</option>
                                <option value="warehouse_rent">Warehouse Rent</option>
                                <option value="office_utilites">Office Utilities</option>
                                <option value="marketing_online">Marketing Online</option>
                                <option value="advertisement">Advertisement</option>
                                <option value="business_promotion">Business Promotion</option>
                                <option value="ip_phone_bill">IP Phone Bill</option>
                                <option value="mobile_bill">Mobile Bill</option>
                                <option value="festibal_allowance">Festival Allowance</option>
                                <option value="bridge_toll_and_parking">Bridge Toll and Parking</option>
                                <option value="gift_and_donation">Gift and Donation</option>
                                <option value="electric_goods">Electric Goods</option>
                                <option value="entertainment_expenses">Entertainment Expenses</option>
                                <option value="fuel_oil">Fuel Oil</option>
                                <option value="internet_and_telephone_line">Internet and Telephone Line</option>
                                <option value="license_and_fees">License and Fees</option>
                                <option value="medical_expenses">Medical Expenses</option>
                                <option value="office_maintenance">Office Maintenance</option>
                                <option value="printing_expenses">Printing Expenses</option>
                                <option value="stationary_expenses">Stationery Expenses</option>
                                <option value="electricity_bill">Electricity Bill</option>
                                <option value="repair_and_maintenance_others">Repair and Maintenance Others</option>
                                <option value="accessories_expenses">Accessories Expenses</option>
                                <option value="carrying_expenses">Carrying Expenses</option>
                                <option value="product_damage_and_lost_expenses">Product Damage and Lost Expenses</option>
                                <option value="fine_and_penalty">Fine and Penalty</option>
                                <option value="load_and_unload_labour_bill">Load and Unload Labour Bill</option>
                                <option value="lunch_tiffin">Lunch and Tiffin</option>
                                <option value="postage_and_courier">Postage and Courier</option>
                                <option value="employee_loan_and_other_payments">Employee Loan and Other Payments</option>
                                <option value="legal_cost">Legal Cost</option>
                                <option value="monthly_incentive_payments">Monthly Incentive Payments</option>
                                <option value="website_expenses">Website Expenses</option>
                                <option value="bank_interest_all">Bank Interest All</option>
                                <option value="miscellaneous_expenses">Miscellaneous Expenses</option>
                                <option value="vat_tax">VAT Tax</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="expense_value" class="form-label">Expenses values</label>
                            <input type="number" class="form-control custom-width" id="expense_value" name="expenses_value" required>
                        </div>
                        <div class="mb-3">
                            <input id="submit_button" type="submit" class="btn btn-primary custom-width" >
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
