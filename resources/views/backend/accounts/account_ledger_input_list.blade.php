@extends('backend.layouts.app')

@section('content')
<div class="cotnainer">
    <h2 class="bg-primary text-center text-white">ALL LEDGER MONTHLY INPUT LIST</h2>
    <div class="row align-items-center">
        <div class="col-md-12">
            @if (session()->has('update_ledger_data'))
            <div class=" notification alert alert-success col-md-12">
                {{ session('update_ledger_data') }}
            </div>
            @endif
            @if (session()->has('delete_ledger_data'))
            <div class=" notification alert alert-danger col-md-12">
                {{ session('delete_ledger_data') }}
            </div>
            @endif
        </div>
    </div>
    <table class="table table-bordered table-responsive">
        <thead>
          <tr class="text-center">
            <th scope="col">#</th>
            <th scope="col">MONTH</th>
            <th scope="col">YEAR</th>
            <th scope="col">OFFICE RENT</th>
            <th scope="col">WAREHOUSE RENT</th>
            <th scope="col">OFFICE UITILIES</th>
            <th scope="col">MARKETING ONLINE</th>
            <th scope="col">ADVERTISEMENT</th>
            <th scope="col">BUSINESS PROMOTION</th>
            <th scope="col">IP PHONE BILL</th>
            <th scope="col">MOBILE BILL</th>
            <th scope="col">FESTIBAL ALLOWANCE</th>
            <th scope="col">BRIDGE TOLL AND PARKING</th>
            <th scope="col">GIFT AND DONATION</th>
            <th scope="col">ELECTRIC GOODS</th>
            <th scope="col">ENTERTAINMENT EXPENSES</th>
            <th scope="col">FUEL OIL</th>
            <th scope="col">INERNET AND TELEPHONE LINE</th>
            <th scope="col">LICENSE AND FEES</th>
            <th scope="col">MEDICAL EXPENSES</th>
            <th scope="col">OFFICE MAINTENANCE</th>
            <th scope="col">PRINTING_EXPENSES</th>
            <th scope="col">STATINONARY EXPNESES</th>
            <th scope="col">ELECTRICITY BILL</th>
            <th scope="col">REPAIR AND MAINTENANCE OTHERS</th>
            <th scope="col">ACCESSORIES EXPENSES</th>
            <th scope="col">CARRING EXPENSES</th>
            <th scope="col">PRODUCT DAMAGE AND LOST EXPENSES</th>
            <th scope="col">FINE AND PENALTY</th>
            <th scope="col">LOAD AND UNLOAD LABOUR BILL</th>
            <th scope="col">LUNCH TIFFIN</th>
            <th scope="col">POSTAGE AND COURIER</th>
            <th scope="col">EMPLOYEE LOAN AND OTHER PAYMENTS</th>
            <th scope="col">LEGAL COST</th>
            <th scope="col">MONTHLY INCENTIVE PAYMENTS</th>
            <th scope="col">WEBSITE EXPENSES</th>
            <th scope="col">BANK INTEREST ALL</th>
            <th scope="col">MISCELLANEOUS EXPENSES</th>
            <th scope="col">VAT TAX</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($get_all_ledger_report as $key => $item)
          <tr class="text-center">
            <th scope="row">{{$key + 1}}</th>
            <td>{{ date("F", strtotime($item->date))}}</td>
            <td>{{date("Y", strtotime($item->date))}}</td>
            <td>{{$item->office_rent}}</td>
            <td>{{$item->warehouse_rent}}</td>
            <td>{{$item->office_utilites}}</td>
            <td>{{$item->marketing_online}}</td>
            <td>{{$item->advertisement}}</td>
            <td>{{$item->business_promotion}}</td>
            <td>{{$item->ip_phone_bill}}</td>
            <td>{{$item->mobile_bill}}</td>
            <td>{{$item->festibal_allowance}}</td>
            <td>{{$item->bridge_toll_and_parking}}</td>
            <td>{{$item->gift_and_donation}}</td>
            <td>{{$item->electric_goods}}</td>
            <td>{{$item->entertainment_expenses}}</td>
            <td>{{$item->fuel_oil}}</td>
            <td>{{$item->inernet_and_telephone_line}}</td>
            <td>{{$item->license_and_fees}}</td>
            <td>{{$item->medical_expenses}}</td>
            <td>{{$item->office_maintenance}}</td>
            <td>{{$item->printing_expenses}}</td>
            <td>{{$item->statinonary_expneses}}</td>
            <td>{{$item->electricity_bill}}</td>
            <td>{{$item->repair_and_maintenance_others}}</td>
            <td>{{$item->accessories_expenses}}</td>
            <td>{{$item->carring_expenses}}</td>
            <td>{{$item->product_damage_and_lost_expenses}}</td>
            <td>{{$item->fine_and_penalty}}</td>
            <td>{{$item->load_and_unload_labour_bill}}</td>
            <td>{{$item->lunch_tiffin}}</td>
            <td>{{$item->postage_and_courier}}</td>
            <td>{{$item->employee_loan_and_other_payments}}</td>
            <td>{{$item->legal_cost}}</td>
            <td>{{$item->monthly_incentive_payments}}</td>
            <td>{{$item->website_expenses}}</td>
            <td>{{$item->bank_interest_all}}</td>
            <td>{{$item->miscellaneous_expenses}}</td>
            <td>{{$item->vat_tax}}</td>
            <td class="text-center">
                <a href="{{route('accounts.maakview_expenses_daily_input_edit', $item->id)}}" class="btn btn-sm btn-primary ">Edit</a>
                {{-- <a href="{{route('accounts.maakview_ledger_monthly_input_delete', $item->id)}}" class="btn btn-sm btn-danger">Delete</a> --}}
            </td>
         
          </tr>
          @endforeach
          
        </tbody>
      </table>
</div>

@endsection


@section('script')
<script type="text/javascript">


//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}




</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
