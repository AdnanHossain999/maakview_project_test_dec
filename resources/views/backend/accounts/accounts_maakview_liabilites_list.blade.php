@extends('backend.layouts.app')

@section('content')
<style>
   
</style>
<section >
    <div class="container ">
        <div >
            @if (session()->has('udpate_status'))
            <div class=" notification alert alert-success text-center col-md-12">
                {{ session('udpate_status') }}
            </div>
            @endif
        </div>
        <div >
            @if (session()->has('delete_status'))
            <div class=" notification alert alert-success text-center col-md-12">
                {{ session('delete_status') }}
            </div>
            @endif
        </div>
        <table class="table table-bordered text-center">
            <thead>
                <th>Date</th>
                <th>Accounts Payable</th>
                <th>Accounts Loan Payable</th>
                <th>Account Accured Expenses</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($all_accountsLiabilities as $item)
                    <tr>
                        <td>{{$item->date}}</td>
                        <td>{{$item->accounts_payable}}</td>
                        <td>{{$item->Loans_payable}}</td>
                        <td>{{$item->accured_expenses}}</td>
                        <td>
                            <a href="{{route('accounts.maakview_monthly_accounts_libilites_edit', $item->id)}}" class="btn btn-sm btn-primary">Edit</a>
                            <a href="{{route('accounts.maakview_monthly_accounts_libilites_delete', $item->id)}}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}


</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
