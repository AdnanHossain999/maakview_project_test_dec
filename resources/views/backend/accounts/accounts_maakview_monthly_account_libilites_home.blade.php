@extends('backend.layouts.app')

@section('content')
<style>
    .custom-width { 
        width: 400px !important; /* Adjust the width as needed */
    }
</style>
<section >
  @extends('backend.layouts.app')

  @section('content')
  <style>
      .custom-width { 
          width: 400px !important; /* Adjust the width as needed */
      }
  </style>
  <section >
      <div class="container">
          <form action="{{route('accounts.maakview_monthly_accounts_libilites_store')}}" method="POST">
              @csrf
              <h2 class="text-center bg-primary text-white mb-3">Select Month And Year For Accounts Liabilities Input</h2>
              <div class="row justify-content-center">
                  <div class="col-md-8">
                      <div >
                          @if (session()->has('liabilites_status'))
                          <div class=" notification alert alert-danger text-center col-md-12">
                              {{ session('liabilites_status') }}
                          </div>
                          @endif
                      </div>
                      <div class="d-flex flex-column align-items-center">
                          <div class="mb-3">
                              <select id="month_select" class="form-control custom-width" name="month_value">
                                  <option value="">Select a month</option>
                                  <option value="1">January</option>
                                  <option value="2">February</option>
                                  <option value="3">March</option>
                                  <option value="4">April</option>
                                  <option value="5">May</option>
                                  <option value="6">June</option>
                                  <option value="7">July</option>
                                  <option value="8">August</option>
                                  <option value="9">September</option>
                                  <option value="10">October</option>
                                  <option value="11">November</option>
                                  <option value="12">December</option>
                                </select>
                          </div>
                          <div class="mb-3">
                              <select id="year_select" class="form-control custom-width" name="year_value">
                                  <option value="">Select a Year</option>
                                  <option value="2021">2021</option>
                                  <option value="2022">2022</option>
                                  <option value="2023">2023</option>
                                  <option value="2024">2024</option>
                                  <option value="2025">2025</option>
                                  <option value="2026">2026</option>
                                  <option value="2027">2027</option>
                                  <option value="2028">2028</option>
                                  <option value="2029">2029</option>
                                  <option value="2030">2030</option>
                
                                </select>
                          </div>
                          <div class="mb-3">
                              <label for="">Accounts Payable</label>
                              <input type="number"  class="form-control custom-width" required name="accounts_payable">
                          </div>
                          <div class="mb-3">
                              <label for="">Loans Payable</label>
                              <input type="text"  class="form-control custom-width" name="Loans_payable">
                          </div>
                          <div class="mb-3">
                              <label for="">Accured Expenses</label>
                              <input type="number"  class="form-control custom-width" name="accured_expenses">
                          </div>
                                           
                          <div class="mb-3">
                              <input id="submit_button"  type="submit" class="btn btn-primary custom-width" disabled >
                          </div>
  
                      </div>
                  </div>
              </div>
          </form>
      </div>
  </section>
  
  @endsection
  
  
  @section('script')
  <script type="text/javascript">
  //remove notification after save data to db
  removeNotification();
  function removeNotification(){
    setTimeout(() => {
      $('.notification').remove();
    }, 3000);
  }
  
  $('#month_select').change(function (e) { 
      e.preventDefault();
      $month_value = e.target.value;
      $year_value = $('#year_select').val();
  
      if($month_value !== "" && $year_value !== "")
      {
          $('#submit_button').prop('disabled', false);
      }else{
          $('#submit_button').prop('disabled', true);
      }
      
  });
  
  $('#year_select').change(function (e) { 
      e.preventDefault();
      $month_value = e.target.value;
      $year_value = $('#year_select').val();
  
      if($month_value !== "" && $year_value !== "")
      {
          $('#submit_button').prop('disabled', false);
      }else{
          $('#submit_button').prop('disabled', true);
      }
      
  });
  
  
  </script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  @endsection
  
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
