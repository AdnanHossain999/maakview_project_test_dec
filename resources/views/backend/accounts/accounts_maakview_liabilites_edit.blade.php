@extends('backend.layouts.app')

@section('content')
<style>
    .custom-width { 
        width: 400px !important; /* Adjust the width as needed */
    }
</style>
<section >
  @extends('backend.layouts.app')

  @section('content')
  <style>
      .custom-width { 
          width: 400px !important; /* Adjust the width as needed */
      }
  </style>
  <section >
      <div class="container">
          <form action="{{route('accounts.maakview_monthly_accounts_libilites_update', $maakview_single_accountsLiabilities->id)}}" method="POST">
              @csrf
              <h2 class="text-center bg-primary text-white mb-3">Select Month And Year For Accounts Liabilities Input</h2>
              <div class="row justify-content-center">
                  <div class="col-md-8">
                      <div >
                          @if (session()->has('liabilites_status'))
                          <div class=" notification alert alert-danger text-center col-md-12">
                              {{ session('liabilites_status') }}
                          </div>
                          @endif
                      </div>
                      <div class="d-flex flex-column align-items-center">
                          <div class="mb-3">
                              <label for="">Accounts Payable</label>
                              <input type="number"  class="form-control custom-width" required name="accounts_payable" value="{{$maakview_single_accountsLiabilities->accounts_payable}}">
                          </div>
                          <div class="mb-3">
                            <label for="">Loans Payable</label>
                            <input type="text"  class="form-control custom-width" name="Loans_payable" value="{{$maakview_single_accountsLiabilities->Loans_payable}}">
                        </div>
                        <div class="mb-3">
                            <label for="">Accured Expenses</label>
                            <input type="number"  class="form-control custom-width" name="accured_expenses" value="{{$maakview_single_accountsLiabilities->accured_expenses}}">
                        </div>
                                           
                          <div class="mb-3">
                            <button id="submit_button"  type="submit" class="btn btn-primary custom-width">Update</button>
                          </div>
  
                      </div>
                  </div>
              </div>
          </form>
      </div>
  </section>
  
  @endsection
  
  
  @section('script')
  <script type="text/javascript">
  //remove notification after save data to db
  removeNotification();
  function removeNotification(){
    setTimeout(() => {
      $('.notification').remove();
    }, 3000);
  }
  

  
  
  </script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  @endsection
  
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
