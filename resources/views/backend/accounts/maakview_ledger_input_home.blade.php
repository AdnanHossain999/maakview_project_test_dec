@extends('backend.layouts.app')

@section('content')
<section >
    <div class="container">
       <form action="{{route('accounts.maakview_ledger_monthly_input_store')}}" method="POST">
        @csrf
        <input type="hidden" name="month" value="{{$month_value}}">
        <input type="hidden" name="year" value="{{$year_value}}">
        <div class="row p-3">
            <div class="col-md-12 text-center">
                <h5 style=" text-decoration: underline;">Maakview Expenses Input</h5>
            </div>
            <div class="col-md-2">
                <label for="">Office Rent</label>
                <input type="number" class="form-control" name="office_rent">
            </div>
            <div class="col-md-2">
                <label for="">Warehouse Rent</label>
                <input type="number" class="form-control" name="warehouse_rent">
            </div>
            <div class="col-md-2">
                <label for="">Office Utilites</label>
                <input type="number" class="form-control" name="office_utilites">
            </div>
            <div class="col-md-2">
                <label for="">Marketing Online</label>
                <input type="number" class="form-control" name="marketing_online">
            </div>
            <div class="col-md-2">
                <label for="">Advertisement</label>
                <input type="number" class="form-control" name="advertisement">
            </div>
            <div class="col-md-2">
                <label for="">Business Promotion</label>
                <input type="number" class="form-control" name="business_promotion">
            </div>
            <div class="col-md-2">
                <label for="">IP Phone Bill</label>
                <input type="number" class="form-control" name="ip_phone_bill">
            </div>
            <div class="col-md-2">
                <label for="">Mobile Bill</label>
                <input type="number" class="form-control" name="mobile_bill">
            </div>
            <div class="col-md-2">
                <label for="">Festiable Allowance</label>
                <input type="number" class="form-control" name="festibal_allowance">
            </div>
            <div class="col-md-2">
                <label for="">Bridge Toll and Parking</label>
                <input type="number" class="form-control" name="bridge_toll_and_parking">
            </div>
            <div class="col-md-2">
                <label for="">Gift and Donation</label>
                <input type="number" class="form-control" name="gift_and_donation">
            </div>
            <div class="col-md-2">
                <label for="">Electric Goods</label>
                <input type="number" class="form-control" name="electric_goods">
            </div>
            <div class="col-md-2">
                <label for="">Entertainment Expenses</label>
                <input type="number" class="form-control" name="entertainment_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Fuel Oil</label>
                <input type="number" class="form-control" name="fuel_oil">
            </div>
            <div class="col-md-2">
                <label for="">Internet and Telephone Line</label>
                <input type="number" class="form-control" name="inernet_and_telephone_line">
            </div>
            <div class="col-md-2">
                <label for="">License and Fees</label>
                <input type="number" class="form-control" name="license_and_fees">
            </div>
            <div class="col-md-2">
                <label for="">medical Expenses</label>
                <input type="number" class="form-control" name="medical_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Office Maintenance</label>
                <input type="number" class="form-control" name="office_maintenance">
            </div>
            <div class="col-md-2">
                <label for="">Printing Expenses</label>
                <input type="number" class="form-control" name="printing_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Stationary Expenses</label>
                <input type="number" class="form-control" name="statinonary_expneses">
            </div>
            <div class="col-md-2">
                <label for="">Electricity Bill</label>
                <input type="number" class="form-control" name="electricity_bill">
            </div>
            <div class="col-md-2">
                <label for="">Repair Maintenance Others</label>
                <input type="number" class="form-control" name="repair_and_maintenance_others">
            </div>
            <div class="col-md-2">
                <label for="">Accessories Expenses</label>
                <input type="number" class="form-control" name="accessories_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Caring Expenses</label>
                <input type="number" class="form-control" name="carring_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Product Damage and Lost Expenses</label>
                <input type="number" class="form-control" name="product_damage_and_lost_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Fine and Penalty</label>
                <input type="number" class="form-control" name="fine_and_penalty">
            </div>
            <div class="col-md-2">
                <label for="">Load and Unlod labour Bill</label>
                <input type="number" class="form-control" name="load_and_unload_labour_bill">
            </div>
            <div class="col-md-2">
                <label for="">Lunch and Tiffin</label>
                <input type="number" class="form-control" name="lunch_tiffin">
            </div>
            <div class="col-md-2">
                <label for="">Postage and courier</label>
                <input type="number" class="form-control" name="postage_and_courier">
            </div>
            <div class="col-md-2">
                <label for="">Employee Loand and other Payments</label>
                <input type="number" class="form-control" name="employee_loan_and_other_payments">
            </div>
            <div class="col-md-2">
                <label for="">Legal Cost</label>
                <input type="number" class="form-control" name="legal_cost">
            </div>
            <div class="col-md-2">
                <label for="">Monthly Incentive Payments</label>
                <input type="number" class="form-control" name="monthly_incentive_payments">
            </div>
            <div class="col-md-2">
                <label for="">Website Expenses</label>
                <input type="number" class="form-control" name="website_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Bank Interest All</label>
                <input type="number" class="form-control" name="bank_interest_all">
            </div>
            <div class="col-md-2">
                <label for="">Miscellaneous Expenses</label>
                <input type="number" class="form-control" name="miscellaneous_expenses">
            </div>
            <div class="col-md-2">
                <label for="">Vat Tax</label>
                <input type="number" class="form-control" name="vat_tax">
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block" style="margin-left: 13px; width:98%">Submit</button>
       </form>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
$('#month_select').change(function (e) { 
    e.preventDefault();
    $month_value = e.target.value;
    $year_value = $('#year_select').val();

    if($month_value !== "" && $year_value !== "")
    {
        $('#submit_button').prop('disabled', false);
    }else{
        $('#submit_button').prop('disabled', true);
    }
    
});

$('#year_select').change(function (e) { 
    e.preventDefault();
    $month_value = e.target.value;
    $year_value = $('#year_select').val();

    if($month_value !== "" && $year_value !== "")
    {
        $('#submit_button').prop('disabled', false);
    }else{
        $('#submit_button').prop('disabled', true);
    }
    
});







</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
