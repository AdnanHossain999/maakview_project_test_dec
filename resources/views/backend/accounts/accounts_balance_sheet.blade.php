@extends('backend.layouts.app')

@section('content')
<section class="accounts_balance_sheet">
      <table class="table table-bordered text-center">
        <thead>
          <tr>
            <th colspan="3">Maakview Balance Sheet</th>
          </tr>
          <tr>
            <th colspan="3">Balance Sheet (for the month of July 2023)</th>
          </tr>
        </thead>
        <tbody>
          {{-- Assets --}}
          <tr>
            <th colspan="3">Assets:</th>
          </tr>
          <tr>
            <th colspan="3">Current Assets:</th>
          </tr>
          <tr>
            <th colspan="2">Cash and Cash Equivalents</th>
            <td>500000</td>
          </tr>
          <tr>
            <th colspan="2">Accounts Receivable</th>
            <td>300000</td>
            
          </tr>
          <tr>
            <th colspan="2">Inventory</th>
            <td>450000</td>
          </tr>
          <tr>
            <th colspan="2">Prepaid Expenses</th>
            <td>20000</td>
          </tr>
          <tr>
            <th class="table-secondary" colspan="2">Total Current Assets:</th>
            <th class="table-secondary">1270000</th>
          </tr>
          {{-- @foreach ($customer_wise_total_sales as $item)
          <tr>
            <td>{{$item->formatted_created_at}}</td>
            <td>{{$item->user_id}}</td>
            <td>{{$item->grand_total}}</td>
          </tr>
          @endforeach --}}

          {{-- Non-Current Assets --}}
          <tr>
            <th colspan="2">Non-Current Assets:</th>
            <th>1270000</th>
          </tr>
          <tr>
            <th>Property, Plan and Equipment</th>
            <th>Intangible Assets</th>
            <th>Investments</th>
          </tr>
          <tr>
            <td>1200000</td>
            <td>50000</td>
            <td>150000</td>
          </tr>
          <tr>
            <th class="table-secondary" colspan="2">Total Non-Current Assets:</th>
            <th class="table-secondary">1850000</th>
          </tr>
          <tr>
            <th class="table-dark" colspan="2">Total Assets:</th>
            <th class="table-dark">3120000</th>
          </tr>

          {{-- Liabilities and Equity --}}
          <tr>
            <th colspan="3">Liabilities and Equity:</th>
          </tr>
          <tr>
            <th colspan="3">Current Liabilities:</th>
          </tr>
          <tr>
            <td colspan="2">Accounts Payable</td>
            <td>1500</td>
          </tr>
          <tr>
            <td colspan="2">Loans Payable</td>
            <td>700</td>
          </tr>
          <tr>
            <td colspan="2">Accrued Expenses</td>
            <td>1200</td>
          </tr>
          <tr>
            <th class="table-secondary" colspan="2">Total Current Liabilities:</th>
            <td class="table-secondary">3400 BDT</td>
          </tr>
          {{-- Equity --}}
          <tr>
            <th colspan="3">Non-Current Liabilities:</th>
          </tr>
          <tr>
            <td colspan="2">Long-Term Deb</td>
            <td>800000</td>
          </tr>
          <tr>
            <td colspan="2">Deferred Tax Liabilities</td>
            <td>100000</td>
          </tr>
          <tr>
            <th class="table-secondary" colspan="2">Total Non-Current Liabilities</th>
            <th class="table-secondary">900000</th>
          </tr>
          <tr>
            <th class="table-dark" colspan="2">Total Liabilities</th>
            <th class="table-dark">130000</th>
          </tr>

        </tbody>
      </table>
</section>

@endsection


@section('script')
<script type="text/javascript">







</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
