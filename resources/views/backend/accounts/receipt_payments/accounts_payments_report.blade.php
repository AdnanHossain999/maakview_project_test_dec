@extends('backend.layouts.app')

@section('content')
<section class="accounts_ledger_report">
      <table class="table table-bordered text-center">
        <thead>
          <tr>
            <th colspan="5" style="font-size: 30px;">Maakview Report on Payments</th>
          </tr>
          <tr>
            <th colspan="5" style="font-size: 20px;">Payments for the Month of {{ DateTime::createFromFormat('!m', $currentMonth)->format('F') }}</th>
          </tr>
        </thead>
        <tbody>

         

          {{-- Payments --}}

          {{-- Start Capital Payments Section --}}
          <tr>
            <th colspan="5">Capital Payments:</th>
            
          </tr>
          <tr>
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_libilities = 0;
          $totalCurrentMonth_libilities = 0;
          $totalUptoDate_libilities = 0;
          @endphp
          
        

         



        {{-- @foreach ($customer_wise_total_liabilities_prev as $prevRecord)
        <tr>
            <td>{{ $prevRecord->description }}</td>
            <td>{{ $prevRecord->amount }}</td>
    
            @php $currentAmount = null; @endphp
    
            @foreach ($customer_wise_total_liabilities_current as $current)
                @if ($current->description === $prevRecord->description)
                    @php $currentAmount = $current->amount; @endphp
                    <td>{{ $currentAmount }}</td>
                   
                    @break
                @endif
            @endforeach
    
            @foreach ($customer_wise_total_liabilities_upto_date as $upToDateRecord)
                @if ($upToDateRecord->description === $prevRecord->description)
                    @php
                        $totalAmount_liabilities = $prevRecord->amount + $current->amount ;
                    @endphp
                    <td>{{ $totalAmount_liabilities }}</td>
                   
                    
                    @break
                @endif
            @endforeach
        </tr>
        @php
           $totalUptoPrevMonth_libilities += $prevRecord->amount;
       @endphp
    @endforeach


    
    @foreach ($customer_wise_total_liabilities_current as $currentRecord)
        @if (!$customer_wise_total_liabilities_prev->contains('description', $currentRecord->description))
            <tr>
                <td>{{ $currentRecord->description }}</td>
                <td></td>
                <td>{{ $currentRecord->amount }}</td>
    
                 @foreach ($customer_wise_total_liabilities_upto_date as $upToDateRecord)
                    @if ($upToDateRecord->description === $currentRecord->description) 
                        <td>{{ $upToDateRecord->amount }}</td>

                        @php
                        $totalUptoDate_libilities += $upToDateRecord->amount;
                        @endphp
                         @break
                    @endif
                @endforeach
            </tr>
        @endif

        @php
        $totalCurrentMonth_libilities += $currentRecord->amount;
        @endphp
    @endforeach --}}



    @foreach ($customer_wise_total_liabilities_prev as $prevRecord)
                <tr>
                    <td>{{ $prevRecord->description }}</td>
                    <td>{{ $prevRecord->amount }}</td>

                    @php
                        $currentRecord = $customer_wise_total_liabilities_current
                            ->firstWhere('description', $prevRecord->description);
                        $currentAmount = $currentRecord ? $currentRecord->amount : 0;
                    @endphp

                    <td>{{ $currentAmount }}</td>

                    @php
                        $upToDateRecord = $customer_wise_total_liabilities_upto_date
                            ->firstWhere('description', $prevRecord->description);
                        $totalAmount_liabilities = $prevRecord->amount + $currentAmount;
                       
                       
                    @endphp

                    <td>{{ $totalAmount_liabilities }}</td>
                   
                </tr>

                @php
                    $totalUptoPrevMonth_libilities += $prevRecord->amount;
                @endphp
            @endforeach

            

            @foreach ($customer_wise_total_liabilities_current as $currentRecord)
                @if (!$customer_wise_total_liabilities_prev->contains('description', $currentRecord->description))
                    <tr>
                        <td>{{ $currentRecord->description }}</td>
                        <td></td>
                        <td>{{ $currentRecord->amount }}</td>

                        @php
                            $upToDateRecord = $customer_wise_total_liabilities_upto_date
                                ->firstWhere('description', $currentRecord->description);
                        @endphp

                        <td>{{$upToDateRecord ? $upToDateRecord->amount : 0}}</td>

                        @php
                        $totalUptoDate_libilities +=  $upToDateRecord->amount;
                        @endphp
                    </tr>
                @endif

                @php
                    $totalCurrentMonth_libilities += $currentRecord->amount;
                @endphp
            @endforeach






          


          {{-- Loans --}}

           @php
          $totalUptoPrevMonth_loan = 0;
          $totalCurrentMonth_loan = 0;
          $totalUptoDate_loan = 0;
          @endphp
        
 


              {{-- @foreach ($customer_wise_total_loans_prev as $prevRecord)
            <tr>
                <td>{{ $prevRecord->description }}</td>
                <td>{{ $prevRecord->amount }}</td>
        
                @php $currentAmount = null; @endphp
        
                @foreach ($customer_wise_total_loans_current as $current)
                    @if ($current->description === $prevRecord->description)
                        @php $currentAmount = $current->amount; @endphp
                        <td>{{ $currentAmount }}</td>
                      
                        @break
                    @endif
                @endforeach
        
                @foreach ($customer_wise_total_loans_upto_date as $upToDateRecord)
                    @if ($upToDateRecord->description === $prevRecord->description)
                        @php
                            $totalAmount_loans = $prevRecord->amount + $current->amount ;
                        @endphp
                        <td>{{ $totalAmount_loans }}</td>
                       
                        
                        @break
                    @endif
                @endforeach
            </tr>
            @php
              $totalUptoPrevMonth_loan += $prevRecord->amount;
          @endphp
        @endforeach


        
        @foreach ($customer_wise_total_loans_current as $currentRecord)
            @if (!$customer_wise_total_loans_prev->contains('description', $currentRecord->description))
                <tr>
                    <td>{{ $currentRecord->description }}</td>
                    <td></td>
                    <td>{{ $currentRecord->amount }}</td>
        
                    @foreach ($customer_wise_total_loans_upto_date as $upToDateRecord)
                        @if ($upToDateRecord->description === $currentRecord->description) 
                            <td>{{ $upToDateRecord->amount }}</td>


                            @php
                            $totalUptoDate_loan += $upToDateRecord->amount;
                            @endphp
                            @break
                        @endif
                    @endforeach
                </tr>
            @endif

            @php
            $totalCurrentMonth_loan += $currentRecord->amount;
            @endphp
        @endforeach --}}





        @foreach ($customer_wise_total_loans_prev as $prevRecord)
                <tr>
                    <td>{{ $prevRecord->description }}</td>
                    <td>{{ $prevRecord->amount }}</td>

                    @php
                        $currentRecord = $customer_wise_total_loans_current
                            ->firstWhere('description', $prevRecord->description);
                        $currentAmount = $currentRecord ? $currentRecord->amount : 0;
                    @endphp

                    <td>{{ $currentAmount }}</td>

                    @php
                        $upToDateRecord = $customer_wise_total_loans_upto_date
                            ->firstWhere('description', $prevRecord->description);
                        $totalAmount_loans = $prevRecord->amount + $currentAmount;
                    @endphp

                    <td>{{ $totalAmount_loans }}</td>
                    
                </tr>

                @php
                    $totalUptoPrevMonth_loan += $prevRecord->amount;
                @endphp
            @endforeach

            

            @foreach ($customer_wise_total_loans_current as $currentRecord)
                @if (!$customer_wise_total_loans_prev->contains('description', $currentRecord->description))
                    <tr>
                        <td>{{ $currentRecord->description }}</td>
                        <td></td>
                        <td>{{ $currentRecord->amount }}</td>

                        @php
                            $upToDateRecord = $customer_wise_total_loans_upto_date
                                ->firstWhere('description', $currentRecord->description);
                        @endphp

                        <td>{{ $upToDateRecord ? $upToDateRecord->amount : 0 }}</td>

                        @php
                        $totalUptoDate_loan += $upToDateRecord->amount;
                    @endphp
                    </tr>
                @endif

                @php
                    $totalCurrentMonth_loan += $currentRecord->amount;
                @endphp
            @endforeach






          {{-- end Loans --}}


          <tr>
            <td style="font-weight: bold;">Total Capital Payments :</td>
            <td style="font-weight: bold;">{{ $total_Prev_Capital = $totalUptoPrevMonth_libilities + $totalUptoPrevMonth_loan }}</td>
            <td style="font-weight: bold;">{{ $total_Current_Capital = $totalCurrentMonth_libilities + $totalCurrentMonth_loan }}</td>
            <td style="font-weight: bold;">{{ $total_Upto_Capital = $totalUptoDate_libilities +  $totalUptoDate_loan}}</td>

            {{-- {{ $total_Upto_Capital = $totalUptoDate_libilities +  $totalUptoDate_loan}} --}}
          </tr>


          {{-- end Capital Payment --}}





          {{-- start payment expenses --}}
           <tr>
            <th colspan="5">Expenses :</th>
          </tr>
          <tr>
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_expense = 0;
          $totalCurrentMonth_expense = 0;
          $totalUptoDate_expense = 0;
          @endphp


        {{-- @foreach ($customer_wise_total_revenue_payments_prev as $recordPrevMonth)
        @php
            $matchedRecord = $customer_wise_total_revenue_payments_upto_date->first(function ($value) use ($recordPrevMonth) {
              
                return (float) $value->amount === (float) $recordPrevMonth->amount;
            });
        @endphp

        <tr>
            <td>{{ $recordPrevMonth->description }}</td>
            <td>{{ $recordPrevMonth->amount }}</td>

          

            <td></td>

            @if ($matchedRecord)
                
                <td>{{ $matchedRecord->amount }}</td>
                @php
                $totalUptoDate_expense += $matchedRecord->amount;
                @endphp
            @else
                
                <td></td>
            @endif
        </tr>

        @php
            $totalUptoPrevMonth_expense += $recordPrevMonth->amount;
        @endphp
        @endforeach






        @foreach ($customer_wise_total_revenue_payments_current as $record)
        <tr>
            <td>{{ $record->description }}</td>
            <td></td> <!-- No data for Upto Prev Month in $recordsForCurrentMonth -->
            <td>{{ $record->amount }}</td>
            <td></td> <!-- No data for Up to Date in $recordsForCurrentMonth -->
        </tr>

        @php
        $totalCurrentMonth_expense += $record->amount;
        @endphp

        @endforeach --}}




        @foreach ($customer_wise_total_revenue_payments_prev as $prevRecord)
        <tr>
            <td>{{ $prevRecord->description }}</td>
            <td>{{ $prevRecord->amount }}</td>
    
            @php $currentAmount = null; @endphp
    
            @foreach ($customer_wise_total_revenue_payments_current as $current)
                @if ($current->description === $prevRecord->description)
                    @php $currentAmount = $current->amount; @endphp
                    <td>{{ $currentAmount }}</td>
                  
                    @break
                @endif
            @endforeach
    
            @foreach ($customer_wise_total_revenue_payments_upto_date as $upToDateRecord)
                @if ($upToDateRecord->description === $prevRecord->description)
                    @php
                        $totalAmount_expense = $prevRecord->amount + $currentAmount ;
                    @endphp
                    <td>{{ $totalAmount_expense }}</td>
                    @php
                    $totalUptoDate_expense += $totalAmount_expense;
                    @endphp
                   
                    
                    @break
                @endif
            @endforeach
        </tr>
        @php
          $totalUptoPrevMonth_expense += $prevRecord->amount;
      @endphp
    @endforeach


    
    @foreach ($customer_wise_total_revenue_payments_current as $currentRecord)
        @if (!$customer_wise_total_revenue_payments_prev->contains('description', $currentRecord->description))
            <tr>
                <td>{{ $currentRecord->description }}</td>
                <td></td>
                <td>{{ $currentRecord->amount }}</td>
    
                @foreach ($customer_wise_total_revenue_payments_upto_date as $upToDateRecord)
                    @if ($upToDateRecord->description === $currentRecord->description) 
                        <td>{{ $upToDateRecord->amount }}</td>


                        @php
                        $totalUptoDate_expense += $upToDateRecord->amount;
                        @endphp
                        @break
                    @endif
                @endforeach
            </tr>
        @endif

        @php
        $totalCurrentMonth_expense += $currentRecord->amount;
        @endphp
    @endforeach

          
          

          <tr>
            <td style="font-weight: bold;">Total Expense:</td>
            <td style="font-weight: bold;">{{ $totalUptoPrevMonth_expense }}</td>
            <td style="font-weight: bold;">{{  $totalCurrentMonth_expense }}</td>
            <td style="font-weight: bold;">{{ $totalUptoDate_expense}}</td>
          </tr> 

         

          {{-- end payment expenses--}}


          {{-- Total Payments Calculation --}}

           @php

          $totalUptoPrevMonth_payments = $total_Prev_Capital + $totalUptoPrevMonth_expense ;
          $totalCurrentMonth_payments = $total_Current_Capital + $totalCurrentMonth_expense ;
          $totalUptoDate_payments =  $total_Upto_Capital + $totalUptoDate_expense ;

          
          @endphp

          <tr>
            <td colspan="5" style="height: 2px; background-color: #000;"></td>
          </tr>

          <tr>
            <td style="font-weight: bold;font-size:1.2rem;">Total Payments :</td>
            <td style="font-weight: bold;font-size:1.2rem;">{{ $totalUptoPrevMonth_payments }}</td>
            <td style="font-weight: bold;font-size:1.2rem;">{{ $totalCurrentMonth_payments}}</td>
            <td style="font-weight: bold;font-size:1.2rem;">{{ $totalUptoDate_payments }}</td>
          </tr> 
          

         

          {{-- Closing Balance --}}

          <tr>
            <th colspan="5">Closing Balance :</th>
          </tr>

          <tr>
          
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

        


          <tr>
            <td style="font-weight: bold;">Total Closing Balance:</td>
            <td style="font-weight: bold;">{{ $totalUptoPrevMonth_Closing }}</td>
            <td style="font-weight: bold;">{{ $totalCurrentMonth_Closing  }}</td>
            <td style="font-weight: bold;">{{  $totalUptoDate_Closing }}</td>
          </tr>
          
          
          
          
        </tbody>
      </table>
</section>

@endsection


@section('script')
<script type="text/javascript">







</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
