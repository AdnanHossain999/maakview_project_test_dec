@extends('backend.layouts.app')

@section('content')

<section>

    <div class="container">
        <form method="POST" action="{{ route('accounts.get_maakview_payments_report_post') }}">
            @csrf

            <div class="row">
                <div class="col-md-4">
                    <select id="month_select" class="form-control" name="month_value">
                        <option value="">Select a month</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                      </select>
                </div>
                <div class="col-md-4">
                    <select id="year_select" class="form-control" name="year_value">
                        <option value="">Select a Year</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2030">2030</option>
      
                      </select>
                </div>
               
                {{-- <div class="col-md-2">
                    <input type="date" name="date_value" class="form-control" id="date_value">
                </div> --}}


            </div>
           
                <div style="display: flex; flex-direction: row; justify-content: Center;">
                    <!-- First Column -->
                    <div style="margin-right: 20px;">
                        <div>
                            <label for="cash_in_hand_1">Cash in Hand / Cash at Bank</label>
                            <select id="cash_in_hand_1" class="form-control" name="description">
                                <option value="Cash in Hand">Cash in Hand</option>
                                <option value="Cash at Bank">Cash at Bank</option>
                            </select>
                        </div>
                        <div>
                            <label for="amount" >Balance</label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                        {{-- <div>
                            <label for="current_month_1">Current Month</label>
                            <input type="number" class="form-control" id="current_month_1" name="current_month_1">
                        </div>
                        <div>
                            <label for="upto_date_1">Upto Date</label>
                            <input type="number" class="form-control" id="upto_date_1" name="upto_date_1">
                        </div> --}}
                    </div>
            
                    <!-- Second Column -->
                    {{-- <div>
                        <div>
                            <label for="cash_in_hand_2">Cash in Hand / Cash at Bank</label>
                            <select id="cash_in_hand_2" class="form-control" name="description_2">
                                <option value="cash_in_hand">Cash in Hand</option>
                                <option value="cash_at_bank">Cash at Bank</option>
                            </select>
                        </div>
                        <div>
                            <label for="upto_prev_month_2">Upto Previous Month</label>
                            <input type="number" class="form-control" id="upto_prev_month_2" name="upto_prev_month_2">
                        </div>
                        <div>
                            <label for="current_month_2">Current Month</label>
                            <input type="number" class="form-control" id="current_month_2" name="current_month_2">
                        </div>
                        <div>
                            <label for="upto_date_2">Upto Date</label>
                            <input type="number" class="form-control" id="upto_date_2" name="upto_date_2">
                        </div>
                    </div> --}}

                    
                </div>
            
                <div style="display: flex;justify-content:center;margin-top:30px;">
                    <input id="submit_button" type="submit" class="btn btn-primary">
                </div>
            
            
        </form>

       
    </div>

   
    
</section>


@endsection