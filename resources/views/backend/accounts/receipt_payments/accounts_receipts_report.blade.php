@extends('backend.layouts.app')
<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')
<section class="accounts_ledger_report">
      <table class="table table-bordered text-center">
        <thead>
          <tr>
            <th colspan="5" style="font-size: 30px;">Maakview Report on Receipts</th>
          </tr>
          <tr>
            <th colspan="5" style="font-size: 20px;">Receipts for the Month of {{ DateTime::createFromFormat('!m', $currentMonth)->format('F') }}</th>
           

          </tr>
        </thead>
        <tbody>

          {{-- Opening Balance Adnan --}}
           <tr>
            <th colspan="5">Openning Balance :</th>
          </tr>
          <tr>
          
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_Opening = 0;
          $totalCurrentMonth_Opening = 0;
          $totalUptoDate_Opening = 0;
          @endphp

          
        {{-- Cash openning Calculation --}}


        {{-- @foreach ($recordsForPreviousMonth as $recordPrevMonth)
          @php
              $matchedRecord = $recordsUpToDate->first(function ($value) use ($recordPrevMonth) {
                 
                  return (float) $value->amount === (float) $recordPrevMonth->amount;
              });
          @endphp

          <tr>
              <td>{{ $recordPrevMonth->description }}</td>
              <td>{{ $recordPrevMonth->amount }}</td>

             

              <td></td>

                  @if ($matchedRecord)
                  
                  <td>{{ $matchedRecord->amount }}</td>

                  @php
                  $totalUptoDate_Opening += $matchedRecord->amount;
                  @endphp
              @else
                  
                  <td></td>
              @endif
          </tr>

          @php
              $totalUptoPrevMonth_Opening += $recordPrevMonth->amount;
          @endphp
        @endforeach

        

        @foreach ($recordsForCurrentMonth as $record)
            <tr>
                <td>{{ $record->description }}</td>
                <td></td>
                <td>{{ $record->amount }}</td>
                <td></td> 
            </tr>

          @php
          $totalCurrentMonth_Opening += $record->amount;
          @endphp

        @endforeach 

      
          <tr>
            <td style="font-weight: bold;">Total Openning Balance:</td>
            <td style="font-weight: bold;">{{ $totalUptoPrevMonth_Opening }}</td>
            <td style="font-weight: bold;">{{ $totalCurrentMonth_Opening }}</td>
            <td style="font-weight: bold;">{{ $totalUptoDate_Opening }}</td>
          </tr> --}}


          @foreach ($recordsForPreviousMonth as $prevRecord)
                <tr>
                    <td>{{ $prevRecord->description }}</td>
                    <td>{{ $prevRecord->amount }}</td>

                    @php
                        $currentRecord = $recordsForCurrentMonth
                            ->firstWhere('description', $prevRecord->description);
                        $currentAmount = $currentRecord ? $currentRecord->amount : 0;
                    @endphp

                    <td>{{ $currentAmount }}</td>

                    @php
                        $upToDateRecord = $recordsUpToDate
                            ->firstWhere('description', $prevRecord->description);
                        $totalAmount_opening = $prevRecord->amount + $currentAmount;
                    @endphp

                    <td>{{ $totalAmount_opening }}</td>
                    @php
                        $totalUptoDate_Opening += $totalAmount_opening;

                     
                    @endphp

                    

                </tr>

                @php
                    $totalUptoPrevMonth_Opening += $prevRecord->amount;
                @endphp
            @endforeach

            

            @foreach ($recordsForCurrentMonth as $currentRecord)

                @if (!$recordsForPreviousMonth->contains('description', $currentRecord->description))
                    <tr>
                        <td>{{ $currentRecord->description }}</td>
                        <td></td>
                        <td>{{ $currentRecord->amount }}</td>

                        @php
                            $upToDateRecord = $recordsUpToDate
                                ->firstWhere('description', $currentRecord->description);
                        @endphp

                        <td>{{ $upToDateRecord ? $upToDateRecord->amount + $currentRecord->amount : 0 }}</td>

                        @php
                        $totalUptoDate_Opening += $upToDateRecord->amount + $currentRecord->amount;

                     
                    @endphp
                    </tr>
                @endif

                @php
                    $totalCurrentMonth_Opening += $currentRecord->amount;
                @endphp


            @endforeach


        






           




       

  
          {{-- end Openning by Adnan --}}

          {{-- Receipts --}}

          {{-- Start Receipts Assets Section --}}
           <tr>
            <th colspan="5">Capital Receipts:</th>
            
          </tr>
          <tr>
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          {{-- @php
          $totalUptoPrevMonth_asset = 0;
          $totalCurrentMonth_asset = 0;
          $totalUptoDate_asset = 0;
          @endphp --}}



          {{-- @foreach ($customer_wise_total_assets_prev as $recordPrevMonth)
          @php
              $matchedRecord = $customer_wise_total_assets_upto_date->first(function ($value) use ($recordPrevMonth) {
                 
                  return (float) $value->amount === (float) $recordPrevMonth->amount;
              });


             
          @endphp

          <tr>
              <td>{{ $recordPrevMonth->description }}</td>
              <td>{{ $recordPrevMonth->amount }}</td>
              

              @if ($matchedRecord)
                  
                <td></td>
                
              
                 
                  @php
                  $totalUptoDate_asset += $matchedRecord->amount;
                  @endphp

                <td>{{ $matchedRecord->amount }}</td>

              @else

          

              @endif
          </tr>

          @php
              $totalUptoPrevMonth_asset += $recordPrevMonth->amount;
          @endphp
        @endforeach --}}


         {{-- @foreach ($customer_wise_total_assets_current as $record)
          <tr>
              <td>{{ $record->description }}</td>
              <td></td> 
              <td>{{ $record->amount }}</td>
              <td></td>
          </tr>

          @php
          $totalCurrentMonth_asset += $record->amount;
          @endphp

          @endforeach  --}}



       {{-- @php
          $totalUptoPrevMonth_asset = 0;
          $totalCurrentMonth_asset = 0;
          $totalUptoDate_asset = 0;
      @endphp


         @foreach ($customer_wise_total_assets_prev as $prevRecord)
         <tr>
             <td>{{ $prevRecord->description }}</td>
             <td>{{ $prevRecord->amount }}</td>
     
             @php $currentAmount = null; @endphp
     
             @foreach ($customer_wise_total_assets_current as $current)
                 @if ($current->description === $prevRecord->description)
                     @php $currentAmount = $current->amount; @endphp
                     <td>{{ $currentAmount }}</td>
                    
                     @break
                 @endif
             @endforeach
     
             @foreach ($customer_wise_total_assets_upto_date as $upToDateRecord)
                 @if ($upToDateRecord->description === $prevRecord->description)
                     @php
                        $totalAmount_asset = $prevRecord->amount + ($current->amount ?? 0);
                     @endphp
                     <td>{{ $totalAmount_asset }}</td>
                     @php
                    $totalUptoDate_asset += $totalAmount_asset;
                    @endphp
                     
                     @break
                 @endif
             @endforeach
         </tr>
         @php
            $totalUptoPrevMonth_asset += $prevRecord->amount;
        @endphp
     @endforeach


     
     @foreach ($customer_wise_total_assets_current as $currentRecord)
         @if (!$customer_wise_total_assets_prev->contains('description', $currentRecord->description))
             <tr>
                 <td>{{ $currentRecord->description }}</td>
                 <td></td>
                 <td>{{ $currentRecord->amount }}</td>
     
                  @foreach ($customer_wise_total_assets_upto_date as $upToDateRecord)
                     @if ($upToDateRecord->description === $currentRecord->description) 
                         <td>{{ $upToDateRecord->amount }}</td>
                          @break
                     @endif
                 @endforeach
             </tr>
         @endif

         @php
         $totalCurrentMonth_asset += $currentRecord->amount;
         @endphp
     @endforeach --}}



                @php
                $totalUptoPrevMonth_asset = 0;
                $totalCurrentMonth_asset = 0;
                $totalUptoDate_asset = 0;
            @endphp

            @foreach ($customer_wise_total_assets_prev as $prevRecord)
                <tr>
                    <td>{{ $prevRecord->description }}</td>
                    <td>{{ $prevRecord->amount }}</td>

                    @php
                        $currentRecord = $customer_wise_total_assets_current
                            ->firstWhere('description', $prevRecord->description);
                        $currentAmount = $currentRecord ? $currentRecord->amount : 0;
                    @endphp

                    <td>{{ $currentAmount }}</td>

                    @php
                        $upToDateRecord = $customer_wise_total_assets_upto_date
                            ->firstWhere('description', $prevRecord->description);
                        $totalAmount_asset = $prevRecord->amount + $currentAmount;
                    @endphp

                    <td>{{ $totalAmount_asset }}</td>
                    @php
                        $totalUptoDate_asset += $totalAmount_asset;
                    @endphp
                </tr>

                @php
                    $totalUptoPrevMonth_asset += $prevRecord->amount;
                @endphp
            @endforeach

            

            @foreach ($customer_wise_total_assets_current as $currentRecord)
                @if (!$customer_wise_total_assets_prev->contains('description', $currentRecord->description))
                    <tr>
                        <td>{{ $currentRecord->description }}</td>
                        <td></td>
                        <td>{{ $currentRecord->amount }}</td>

                        @php
                            $upToDateRecord = $customer_wise_total_assets_upto_date
                                ->firstWhere('description', $currentRecord->description);
                        @endphp

                        <td>{{ $upToDateRecord ? $upToDateRecord->amount : 0 }}</td>
                    </tr>
                @endif

                @php
                    $totalCurrentMonth_asset += $currentRecord->amount;
                @endphp
            @endforeach




        


          {{-- Liabilities --}}

            {{-- liabilities --}}
            @php
            $totalUptoPrevMonth_liability = 0;
            $totalCurrentMonth_liability = 0;
            $totalUptoDate_liability = 0;
            @endphp


{{-- 
            @foreach ($customer_wise_total_liabilties_prev as $prevRecord)
            <tr>
                <td>{{ $prevRecord->description }}</td>
                <td>{{ $prevRecord->amount }}</td>

                @php $currentAmount = null; @endphp

                @foreach ($customer_wise_total_liabilties_current as $current)
                    @if ($current->description === $prevRecord->description)
                        @php $currentAmount = $current->amount; @endphp
                        <td>{{ $currentAmount }}</td>
                    
                        @break
                    @endif
                @endforeach

                @foreach ($customer_wise_total_liabilties_upto_date as $upToDateRecord)
                    @if ($upToDateRecord->description === $prevRecord->description)
                        @php
                            $totalAmount_liability = $prevRecord->amount + ($current->amount ?? 0);
                        @endphp
                        <td>{{ $totalAmount_liability }}</td>
                        @php
                    $totalUptoDate_liability += $totalAmount_liability;
                    @endphp
                        
                        @break
                    @endif
                @endforeach
            </tr>
            @php
            $totalUptoPrevMonth_liability += $prevRecord->amount;
            @endphp
            @endforeach



            @foreach ($customer_wise_total_liabilties_current as $currentRecord)
            @if (!$customer_wise_total_liabilties_prev->contains('description', $currentRecord->description))
                <tr>
                    <td>{{ $currentRecord->description }}</td>
                    <td></td>
                    <td>{{ $currentRecord->amount }}</td>

                    @foreach ($customer_wise_total_liabilties_upto_date as $upToDateRecord)
                        @if ($upToDateRecord->description === $currentRecord->description) 
                            <td>{{ $upToDateRecord->amount }}</td>
                            @break
                        @endif
                    @endforeach
                </tr>
            @endif

            @php
            $totalCurrentMonth_liability += $currentRecord->amount;
            @endphp
            @endforeach --}}





            @foreach ($customer_wise_total_liabilties_prev as $prevRecord)
            <tr>
                <td>{{ $prevRecord->description }}</td>
                <td>{{ $prevRecord->amount }}</td>

                @php
                    $currentRecord = $customer_wise_total_liabilties_current
                        ->firstWhere('description', $prevRecord->description);
                    $currentAmount = $currentRecord ? $currentRecord->amount : 0;
                @endphp

                <td>{{ $currentAmount }}</td>

                @php
                    $upToDateRecord = $customer_wise_total_liabilties_upto_date
                        ->firstWhere('description', $prevRecord->description);
                    $totalAmount_liabilties = $prevRecord->amount + $currentAmount;
                @endphp

                <td>{{ $totalAmount_liabilties }}</td>
                @php
                    $totalUptoDate_liability += $totalAmount_liabilties;
                @endphp
            </tr>

            @php
                $totalUptoPrevMonth_liability += $prevRecord->amount;
            @endphp
        @endforeach

        

        @foreach ($customer_wise_total_liabilties_current as $currentRecord)
            @if (!$customer_wise_total_liabilties_prev->contains('description', $currentRecord->description))
                <tr>
                    <td>{{ $currentRecord->description }}</td>
                    <td></td>
                    <td>{{ $currentRecord->amount }}</td>

                    @php
                        $upToDateRecord = $customer_wise_total_liabilties_upto_date
                            ->firstWhere('description', $currentRecord->description);
                    @endphp

                    <td>{{ $upToDateRecord ? $upToDateRecord->amount : 0 }}</td>
                </tr>
            @endif

            @php
                $totalCurrentMonth_liability += $currentRecord->amount;
            @endphp
        @endforeach











          {{-- end liabilities --}}

          {{-- Start Loans --}}

          @php
          $totalUptoPrevMonth_loan = 0;
          $totalCurrentMonth_loan = 0;
          $totalUptoDate_loan = 0;
          @endphp
        
        

        {{-- @foreach ($customer_wise_total_loan_prev as $prevRecord)
        <tr>
            <td>{{ $prevRecord->description }}</td>
            <td>{{ $prevRecord->amount }}</td>

            @php $currentAmount = null; @endphp

            @foreach ($customer_wise_total_loan_current as $current)
                @if ($current->description === $prevRecord->description)
                    @php $currentAmount = $current->amount; @endphp
                    <td>{{ $currentAmount }}</td>
                
                    @break
                @endif
            @endforeach
            

            @foreach ($customer_wise_total_loan_upto_date as $upToDateRecord)
                @if ($upToDateRecord->description === $prevRecord->description)
                    @php
                        $totalAmount_loan = $prevRecord->amount + ($current->amount ?? 0);
                    @endphp
                    <td>{{ $totalAmount_loan }}</td>
              
                    
                    @break
                @endif
            @endforeach


            
        </tr>
        @php
        $totalUptoPrevMonth_loan += $prevRecord->amount;
        @endphp
        @endforeach



        @foreach ($customer_wise_total_loan_current as $currentRecord)
        @if (!$customer_wise_total_loan_prev->contains('description', $currentRecord->description))
            <tr>
                <td>{{ $currentRecord->description }}</td>
                <td></td>
                <td>{{ $currentRecord->amount }}</td>

                @foreach ($customer_wise_total_loan_upto_date as $upToDateRecord)
                    @if ($upToDateRecord->description === $currentRecord->description) 
                        <td>{{ $upToDateRecord->amount }}</td>

                        @php
                        $totalUptoDate_loan += $upToDateRecord->amount;
                        @endphp
                        @break

                    @endif
                @endforeach
            </tr>
        @endif

        @php
        $totalCurrentMonth_loan += $currentRecord->amount;
        @endphp
        @endforeach --}}




        @foreach ($customer_wise_total_loan_prev as $prevRecord)
            <tr>
                <td>{{ $prevRecord->description }}</td>
                <td>{{ $prevRecord->amount }}</td>

                @php
                    $currentRecord = $customer_wise_total_loan_current
                        ->firstWhere('description', $prevRecord->description);
                    $currentAmount = $currentRecord ? $currentRecord->amount : 0;
                @endphp

                <td>{{ $currentAmount }}</td>

                @php
                    $upToDateRecord = $customer_wise_total_loan_upto_date
                        ->firstWhere('description', $prevRecord->description);
                    $totalAmount_loan = $prevRecord->amount + $currentAmount;
                @endphp

                <td>{{ $totalAmount_loan }}</td>
                @php
                    $totalUptoDate_loan += $totalAmount_loan;
                @endphp
            </tr>

            @php
                $totalUptoPrevMonth_loan += $prevRecord->amount;
            @endphp
        @endforeach

        

        @foreach ($customer_wise_total_loan_current as $currentRecord)
            @if (!$customer_wise_total_loan_prev->contains('description', $currentRecord->description))
                <tr>
                    <td>{{ $currentRecord->description }}</td>
                    <td></td>
                    <td>{{ $currentRecord->amount }}</td>

                    @php
                        $upToDateRecord = $customer_wise_total_loan_upto_date
                            ->firstWhere('description', $currentRecord->description);
                    @endphp

                    <td>{{ $upToDateRecord ? $upToDateRecord->amount : 0 }}</td>
                </tr>
            @endif

            @php
                $totalCurrentMonth_loan += $currentRecord->amount;
            @endphp
        @endforeach




          
          {{-- end Loans --}}

           <tr>
            <td style="font-weight: bold;">Total Capital Receipts:</td>
            <td style="font-weight: bold;">{{ $total_Prev_Capital = $totalUptoPrevMonth_asset + $totalUptoPrevMonth_liability + $totalUptoPrevMonth_loan }}</td>
            <td style="font-weight: bold;">{{ $total_Current_Capital = $totalCurrentMonth_asset +  $totalCurrentMonth_liability + $totalCurrentMonth_loan }}</td>
            <td style="font-weight: bold;">{{ $total_Upto_Capital = $totalUptoDate_asset + $totalUptoDate_liability + $totalUptoDate_loan }}</td>
          </tr> 


          {{-- end Receipts Assets --}}





          {{-- start Receipts Incomes --}}
           <tr>
            <th colspan="5">Incomes and Revenue:</th>
          </tr>
          <tr>
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_income = 0;
          $totalCurrentMonth_income = 0;
          $totalUptoDate_income = 0;
          @endphp 
          

         


{{-- 
            @foreach ($customer_wise_total_sales_income_prev as $prevRecord)
            <tr>
                <td>{{ $prevRecord->description }}</td>
                <td>{{ $prevRecord->amount }}</td>
        
                @php $currentAmount = null; @endphp
        
                @foreach ($customer_wise_total_sales_income_current as $current)
                    @if ($current->description === $prevRecord->description)
                        @php $currentAmount = $current->amount; @endphp
                        <td>{{ $currentAmount }}</td>
                       
                        @break
                    @endif
                @endforeach
        
                @foreach ($customer_wise_total_sales_income_upto_date as $upToDateRecord)
                    @if ($upToDateRecord->description === $prevRecord->description)
                        @php
                            $totalAmount_revenue = $prevRecord->amount + ($current->amount ?? 0);
                        @endphp
                        <td>{{ $totalAmount_revenue }}</td>
                        @php
                       $totalUptoDate_income += $totalAmount_revenue;
                       @endphp
                        
                        @break
                    @endif
                @endforeach
            </tr>
            @php
               $totalUptoPrevMonth_income += $prevRecord->amount;
           @endphp
        @endforeach
   
   
        
        @foreach ($customer_wise_total_sales_income_current as $currentRecord)
            @if (!$customer_wise_total_sales_income_prev->contains('description', $currentRecord->description))
                <tr>
                    <td>{{ $currentRecord->description }}</td>
                    <td></td>
                    <td>{{ $currentRecord->amount }}</td>
        
                     @foreach ($customer_wise_total_sales_income_upto_date as $upToDateRecord)
                        @if ($upToDateRecord->description === $currentRecord->description) 
                            <td>{{ $upToDateRecord->amount }}</td>
                             @break
                        @endif
                    @endforeach
                </tr>
            @endif
   
            @php
            $totalCurrentMonth_income += $currentRecord->amount;
            @endphp
        @endforeach --}}





        @foreach ($customer_wise_total_sales_income_prev as $prevRecord)
        <tr>
            <td>{{ $prevRecord->description }}</td>
            <td>{{ $prevRecord->amount }}</td>

            @php
                $currentRecord = $customer_wise_total_sales_income_current
                    ->firstWhere('description', $prevRecord->description);
                $currentAmount = $currentRecord ? $currentRecord->amount : 0;
            @endphp

            <td>{{ $currentAmount }}</td>

            @php
                $upToDateRecord = $customer_wise_total_sales_income_upto_date
                    ->firstWhere('description', $prevRecord->description);
                $totalAmount_sales_income = $prevRecord->amount + $currentAmount;
            @endphp

            <td>{{ $totalAmount_sales_income }}</td>
            @php
                $totalUptoDate_income += $totalAmount_sales_income;
            @endphp
        </tr>

        @php
            $totalUptoPrevMonth_income += $prevRecord->amount;
        @endphp
    @endforeach

    

    @foreach ($customer_wise_total_sales_income_current as $currentRecord)
        @if (!$customer_wise_total_sales_income_prev->contains('description', $currentRecord->description))
            <tr>
                <td>{{ $currentRecord->description }}</td>
                <td></td>
                <td>{{ $currentRecord->amount }}</td>

                @php
                    $upToDateRecord = $customer_wise_total_sales_income_upto_date
                        ->firstWhere('description', $currentRecord->description);
                @endphp

                <td>{{ $upToDateRecord ? $upToDateRecord->amount : 0 }}</td>
            </tr>
        @endif

        @php
            $totalCurrentMonth_income += $currentRecord->amount;
        @endphp
    @endforeach

         

         

          {{-- end Receipts Incomes--}}


          {{-- Total Receipts Calculation --}}

          @php
          $totalUptoPrevMonth = $totalUptoPrevMonth_Opening + $total_Prev_Capital + $totalUptoPrevMonth_income ;
          $totalCurrentMonth = $totalCurrentMonth_Opening + $total_Current_Capital + $totalCurrentMonth_income ;
        //   $totalUptoDate = dd($totalUptoDate_Opening);

          $totalUptoDate = $totalUptoDate_Opening + $total_Upto_Capital + $totalUptoDate_income ;

          
          @endphp

          <tr>
            <td colspan="4" style="background-color: #000;"></td>
          </tr>

          <tr id="totalReceiptsRow">
            <td style="font-weight: bold;font-size:1.2rem;">Total Receipts :</td>
            <td id="totalUptoPrevMonth" style="font-weight: bold;font-size:1.2rem;">{{ $totalUptoPrevMonth }}</td>
            <td id="totalCurrentMonth" style="font-weight: bold;font-size:1.2rem;">{{ $totalCurrentMonth }}</td>
            <td id="totalUptoDate" style="font-weight: bold;font-size:1.2rem;">{{ $totalUptoDate }}</td>
        </tr>
          

         

         


          
          
          
         
        </tbody>
      </table>
</section>

@endsection


@section('script')
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>



<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Collect data from the table row
        var totalUptoPrevMonth = document.getElementById('totalUptoPrevMonth').textContent;
        var totalCurrentMonth = document.getElementById('totalCurrentMonth').textContent;
        var totalUptoDate = document.getElementById('totalUptoDate').textContent;

        // Prepare data for the Axios request
        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        };

        var requestData = {
           
            totalUptoPrevMonth: totalUptoPrevMonth,
            totalCurrentMonth: totalCurrentMonth,
            totalUptoDate: totalUptoDate,
        };

        


        // Make the Axios request
        axios.post('{{ route("accounts.maakview_payments_report_data") }}', requestData, { headers: headers })
        .then(function (response) {
            // Handle the success response
            // console.log('Axios request successful!');
            // console.log('Response:', response.data);

            // Add your additional success handling logic here
        })
        .catch(function (error) {
            // Handle the error response
            console.error('Axios request failed!');
            console.error('Error:', error);

            // Add your error handling logic here
        });
    });
</script>

@endsection
