@extends('backend.layouts.app')

@section('content')
<section class="accounts_ledger_report">
      <table class="table table-bordered text-center">
        <thead>
          <tr>
            <th colspan="5" style="font-size: 30px;">Maakview Report on Receipts</th>
          </tr>
          <tr>
            
            <th colspan="5" style="font-size: 20px;"> Upto Date: {{ \Carbon\Carbon::parse($upto_date)->format('F j, Y') }} </th>

          </tr>
        </thead>
        <tbody>

          {{-- Opening Balance Adnan --}}
           <tr>
            <th colspan="5">Openning Balance :</th>
          </tr>
          <tr>
          
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_Opening = 0;
          $totalCurrentMonth_Opening = 0;
          $totalUptoDate_Opening = 0;
          @endphp

          @foreach ($recordsForPreviousMonth as $current)
          <tr>
              <td>{{ $current->description }}</td>
              <td>{{ $current->upto_prev_month }}</td>
              <td>{{ $current->current_month }}</td>
              <td>{{ $current->upto_date }}</td>
          </tr>

          @php
          $totalUptoPrevMonth_Opening += $current->upto_prev_month;
          $totalCurrentMonth_Opening += $current->current_month;
          $totalUptoDate_Opening += $current->upto_date;
          @endphp

          @endforeach

          @foreach ($recordsForCurrentMonth as $current)
              <tr>
                  <td>{{ $current->description }}</td>
                  <td>{{ $current->upto_prev_month }}</td>
                  <td>{{ $current->current_month }}</td>
                  <td>{{ $current->upto_date }}</td>
              </tr>

              @php
              $totalUptoPrevMonth_Opening += $current->upto_prev_month;
              $totalCurrentMonth_Opening += $current->current_month;
              $totalUptoDate_Opening += $current->upto_date;
              @endphp

          @endforeach

          @foreach ($recordsUpToUptoDate as $current)
          <tr>
              <td>{{ $current->description }}</td>
              <td>{{ $current->upto_prev_month }}</td>
              <td>{{ $current->current_month }}</td>
              <td>{{ $current->upto_date }}</td>
          </tr>

          @php
          $totalUptoPrevMonth_Opening += $current->upto_prev_month;
          $totalCurrentMonth_Opening += $current->current_month;
          $totalUptoDate_Opening += $current->upto_date;
          @endphp

          @endforeach


      {{-- @foreach ($recordsForCurrentMonth as $current)
      <tr>
          <td>{{ $current->description }}</td>
          <td>{{ $current->upto_prev_month }}</td>
          <td>{{ $current->current_month }}</td>
          <td>{{ $current->upto_date }}</td>
      </tr>

      @php
      $totalUptoPrevMonth_Opening += $current->upto_prev_month;
      $totalCurrentMonth_Opening += $current->current_month;
      $totalUptoDate_Opening += $current->upto_date;
      @endphp

      @endforeach --}}
        


          <tr>
            <td style="font-weight: bold;">Total Openning Balance:</td>
            <td style="font-weight: bold;">{{ $totalUptoPrevMonth_Opening }}</td>
            <td style="font-weight: bold;">{{ $totalCurrentMonth_Opening }}</td>
            <td style="font-weight: bold;">{{ $totalUptoDate_Opening }}</td>
          </tr>

  
          {{-- end Openning by Adnan --}}

          {{-- Receipts --}}

          {{-- Start Receipts Assets Section --}}
          <tr>
            <th colspan="5">Capital Receipts:</th>
            
          </tr>
          <tr>
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_asset = 0;
          $totalCurrentMonth_asset = 0;
          $totalUptoDate_asset = 0;
          @endphp

        
        

          @foreach ($customer_wise_total_assets_current as $current)

          <tr>
            <td>{{$current->description}}</td>
            <td>{{$current->upto_prev_month}}</td>
            <td>{{$current->current_month}}</td>
            <td>{{$current->upto_date}}</td>
          </tr>

          @php
              $totalUptoPrevMonth_asset += $current->upto_prev_month;
              $totalCurrentMonth_asset += $current->current_month;
              $totalUptoDate_asset += $current->upto_date;
          @endphp
          
          @endforeach

          {{-- Liabilities --}}

            {{-- liabilities --}}
            @php
            $totalUptoPrevMonth_liability = 0;
            $totalCurrentMonth_liability = 0;
            $totalUptoDate_liability = 0;
            @endphp
          
          

          @foreach ($customer_wise_total_liabilties_current as $current)

          <tr>
            <td>{{$current->description}}</td>
            <td>{{$current->upto_prev_month}}</td>
            <td>{{$current->current_month}}</td>
            <td>{{$current->upto_date}}</td>
          </tr>

          @php
              $totalUptoPrevMonth_liability += $current->upto_prev_month;
              $totalCurrentMonth_liability += $current->current_month;
              $totalUptoDate_liability += $current->upto_date;
          @endphp
          
          @endforeach

          {{-- end liabilities --}}

          {{-- Start Loans --}}

          @php
          $totalUptoPrevMonth_loan = 0;
          $totalCurrentMonth_loan = 0;
          $totalUptoDate_loan = 0;
          @endphp
        
        

        @foreach ($customer_wise_total_loan_current as $current)

        <tr>
          <td>{{$current->description}}</td>
          <td>{{$current->upto_prev_month}}</td>
          <td>{{$current->current_month}}</td>
          <td>{{$current->upto_date}}</td>
        </tr>

        @php
            $totalUptoPrevMonth_loan += $current->upto_prev_month;
            $totalCurrentMonth_loan += $current->current_month;
            $totalUptoDate_loan += $current->upto_date;
        @endphp
        
        @endforeach
          {{-- end Loans --}}

          <tr>
            <td style="font-weight: bold;">Total Capital Receipts:</td>
            <td style="font-weight: bold;">{{ $total_Prev_Capital = $totalUptoPrevMonth_asset + $totalUptoPrevMonth_liability + $totalUptoPrevMonth_loan }}</td>
            <td style="font-weight: bold;">{{ $total_Current_Capital = $totalCurrentMonth_asset +  $totalCurrentMonth_liability + $totalCurrentMonth_loan }}</td>
            <td style="font-weight: bold;">{{ $total_Upto_Capital = $totalUptoDate_asset + $totalUptoDate_liability + $totalUptoDate_loan}}</td>
          </tr>


          {{-- end Receipts Assets --}}





          {{-- start Receipts Incomes --}}
           <tr>
            <th colspan="5">Incomes and Revenue:</th>
          </tr>
          <tr>
            <th>Description</th>
            <th>Upto Prev Month</th>
            <th>Current Month</th>
            <th>Up to Date</th>
          </tr>

          @php
          $totalUptoPrevMonth_income = 0;
          $totalCurrentMonth_income = 0;
          $totalUptoDate_income = 0;
          @endphp
          
          {{-- @foreach ($customer_wise_total_sales_prev as $prev)
          <tr>
            <td>{{$prev->description}}</td>
            <td>{{$prev->upto_prev_month}}</td>
            <td>{{$prev->current_month}}</td>
            <td>{{$prev->upto_date}}</td>
          </tr>
          
          @endforeach --}}

          @foreach ($customer_wise_total_sales_income_current as $current)

          <tr>
            <td>{{$current->description}}</td>
            <td>{{$current->upto_prev_month}}</td>
            <td>{{$current->current_month}}</td>
            <td>{{$current->upto_date}}</td>
          </tr>

          @php
              $totalUptoPrevMonth_income += $current->upto_prev_month;
              $totalCurrentMonth_income += $current->current_month;
              $totalUptoDate_income += $current->upto_date;
          @endphp
          
          @endforeach

          <tr>
            <td>Total Incomes:</td>
            <td>{{ $totalUptoPrevMonth_income }}</td>
            <td>{{ $totalCurrentMonth_income }}</td>
            <td>{{ $totalUptoDate_income}}</td>
          </tr>

          <tr>
            <td style="font-weight: bold;">Total Revenue Receipts:</td>
            <td style="font-weight: bold;">{{ $totalUptoPrevMonth_income }}</td>
            <td style="font-weight: bold;">{{ $totalCurrentMonth_income }}</td>
            <td style="font-weight: bold;">{{ $totalUptoDate_income}}</td>
          </tr> 

          {{-- end Receipts Incomes--}}


          {{-- Total Receipts Calculation --}}

          @php
          $totalUptoPrevMonth = $totalUptoPrevMonth_Opening + $total_Prev_Capital + $totalUptoPrevMonth_income ;
          $totalCurrentMonth = $totalCurrentMonth_Opening + $total_Current_Capital + $totalCurrentMonth_income ;
          $totalUptoDate = $totalUptoDate_Opening + $total_Upto_Capital + $totalUptoDate_income ;

          
          @endphp

          <tr>
            <td colspan="5" style="height: 2px; background-color: #000;"></td>
          </tr>

          <tr>
            <td style="font-weight: bold;font-size:1.2rem;">Total Receipts :</td>
            <td style="font-weight: bold;font-size:1.2rem;">{{ $totalUptoPrevMonth }}</td>
            <td style="font-weight: bold;font-size:1.2rem;">{{ $totalCurrentMonth}}</td>
            <td style="font-weight: bold;font-size:1.2rem;">{{ $totalUptoDate }}</td>
          </tr> 
          

         

          {{-- Assets --}}


          
          
          
          {{-- <tr>
            <th colspan="3">Expenses:</th>
          </tr>

          @php
            $itemsTotal = 0;
          @endphp

            
            @foreach ($all_expenses as $key => $items)
                @if ($key === 'supplies_expenses' || $key === 'monthly_total_salaries')
                    <tr>
                        <th>{{ ucwords(str_replace('_', ' ', $key))}}</th>
                        <td colspan="2">{{ $items }}</td>
                    </tr>
                    @php
                    $itemsTotal += $items; 
                  @endphp

                @endif
                
            @endforeach


            @if (isset($all_expenses['more_expenses_data']))
            @foreach ($all_expenses['more_expenses_data'] as $key => $items)


            <tr>
              <th>{{ ucwords(str_replace('_', ' ', $key))}}</th>
              <td colspan="2">{{ $items }}</td>
            </tr>



              @php
                  $itemsTotal += $items; 
                @endphp
              @endforeach
            @endif        
            
            <tr>
              <th class="table-secondary">Total Expenses:</th>
              <td class="table-secondary" colspan="2">{{$itemsTotal}} BDT</td>
            </tr>
            --}}


          {{-- Assets --}}
          {{-- <tr>
            <th colspan="3">Assets:</th>
          </tr>
          <tr>
            <th colspan="2">Account</th>
            <th>Amount (BD)</th>
          </tr>
          <tr>
            <td colspan="2">Cash</td>
            <td>1500</td>
          </tr>
          <tr>
            <td colspan="2">Acconts Receivable</td>
            <td>700</td>
          </tr>
          <tr>
            <td colspan="2">Inventory</td>
            <td>1200</td>
          </tr>
          <tr>
            <th class="table-secondary" colspan="2">Total Assets:</th>
            <td class="table-secondary">3400 BDT</td>
          </tr> --}}

          {{-- Liabilities --}}
          {{-- <tr>
            <th colspan="3">Liabilities:</th>
          </tr>
          <tr>
            <th colspan="2">Account</th>
            <th>Amount (BD)</th>
          </tr>
          <tr>
            <td colspan="2">Accounts Payable</td>
            <td>1500</td>
          </tr>
          <tr>
            <td colspan="2">Loans Payable</td>
            <td>700</td>
          </tr>
          <tr>
            <td colspan="2">Accrued Expenses</td>
            <td>1200</td>
          </tr>
          <tr>
            <th class="table-secondary" colspan="2">Total Liabilities:</th>
            <td class="table-secondary">3400 BDT</td>
          </tr> --}}
          {{-- Equity --}}
          {{-- <tr>
            <th colspan="3">Equity:</th>
          </tr>
          <tr>
            <td colspan="2">Owner's Capital:</td>
            <td>1200 BDT</td>
          </tr>

          <tr>
            <th class="table-secondary" colspan="2">Total Liabilities and Equity:</th>
            <th class="table-secondary">3400 BDT</th>
          </tr> --}}
        </tbody>
      </table>
</section>

@endsection


@section('script')
<script type="text/javascript">







</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
