@extends('backend.layouts.app')

@section('content')
<style>
   
</style>
<section >
    <div class="container ">
        <div >
            @if (session()->has('udpate_status'))
            <div class=" notification alert alert-success text-center col-md-12">
                {{ session('udpate_status') }}
            </div>
            @endif
        </div>
        <div >
            @if (session()->has('delete_status'))
            <div class=" notification alert alert-success text-center col-md-12">
                {{ session('delete_status') }}
            </div>
            @endif
        </div>
        <table class="table table-bordered text-center">
            <thead>
                <th>Date</th>
                <th>Current Cash</th>
                <th>Account Receivable Source</th>
                <th>Account Receivable</th>
                <th>Fixed Assets</th>
                <th>Owners Capital</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($maakview_assets_data as $item)
                    <tr>
                        <td>{{$item->date}}</td>
                        <td>{{$item->current_hand_cash}}</td>
                        <td>{{$item->accounts_receivable_source}}</td>
                        <td>{{$item->accounts_receivable}}</td>
                        <td>{{$item->fixed_assets}}</td>
                        <td>{{$item->owners_capital}}</td>
                        <td>
                            <a href="{{route('accounts.maakview_monthly_assets_list_edit', $item->id)}}" class="btn btn-sm btn-primary">Edit</a>
                            <a href="{{route('accounts.maakview_monthly_assets_list_delete', $item->id)}}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}


</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
