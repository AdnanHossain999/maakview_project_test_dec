@extends('backend.layouts.app')

@section('content')
<style>
    .custom-width { 
        width: 400px !important; /* Adjust the width as needed */
    }
</style>
<section >
    <div class="container">
        <form action="{{route('accounts.maakview_ledger_expenses_report')}}" method="POST">
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div >
                        @if (session()->has('expenses_statsu'))
                        <div class=" notification alert alert-danger text-center col-md-12">
                            {{ session('expenses_statsu') }}
                        </div>
                        @endif
                        @if (session()->has('update_ledger_data'))
                        <div class=" notification alert alert-success text-center col-md-12">
                            {{ session('update_ledger_data') }}
                        </div>
                        @endif
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="mb-3">
                            <label for="date" class="form-label">Date</label>
                            <input type="date" class="form-control custom-width" id="date" name="expenses_date" required>
                        </div>
                        <div class="mb-3">
                            <input id="submit_button" type="submit" class="btn btn-primary custom-width" >
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
