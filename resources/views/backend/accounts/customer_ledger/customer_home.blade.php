@extends('backend.layouts.app')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
			<h1 class="h3">{{translate('All Customers')}}</h1>
	</div>
</div>
<script>
function sweet_alert_customer_registration(){

Swal.fire(
'Good job!',
'You clicked the button!',
'success',
)
}


</script>
@if(session()->has('status'))
    <div class="">
      <script>sweet_alert_customer_registration();</script>
    </div>
@endif


<div class="card">
    <div class="card-header hide_class">
        <h5 class="mb-0 h6">{{translate('Customers')}}</h5>
        
       <div class="pull-right clearfix">
            <form class="" id="sort_customers" action="" method="GET">
                <div class="box-inline pad-rgt pull-left">
                    <div class="" style="min-width: 200px;">
                        <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type email or name & Enter') }}">
                    </div>
                </div>
            </form>
        </div>

    </div>

    <div class="card-body" style=" overflow-x: scroll;">
        <table id="dtBasicExample" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{translate('Date')}}</th>
                    <th>{{translate('Name')}}</th>
                    <th data-breakpoints="lg">{{translate('Email Address')}}</th>
                    <th data-breakpoints="lg">{{translate('Phone')}}</th>
                    <th class="text-right" data-breakpoints="lg">{{translate('Ledger Report')}}</th>
                </tr>
            </thead>
             
           
            <tbody>

                @foreach($customers as $key => $user)
                    <tr>
                        <td>{{ ($key+1) + ($customers->currentPage() - 1)*$customers->perPage() }}</td>
                        <td><nobr>{{date("d-m-Y", strtotime($user->created_at))}}</nobr></td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        @php
                            $customerId = $user->id;
                        @endphp
                        <td class="text-center">
                            @can('view_customers')
                                <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" target="_blank" href="{{route('accounts.customer_ledger.customer_ledger_report', $customerId)}}"   title="{{ translate('View') }}">
                                    <i class="las la-eye"></i>
                                </a>
                            @endcan
                         
                           
                        </td>
                    </tr>
                @endforeach
            </tbody>
             
      </table>
      <div class="aiz-pagination">
        {{ $customers->appends(request()->input())->links() }}
    </div>

    </div>
</div>


       
@endsection

@section('modal')
    @include('backend.inc.delete_modal')
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/jquery.PrintArea.min.js" integrity="sha512-mPA/BA22QPGx1iuaMpZdSsXVsHUTr9OisxHDtdsYj73eDGWG2bTSTLTUOb4TG40JvUyjoTcLF+2srfRchwbodg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
      crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">


      





    </script>

@endsection
