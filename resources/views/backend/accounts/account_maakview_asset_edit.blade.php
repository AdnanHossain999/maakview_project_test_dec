@extends('backend.layouts.app')

@section('content')
<style>
    .custom-width { 
        width: 400px !important; /* Adjust the width as needed */
    }
</style>
<section >
    <div class="container">
        <form action="{{route('accounts.maakview_monthly_assets_list_update', $maakview_assets_data->id)}}" method="POST">
            @csrf
            <h2 class="text-center bg-primary text-white mb-3">Update Maakview Expense Data</h2>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div >
                        @if (session()->has('asset_status'))
                        <div class=" notification alert alert-danger text-center col-md-12">
                            {{ session('asset_status') }}
                        </div>
                        @endif
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <div class="mb-3">
                            <label for="">Current Hand Cash</label>
                            <input type="number"  class="form-control custom-width" required name="current_hand_cash" value="{{$maakview_assets_data->current_hand_cash}}">
                        </div>
                        <div class="mb-3">
                            <label for="">Account Receivable Source</label>
                            <input type="text"  class="form-control custom-width" name="accounts_receivable_source" value="{{$maakview_assets_data->accounts_receivable_source}}">
                        </div>
                        <div class="mb-3">
                            <label for="">Account Receivable</label>
                            <input type="number"  class="form-control custom-width" name="accounts_receivable" value="{{$maakview_assets_data->accounts_receivable}}">
                        </div>
                        <div class="mb-3">
                            <label for="">Fixed  Asset</label>
                            <input type="number" required  class="form-control custom-width" name="fixed_assets" value="{{$maakview_assets_data->fixed_assets}}">
                        </div><div class="mb-3">
                            <label for="">Owners Capital</label>
                            <input type="number" required  class="form-control custom-width" name="owners_capital" value="{{$maakview_assets_data->owners_capital}}">
                        </div>
                       
                        <div class="mb-3">
                            <button id="submit_button"  type="submit" class="btn btn-primary custom-width">Update</button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
