@extends('backend.layouts.app')

@section('content')
<section >
    <div class="container">
       <form action="{{route('accounts.maakview_expenses_daily_input_update', $get_single_month_specefic_ledger_data->id)}}" method="POST">
        @csrf
 
        <div class="row p-3">
            <input type="hidden" name="date" id="" value="{{$get_single_month_specefic_ledger_data->date}}">
            <!-- Loop through each expense category and generate input fields with values -->
            <?php
            $expense_categories = [
                'office_rent', 'warehouse_rent', 'office_utilites', 'marketing_online', 'advertisement',
                'business_promotion', 'ip_phone_bill', 'mobile_bill', 'festibal_allowance',
                'bridge_toll_and_parking', 'gift_and_donation', 'electric_goods', 'entertainment_expenses',
                'fuel_oil', 'inernet_and_telephone_line', 'license_and_fees', 'medical_expenses',
                'office_maintenance', 'printing_expenses', 'statinonary_expneses', 'electricity_bill',
                'repair_and_maintenance_others', 'accessories_expenses', 'carring_expenses',
                'product_damage_and_lost_expenses', 'fine_and_penalty', 'load_and_unload_labour_bill',
                'lunch_tiffin', 'postage_and_courier', 'employee_loan_and_other_payments', 'legal_cost',
                'monthly_incentive_payments', 'website_expenses', 'bank_interest_all', 'miscellaneous_expenses',
                'vat_tax'
            ];
        
            foreach ($expense_categories as $category) {
                $field_name = $category;
                $field_value = $get_single_month_specefic_ledger_data->$field_name;
            ?>
            <div class="col-md-2">
                <label for="<?php echo $field_name; ?>"><?php echo ucwords(str_replace('_', ' ', $field_name)); ?></label>
                <input type="number" class="form-control" name="<?php echo $field_name; ?>" value="<?php echo $field_value; ?>">
            </div>
            <?php
            }
            ?>
        </div>
        
        <button type="submit" class="btn btn-primary btn-lg btn-block" style="margin-left: 13px; width:98%">Update</button>
       </form>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">








</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
