@extends('backend.layouts.app')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css"/>
@section('content')
<style>
  body{
            margin: 0;
            box-sizing: border-box;
        }

        .main-table-data{
            width: 80%;
            margin: 0 auto;

        }
        .table-content{
            width: 100%;
            border-collapse: collapse;
        }
        .table-content th, .table-content td{
            border: 1px solid black;
            text-align: center;
            padding: 10px;
        }
        .table-content tbody tr:hover {
            background-color: #ddd;
        }
        table{
            border: 1px solid black;
            width: 100%;
            border-collapse: collapse;
            text-align: center;
        }
        table ,th ,td{
            border: 1px solid black;
            padding: 10px
        }
</style>
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-12">
            <h2 class="bg-primary  text-center" style="color:white;">Add Maakview Opening Balance</h2>
                @if (session()->has('status'))
                <div class=" notification alert alert-success col-md-12">
                    {{ session('status') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="opening_data mx-auto">
            <table>
                <thead>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Previous Month</th>
                    <th>Current Month</th>
                    <th>Upto Month</th>
                </thead>
                <tbody>
                    @foreach ($current_cash_opening as $item)
                    <tr>
                        <td>{{$item->date}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->upto_prev_month}}</td>
                        <td>{{$item->current_month}}</td>
                        <td>{{$item->upto_date}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <form action="{{route('accounts.cashbook.store_maakview_opening_balance')}}" method="POST">
            @csrf
            <div class="raw" style="margin: 0 auto">
                <div class="col-md-6 mx-auto">
                    <div>
                        <label for="">Enter Cash Opening Date</label>
                        <input required type="date" name="date" class="form-control">
                    </div>
                    <div>
                        <label for="">Select Cash Opening Description</label>
                        <select required name="description" class="form-control">
                            <option value="Cash in Hand">Cash in Hand</option>
                            <option value="Cash at Bank">Cash at Bank</option>
                        </select>
                    </div>
                    <div>
                        <label for="">Enter Cash Opening For Previous month</label>
                        <input required type="number" name="prev_month" class="form-control">
                    </div>
                    <div>
                        <label for="">Enter Cash Opening For Current Month</label>
                        <input required type="number" name="current_month" class="form-control">
                    </div>
                    <div>
                        <label for="">Enter Cash Opening Upto Date</label>
                        <input type="number" class="form-control" name="upto_date">
                    </div>
                    <button class="btn btn-primary btn-lg btn-block mt-3">Submit</button>
                </div>
            </div>
        </form>
        </div>


@endsection




@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');




});




//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}


</script>
@endsection
