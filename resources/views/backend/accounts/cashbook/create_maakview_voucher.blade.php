@extends('backend.layouts.app')

@section('content')
<style>
    .custom-width { 
        width: 400px !important; /* Adjust the width as needed */
    }

    .main_div{
            background-color: #F0F1F7;
        }
        .serch_option input:hover{
            border: 1px solid #3498db;

        }
        #suggesstion-box, #suggesstion-box_customer li{
            list-style-type: none;
            cursor: pointer;
            padding: 5px;
            padding-left: 30px;
        }
        #suggesstion-box, #suggesstion-box_customer li:hover{
            color: #3498db;

        }
</style>
<section >
    <div class="container">
        @php
        $bankNames = [
            1 => 'AB Bank Limited',
            2 => 'Al-Arafah Islami Bank Limited',
            3 => 'Bangladesh Commerce Bank Limited',
            4 => 'Bangladesh Krishi Bank',
            5 => 'Bank Asia Limited',
            6 => 'Bengal Commercial Bank Limited',
            7 => 'BRAC Bank Limited',
            8 => 'City Bank',
            9 => 'Community Bank Bangladesh Limited',
            10 => 'Dhaka Bank Limited',
            11 => 'Dhaka Mercantile Co-Operative Bank Limited',
            12 => 'Dutch Bangla Bank',
            13 => 'Eastern Bank Limited',
            14 => 'First Security Islami Bank Limited',
            15 => 'Habib Bank Limited',
            16 => 'Islami Bank Bangladesh Limited',
            17 => 'Jamuna Bank Limited',
            18 => 'Janata Bank',
            19 => 'Meghna Bank Limited',
            20 => 'Mercantile Bank Limited',
            21 => 'Midland Bank Limited',
            22 => 'Modhumoti Bank Limited',
            23 => 'Mutual Trust Bank Limited',
            24 => 'National Bank Limited',
            25 => 'National Credit & Commerce Bank Limited',
            26 => 'NRB Bank Limited',
            27 => 'NRB Commercial Bank Limited',
            28 => 'One Bank Limited',
            29 => 'Padma Bank Limited',
            30 => 'Premier Bank Limited',
            31 => 'Prime Bank Limited',
            32 => 'Pubali Bank Limited',
            33 => 'Shahjalal Islami Bank Limited',
            34 => 'Shimanto Bank Limited',
            35 => 'Sonali Bank',
            36 => 'Southeast Bank Limited',
            37 => 'South Bangla Agriculture and Commerce Bank Limited',
            38 => 'Standard Chartered',
            39 => 'United Commercial Bank PLC',
            40 => 'Uttara Bank Limited',
        ];

        $all_accounts_name = [
            'maak101' => 'Office Rent',
            'maak102' => 'Warehouse Rent',
            'maak103' => 'Office Utilities',
            'maak104' => 'Marketing Online',
            'maak105' => 'Advertisement',
            'maak106' => 'Business Promotion',
            'maak107' => 'IP Phone Bill',
            'maak108' => 'Mobile Bill',
            'maak109' => 'Festival Allowance',
            'maak110' => 'Bridge Toll and Parking',
            'maak111' => 'Gift and Donation',
            'maak112' => 'Electric Goods',
            'maak113' => 'Entertainment Expenses',
            'maak114' => 'Fuel Oil',
            'maak115' => 'Internet and Telephone Line',
            'maak116' => 'License and Fees',
            'maak117' => 'Medical Expenses',
            'maak118' => 'Office Maintenance',
            'maak119' => 'Printing Expenses',
            'maak120' => 'Stationery Expenses',
            'maak121' => 'Electricity Bill',
            'maak122' => 'Repair and Maintenance Others',
            'maak123' => 'Accessories Expenses',
            'maak124' => 'Carrying Expenses',
            'maak125' => 'Product Damage and Lost Expenses',
            'maak126' => 'Fine and Penalty',
            'maak127' => 'Load and Unload Labour Bill',
            'maak128' => 'Lunch and Tiffin',
            'maak129' => 'Postage and Courier',
            'maak130' => 'Employee Loan and Other Payments',
            'maak131' => 'Legal Cost',
            'maak132' => 'Monthly Incentive Payments',
            'maak133' => 'Website Expenses',
            'maak134' => 'Bank Interest All',
            'maak135' => 'Miscellaneous Expenses',
            'maak136' => 'VAT Tax',
            'maak137' => 'Bank',
            'maak138' => 'Sales',
            'maak139' => 'Purchase',
            'maak140' => 'Funds Transfer',
            'maak141' => 'Funds Receipt',
        ];

    @endphp
        <form action="{{route('accounts.cashbook.store_maakview_voucher')}}" method="POST">
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h2 class="text-center bg-success" style="color: #fff">Create Maakview Voucher</h2>
                    <div >
                        @if (session()->has('expenses_statsu'))
                        <div class=" notification alert alert-danger text-center col-md-12">
                            {{ session('expenses_statsu') }}
                        </div>
                        @endif
                    </div>
                    
                    <div class="d-flex flex-column align-items-center">
                        <div class="mb-3">
                            <label for="date" class="form-label"> Voucher Date</label>
                            <input type="date" class="form-control custom-width" id="date" name="voucher_date" required>
                        </div>
                    
                        <div class="mb-3">
                            <label for="accounts_category" class="form-label">Select Accounts Category</label><br>
                            <select class="form-control custom-width aiz-selectpicker" id="accounts_category"  data-live-search="true" name="accounts_code" required >
                                <option selected value="">Select Accounts Category</option>
                                    @foreach ($all_accounts_name as $key => $item)
                                        <option value="{{ $key }}">{{ $item }}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="mb-3" id="bankDetails" style="display: none">
                            <label for="bank_details" class="form-label">Select Bank</label><br>
                            <select class="form-control custom-width aiz-selectpicker" id="bankCategory"  data-live-search="true" name="bank_name" >
                                <option selected value="">Select Bank</option>
                                    @foreach ($bankNames as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="mb-3" id="customer_id" style="display: none">
                            <label for="bank_details" class="form-label">Select Customer</label><br>
                            <input type="text" id="customer_serch_id" class="form-control custom-width serch_customer">
                            <table class="table table-striped">
                                <tbody id="append_customer_data" class="mt-2">
                
                                </tbody>
                            </table>
                        </div>
                        <div class="mb-3" id="supplier_id" style="display: none">
                            <label for="bank_details" class="form-label">Select Supplier</label><br>
                            <input type="text" id="supplier_serch_id" class="form-control custom-width serch_customer">
                            <table class="table table-striped">
                                <tbody id="append_supplier_data" class="mt-2">
                
                                </tbody>
                            </table>
                        </div>
                        <div id="suggesstion-box_customer" class="search-box_popup text-center"></div>
                        <div class="mb-3">
                            <label for="boucher_category" class="form-label">Select Voucher Category</label><br>
                            <select  class="form-control custom-width aiz-selectpicker" id="voucher_category"  data-live-search="true" name="voucher_category" required >
                                <option value="">Select Voucher Category</option>
                                <option value="cash_debit">Debit</option>
                                <option value="cash_credit">Credit</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="accounts_descriptoin" class="form-label">Voucher values</label>
                            <input type="number" class="form-control custom-width" id="voucher_value" name="voucher_value" required>
                        </div>
                        <div class="mb-3">
                            <label for="voucher_value" class="form-label">Voucher Description</label>
                            <textarea required name="accounts_descriptoin"  class="form-control custom-width" style="resize: none"></textarea>
                        </div>
                        <div class="mb-3">
                            <input id="submit_button" type="submit" class="btn btn-primary custom-width" >
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">
//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}

$('#accounts_category').change(function (e) { 
    e.preventDefault();

    $accounts_category_values = e.target.value;
    if($accounts_category_values == 'maak137'){
        $('#bankCategory').prop('required', true);
        $('#customer_id').hide();
        $('#supplier_id').hide();
        $('#bankDetails').show();
        
    }else if($accounts_category_values == 'maak138'){
        console.log('sales');
        $('#supplier_id').hide();
        $('#customer_id').show();
        
    }else if($accounts_category_values == 'maak139'){
        console.log('purchase');
        $('#bankCategory').hide();
        $('#customer_id').hide();
        $('#supplier_id').show();
       
    }else{
        $('#bankCategory').hide();
        $('#customer_id').hide();
        $('#supplier_id').hide();
    }


    
});


//code start for customer section
$(".serch_customer").on('keyup', function () {
            $("#suggesstion-box_customer").html("");
            var input_customer_value = $(this).val();
            var search_id_value = $(this).attr('id');
            if(search_id_value == 'supplier_serch_id')
            {
                 hit_url = "{{route('pos.inventory.supplier_search')}}";
            }else if(search_id_value == 'customer_serch_id'){
                 hit_url = "{{route('pos.customer_search')}}";
            }

            $.ajax({
                type: "GET",
                url: hit_url,
                data: {'search':input_customer_value, 'id':""},
                success: function (response) {

                   var data = "";
                   var search_result_data_length = response.data.length;
                   for (let i = 0; i <search_result_data_length; i++) {
                      var customer_name = response.data[i]["name"];
                      var customer_phone = response.data[i]["phone"];
                      var phone_name = customer_name.concat(" (", customer_phone, ")")
                     
                      if(search_id_value == 'supplier_serch_id'){
                        data += '<li onClick="selectSupplierInfo('+response.data[i]["id"] +')">'+phone_name+'</li>'
                      }else{
                        data += '<li onClick="selectCustomerInfo('+response.data[i]["id"] +')">'+phone_name+'</li>'
                      }
                      

                   }

                   $("#suggesstion-box_customer").show();
                   if(input_customer_value !== "")
                   {
                    $("#suggesstion-box_customer").html(data);
                   }
                   else
                   {
                    $("#suggesstion-box_customer").html("");
                   }

                }
            });

        });


         //function for append data after select specefic customer
         var increments = 1;
        function selectCustomerInfo(id)
        {
           
            $.ajax({
                type: "GET",
                url: "{{route('pos.customer_search')}}",
                data:{'search':"",'id':id},
                success: function (response) {

                    var row = 
                        '<tr id="data-row-id_customer" class="customer_row_'+increments+' customer_common">\
                            <td class="text-bold"><input type="hidden" name="customer_id" id="customer_id" class="form-control" value="'+response.id+'">'+response.name+'</td>\
                            <td class="text-bold">'+response.phone+'</td>\
                            <td class="text-bold">Customer</td>\
                            <td class="text-center"><i class="fa fa-trash" onclick="deleteCustomer('+increments+')" style="color:red; cursor:pointer" aria-hidden="true"></i></td>\
                        </tr>';

                    if($('.customer_common').length <1){
                        $('#append_customer_data').append(row);
                        $('#customer_serch_id').val('');
                    }

                    $("#suggesstion-box_customer").hide();


                    increments++;
                }
            });

        }


        //delete customer function
        function deleteCustomer(customer_row_value)
        {

            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $('.customer_row_'+customer_row_value).remove();
                Swal.fire(
                'Deleted!',
                'Customer has been deleted.',
                'success'
                )
               
            }
            });
        }
//end customer

var supplier_increments = 1;
function selectSupplierInfo(id)
{
    console.log(id);
    $.ajax({
        type: "GET",
        url: "{{route('pos.inventory.supplier_search')}}",
        data:{'search':"",'id':id},
        success: function (response) {

            var row = 
                '<tr id="data-row-id_customer" class="customer_row_'+supplier_increments+' supplier_common">\
                    <td class="text-bold"><input type="hidden" name="supplier_id" id="supplier_id" class="form-control" value="'+response.id+'">'+response.name+'</td>\
                    <td class="text-bold">'+response.phone+'</td>\
                    <td class="text-center"><i class="fa fa-trash" onclick="deleteCustomer('+supplier_increments+')" style="color:red; cursor:pointer" aria-hidden="true"></i></td>\
                </tr>';

            if($('.supplier_common').length <1){
                $('#append_supplier_data').append(row);
                $('#supplier_serch_id').val('');
            }

            $("#suggesstion-box_customer").hide();


            supplier_increments++;
        }
    });

}



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection
