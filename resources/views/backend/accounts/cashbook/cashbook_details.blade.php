<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        body{
            margin: 0;
            box-sizing: border-box;
        }

        .main-table-data{
            width: 80%;
            margin: 0 auto;

        }
        .table-content{
            width: 100%;
            border-collapse: collapse;
        }
        .table-content th, .table-content td{
            border: 1px solid black;
            text-align: center;
            padding: 10px;
        }
        .table-content tbody tr:hover {
            background-color: #ddd;
        }
        table{
            border: 1px solid black;
            width: 100%;
            border-collapse: collapse;
            text-align: center;
        }
        table ,th ,td{
            border: 1px solid black;
            padding: 10px
        }
    
        
    @media print{
        body{
            -webkit-print-color-adjust: exact;
        }
        .no-print{
            display: none;
        }
        

    }

    .print-date{
            position: absolute;
            bottom: 0;
            right: 0;
            font-size: 14px;
        }
    
       
    </style>
</head>
<body>
    <div style="text-align:center">
        <button class="btn btn-success no-print mt-1 " onclick="printFun()"><i class="fa fa-print" aria-hidden="true"></i> Print</button> 
    </div>
    <div>
        @php
           

        function getAccountNameFromCode($account_code)
        {
            $all_accounts_name = [
            'maak101' => 'Office Rent',
            'maak102' => 'Warehouse Rent',
            'maak103' => 'Office Utilities',
            'maak104' => 'Marketing Online',
            'maak105' => 'Advertisement',
            'maak106' => 'Business Promotion',
            'maak107' => 'IP Phone Bill',
            'maak108' => 'Mobile Bill',
            'maak109' => 'Festival Allowance',
            'maak110' => 'Bridge Toll and Parking',
            'maak111' => 'Gift and Donation',
            'maak112' => 'Electric Goods',
            'maak113' => 'Entertainment Expenses',
            'maak114' => 'Fuel Oil',
            'maak115' => 'Internet and Telephone Line',
            'maak116' => 'License and Fees',
            'maak117' => 'Medical Expenses',
            'maak118' => 'Office Maintenance',
            'maak119' => 'Printing Expenses',
            'maak120' => 'Stationery Expenses',
            'maak121' => 'Electricity Bill',
            'maak122' => 'Repair and Maintenance Others',
            'maak123' => 'Accessories Expenses',
            'maak124' => 'Carrying Expenses',
            'maak125' => 'Product Damage and Lost Expenses',
            'maak126' => 'Fine and Penalty',
            'maak127' => 'Load and Unload Labour Bill',
            'maak128' => 'Lunch and Tiffin',
            'maak129' => 'Postage and Courier',
            'maak130' => 'Employee Loan and Other Payments',
            'maak131' => 'Legal Cost',
            'maak132' => 'Monthly Incentive Payments',
            'maak133' => 'Website Expenses',
            'maak134' => 'Bank Interest All',
            'maak135' => 'Miscellaneous Expenses',
            'maak136' => 'VAT Tax',
            'maak137' => 'Bank',
            'maak138' => 'Sales',
            'maak139' => 'Purchase',
        ];

        if(array_key_exists($account_code, $all_accounts_name)){
            return $all_accounts_name[$account_code];
        }else{
            return '';
        }

        }

        $dateString = $date;
        $formattedDate = \Carbon\Carbon::createFromFormat('Y-m-d', $dateString)->format('Y-M-d');
        @endphp
        <header style="font-size: 18px">
            <div class="logo" style="margin: auto; text-align:center">
                <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png"  height="75" alt="Logo"> 
            </div>
            <div class="text-body" style="margin: auto; text-align:center">
                <p style="margin-bottom: 0;">Address: Rahima Plaza(6th floor), 82/3 Laboratory Road, Dhaka-1205, Email: maakview.info@gmail.com</p>
                <p style="margin-top: 0;margin-bottom: 0; padding-top:0">Website: www.maakview.com, Phone: +8801888-012727, +880244613763</p>
                <p style="margin-top: 0;margin-bottom: 0; padding-top:0">Statement Of Cash & Bank Book - {{$formattedDate}}</p>
                <p style="margin-top: 0; padding-top:0; font-weight:bold">Cash In Hand : {{number_format($_previous_day_balanace_cash + ($_get_all_sum_cashbook_date_wise_current[0]->total_cash_debit - $_get_all_sum_cashbook_date_wise_current[0]->total_cash_credit), 2, '.', '')}}</p>
            </div>
        </header>
        <main style="font-size: 18px">
            <table class="table-content">
                <thead>
                    <tr>
                        <th rowspan="2">Voucher No</th>
                        <th rowspan="2">Account Name</th>
                        <th rowspan="2" colspan="2">Description</th>
                        <th colspan="2">Cash</th>
                        <th colspan="2">Bank</th>
                    </tr>
                    <tr>
                        <td>Receipt(Debit)</td>
                        <td>Payment(Credit)</td>
                        <td>Receipt(Debit)</td>
                        <td>Payment(Credit)</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="4" style="text-align: right">Opening Balance</td>
                        <td>{{number_format($_previous_day_balanace_cash, 2, '.', '')}}</td>
                        <td>0.00</td>
                        <td>{{number_format($_previous_day_balanace_bank, 2, '.', '')}}</td>
                        <td>0.00</td>
                    </tr>
                    @foreach ($_get_all_data_cashbook_date_wise_current as $item)
                        <tr>
                            <td>{{$item->voucher_number}}</td>
                            <td>{{$item->accounts_code. ', '.getAccountNameFromCode($item->accounts_code)}}</td>
                            <td colspan="2">{{$item->accounts_descriptoin}}</td>
                            <td>{{number_format($item->cash_debit, 2, '.', '')}}</td>
                            <td>{{number_format($item->cash_credit, 2, '.', '')}}</td>
                            <td>{{number_format($item->bank_debit, 2, '.', '')}}</td>
                            <td>{{number_format($item->bank_credit, 2, '.', '')}}</td>
                        </tr>
                    @endforeach
                    <tr style="font-weight: bold">
                        <td colspan="4" style="text-align: right">Sum of Transactoin</td>
                        <td>{{number_format($_get_all_sum_cashbook_date_wise_current[0]->total_cash_debit, 2, '.', '')}}</td>
                        <td>{{number_format($_get_all_sum_cashbook_date_wise_current[0]->total_cash_credit, 2, '.', '')}}</td>
                        <td>{{number_format($_get_all_sum_cashbook_date_wise_current[0]->total_bank_debit, 2, '.', '')}}</td>
                        <td>{{number_format($_get_all_sum_cashbook_date_wise_current[0]->total_bank_credit, 2, '.', '')}}</td>
                    </tr>
                    <tr style="font-weight: bold">
                        @php
                            $closing_balance_cash = $_previous_day_balanace_cash + ($_get_all_sum_cashbook_date_wise_current[0]->total_cash_debit - $_get_all_sum_cashbook_date_wise_current[0]->total_cash_credit);
                            $closing_balance_bank = $_previous_day_balanace_bank + ($_get_all_sum_cashbook_date_wise_current[0]->total_bank_debit - $_get_all_sum_cashbook_date_wise_current[0]->total_bank_credit);
                        @endphp
                        <td colspan="4">Closing Balanace</td>
                        <td>{{number_format($closing_balance_cash, 2, '.', '')}}</td>
                        <td></td>
                        <td>{{number_format($closing_balance_bank, 2, '.', '')}}</td>
                        <td></td>
                    </tr>
                    
                </tbody>
            </table>
        </main>
        <footer style=" position: fixed; bottom: 0; width: 95%;">
            <div class="div" style="display: flex; justify-content: space-between; margin-bottom:10px">
                <div class="mb-2" style="margin-left: 0;">
                    <ul style="list-style-type: none; margin: 0; padding-left: 0;">
                        <li>........................................</li>
                        <li style="font-weight: bold; padding-left: 20px;">Prepared By</li>
                    </ul>
                </div>
                <div class="mb-2" style="margin-left: 0;">
                    <ul style="list-style-type: none; margin: 0; padding-left: 0;">
                        <li>........................................</li>
                        <li style="font-weight: bold; padding-left: 20px;">Checked By</li>
                    </ul>
                </div>
                <div class="mb-2" style="margin-left: 0;">
                    <ul style="list-style-type: none; margin: 0; padding-left: 0;">
                        <li>...................................</li>
                        <li style="font-weight: bold; padding-left: 20px;">Approved By</li>
                    </ul>
                </div>
            </div>
        </footer>
        
        <div class="print-date">
            Date: {{ date('Y-m-d h:i A') }}
        </div>
    </div>
        

    
        
  



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">

    let printFun = () => {
                window.print();
                
            }

    </script>
</body>
</html>