@extends('backend.layouts.app')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<style>
    
</style>

<div class="card">
    
    <div class="card-body">
       
            @csrf

            <div class="card-title">
                <h4 class="text-center m-auto bg-primary text-white" style="text-decoration: underline;">Create MR Trade Money Receipt</h4>
            </div>

            <div class="m-auto">
                <div class="row">
                    <div class="col-md-12 offset-md-2">
                        <div class="category_search flex-fill p-1 mb-3">
                            <div class="col-md-12 offset-md-2">
                                <div class="row mt-2">
                                    <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                        Customer Account name:
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control serch_customer" id="customer_serch_id" placeholder="Search Customer ">
                                    </div>
                                </div>
                             
                             <div id="suggesstion-box_customer" class="search-box_popup" style="padding: 20px;font-size: 15px;font-weight:bold;"></div>

                             <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                    Supplier Account name:
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control serch_customer" id="supplier_search_id" placeholder="Search Supplier ">
                                </div>
                             </div>
                             
                             <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                    Total Payment:
                                </div>
                                <div class="col-md-3" >
                                   
                                <input type="number" class="form-control" id="amount" placeholder="Enter the amount">
                                </div>
                             </div>
                             
                             <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                  Account Name:
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id= "account_name" name="account_name" required  class="form-control">
                                </div>
                            </div>

                             <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                    Checque No:
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id= "checque_number" name="checque_number" required  class="form-control">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                    Cheque Date:
                                </div>
                                <div class="col-md-3">
                                    <input type="date"  id= "checque_date" name="checque_date"  id="datepicker"  class="form-control">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                Against Bill Info:
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="bill_info" name="bill_info" required  class="form-control">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                    Bill Date:
                                </div>
                                <div class="col-md-3">
                                    <input type="date"  id= "bill_date" name="bill_date"  id="datepicker"  class="form-control">
                                </div>
                            </div>
                            
                            </div>
                            
                          
                        </div>
                    </div>
                    

                   
                </div>
            </div>
            
                





               
                {{-- customer data --}}
                {{-- <form action="" id="order_create-form">
                       

                        <button type="button" id="print-button">Print</button>

                </form> --}}

             
    </div>
    
</div>
    

@endsection

@section('script')
    
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    })
</script>


<script>
  


  //code start for customer section
  $(".serch_customer").on('keyup', function () {
            $("#suggesstion-box_customer").html("");
            var input_customer_value = $(this).val();
            var search_id_value = $(this).attr('id');
            if(search_id_value == 'supplier_search_id')
            {
                hit_url = "{{route('pos.inventory.supplier_search')}}";
            }else if(search_id_value == 'customer_serch_id'){
                hit_url = "{{route('pos.customer_search')}}";
            }
            
            $.ajax({
                type: "GET",
                url: hit_url,
                data: {'search':input_customer_value, 'id':""},
                success: function (response) {
                   
                   var data = "";
                   var search_result_data_length = response.data.length;
                   for (let i = 0; i <search_result_data_length; i++) {
                      var customer_name = response.data[i]["name"];
                      var customer_phone = response.data[i]["phone"];
                      var phone_name = customer_name.concat(" (", customer_phone, ")")
                     
                      if(search_id_value == 'supplier_search_id'){
                        data += '<li onClick="selectSupplierInfo('+response.data[i]["id"] +')">'+phone_name+'</li>'
                      }else{
                        data += '<li onClick="selectCustomerInfo('+response.data[i]["id"] +')">'+phone_name+'</li>'
                      }
                      

                   }

                   $("#suggesstion-box_customer").show();
                   if(input_customer_value !== "")
                   {
                    $("#suggesstion-box_customer").html(data);
                   }
                   else
                   {
                    $("#suggesstion-box_customer").html("");
                   }

                }
            });

        });
        

        // Search supplier

         




        //function for append data after select specefic customer
        var increments = 1;
        function selectCustomerInfo(id)
        {
            $.ajax({
                type: "GET",
                url: "{{ route('pos.customer_search') }}",
                data: { 'search': "", 'id': id },
                success: function (response) {
                    var customerName = response.name;

                    // Get the amount input value
                    var amountValue = $('#amount').val();
                    var accountName = $('#account_name').val();
                    var checqueNumber = $('#checque_number').val();
                    var checqueDate = $('#checque_date').val();
                    var billInfo = $('#bill_info').val();
                    var billDate = $('#bill_date').val();

                    // Construct the route URL with both customerName and amount parameters
                    var routeUrl = "{{ route('demo_money_receipt.view') }}?customerName=" + customerName + "&stash=" + amountValue + "&name=" + accountName + "&CNumber=" + checqueNumber + "&CDate=" + checqueDate + "&info=" + billInfo + "&info_date=" + billDate;

                    // Open the view in a new window
                    window.open(routeUrl, '_blank');
                }
            });

        }

        // Supplier field Append

                function selectSupplierInfo(id) {
            $.ajax({
                type: "GET",
                url: "{{ route('pos.inventory.supplier_search') }}",
                data: { 'search': "", 'id': id },
                success: function(response) {
                    var supplierName = response.name;

                    var amountValue = $('#amount').val();
                    var accountName = $('#account_name').val();
                    var checqueNumber = $('#checque_number').val();
                    var checqueDate = $('#checque_date').val();
                    var billInfo = $('#bill_info').val();
                    var billDate = $('#bill_date').val();

                    var routeUrl = "{{ route('mrt_money_receipt.view') }}?supplierName=" + supplierName + "&stash=" + amountValue + "&name=" + accountName + "&CNumber=" + checqueNumber + "&CDate=" + checqueDate + "&info=" + billInfo + "&info_date=" + billDate;

                    window.open(routeUrl, '_blank');
                }
            });
        }

          //customer validation and add
            
          
                
          
          
          $('#add_customer').on('click', function (e) {
           e.preventDefault();
           var data = {
               'name' : $('.customer_name').val(),
               'phone' : $('.customre_phone').val(),
               'state' : $('#state').val(),
               'city' : $('#city').val(),
               'customer_address' : $('#customer_address').val(),
               'customer_postal_code' : $('#customer_postal_code').val(),
               
           }
           if(data.name.trim() == "")
           {
            $('.customer_name').addClass("border border-danger");
            $('.customer_name').focus();
           }
           if(data.phone.trim() == "")
           {
            $('.customre_phone').addClass("border border-danger");
            $('.customre_phone').focus();
           }
           if(data.customer_address.trim() == "")
           {
            $('#customer_address').addClass("border border-danger");
            $('#customer_address').focus();
           }
          
           if(data.name != "" && data.phone != "" && data.customer_address != "")
           {
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

            $.ajax({
                type: "POST",
                url: "{{route('pos.customer_store')}}",
                data: data,
                success: function (response) {
                    if(response.status == 200)
                    {
                        
                        $('#close_customer_modal').trigger('click');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                            });
                       
                        $('.modal-body').find('input').val('');
                    }
                    else if(response.status == 403)
                    {
                        Swal.fire({
                            icon: 'error',
                            text: response.message
                        });
                    }

                }
            });

            

            
            
        }
           
                });

        
        // Add Supplier

        $('#add_supplier').on('click', function (e) {
    e.preventDefault();
    var data = {
        'name': $('.supplier_name').val(),
     
        // Add other supplier-related fields as needed
    }

    if (data.name.trim() == "") {
        $('.supplier_name').addClass("border border-danger");
        $('.supplier_name').focus();
    }
    // Add similar validation checks for other fields

    if (data.name != "" && data.phone != "" && data.address != "" && data.email != "") {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "{{ route('pos.inventory.supplier_store') }}",
            data: data,
            success: function (response) {
                if (response.status == 200) {

                    $('#close_supplier_modal').trigger('click');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    $('.modal-body').find('input').val('');
                } else if (response.status == 403) {
                    Swal.fire({
                        icon: 'error',
                        text: response.message
                    });
                }
            }
        });
    }
});

        

       

        //search result pop up responsive
        $('#customer_serch_id').keyup(function (e) { 
            $('#suggesstion-box_customer').css('marginLeft', '35px');
        });

        $('#supplier_search_id').keyup(function (e) { 
            $('#suggesstion-box_customer').css('marginLeft', '35px');
        });



         //delete customer function
         function deleteCustomer(customer_row_value)
        {


            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $('.customer_row_'+customer_row_value).remove();
                Swal.fire(
                'Deleted!',
                'Customer has been deleted.',
                'success'
                )
               
            }
            });
        }
    //end customer section




</script>



    
@endsection