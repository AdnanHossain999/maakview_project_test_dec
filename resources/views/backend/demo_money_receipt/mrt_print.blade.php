<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        body{
            margin: 0;
            box-sizing: border-box;
         
        }

        table
            {
            border-collapse: separate;
            border-spacing: 0 8px;
            }

        .print-offc{
            position: absolute;
            top: 816px;
            right: 0;
            font-size: 14px;
        }
        .print-custo{
            position: absolute;
            top: 30px;
            right: 0;
            font-size: 14px;
        }
    
       
    </style>
</head>
<body>

    {{-- customer copy --}}
    <div class="container" style="border: 1px dotted #3a3a3a; margin-top:67px; margin-bottom:65px">
        <button style="margin-left: 87%; margin-top:5px; border:none;background:#6A4FE0;">Customer Copy</button>
        
        <div class="logo" style="margin: auto; text-align:center">
            <img class="m-0 p-0" src="{{asset('logo')}}/mr_trade.png"  height="75" alt="Logo"> 
        </div>
        <div class="text-body" style="margin: auto; text-align:center">
            <p style="margin-bottom: 0;">Address: Rahima Plaza(6th floor), 82/3 Laboratory Road, Dhaka-1205, Email: deshtechnology2016@gmail.com</p>
            <p style="margin-top: 0; padding-top:0">Website: www.maakview.com, Phone: +8801888012727, +880244613763</p>
        </div>
        <div class="money_receipt_title" style="margin: auto; text-align:center">
            <button style="border: none; padding:10px; background-color:#6A4FE0; border-radius:6px; font-weight:bold; width:280px;font-size:20px">MRT Money Receipt</button>
        </div>
       

        <div class="main_contain" style="margin: auto;width:90%">
            <div style="display: flex; justify-content: space-between;">
                <div style="border: 1px solid black; width: 200px;padding:2px;font-size: 16px;text-align:center">
                    {{-- Invoice No: {{$money_receipt_data->order_invoice}} --}}

                    {{ $serialNumber }}
                </div>
                <div style="border: 1px solid black; width: 200px;padding:2px;font-size: 16px">
                    Date: <?php echo date("d-m-Y"); ?>
                </div>
            </div>
            <div class="main_body">
                <table style="width: 100%;">
                    <thead>
                        <tr style="padding-bottom:10px">
                            <td colspan="12" style="border-bottom: 1px dotted #3a3a3a;font-size:18px"><span style="font-weight: bold;padding-bottom: 3px;  border-bottom: 1px solid white">Received with thanks from :</span><span style="padding-left: 5px;font-weight:bold;">
                                @if(isset($customerName))
                                    {{ $customerName }}
                                @elseif(isset($supplierName))
                                    {{ $supplierName }}
                                @endif
                            </span></td>
                        </tr>
                      
                        <tr>
                            <td colspan="12" style="border-bottom: 1px dotted #3a3a3a; font-size: 18px">
                                <span style="padding-bottom: 3px; border-bottom: 1px solid white">
                                    The sum of taka (in words): 
                                </span>
                                <span id="amountInWords" style="padding-left: 5px"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" style="border-bottom: 1px dotted #3a3a3a;font-size:18px"><span style="padding-bottom: 3px;  border-bottom: 1px solid white">On account of : </span><span style="padding-left: 20px">{{$account_name}}</span></td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 1px dotted #3a3a3a;width:40%;font-size:18px" ><span style="padding-bottom: 3px;  border-bottom: 1px solid white">By cash/cheque no : </span><span style="padding-left: 5px">@php
                                echo (!empty($checque_number)) ? $checque_number : 'Cash';
                            @endphp</span></td>
                            <td style="border-bottom: 1px dotted #3a3a3a;width:30%;font-size:18px" ><span style="padding-bottom: 3px;  border-bottom: 1px solid white">Date: </span><span style="padding-left: 5px">@php
                                 echo (!empty($formatted_checque_date)) ? $formatted_checque_date : '';
                            @endphp</span></td>
                            {{-- <td style="border-bottom: 1px dotted #3a3a3a;width:29%;font-size:18px" ><span style="font-weight: bold;padding-bottom: 3px;  border-bottom: 1px solid white">as advance part/full payment</span></td> --}}
                        </tr>
                    </thead>
                </table>
                <table style="width: 100%">
                     <thead> <tr>
                        <td style="border-bottom: 1px dotted #3a3a3a;width:75%;font-size:18px"><span style="padding-bottom: 3px;  border-bottom: 1px solid white">Against the bill : </span><span style="padding-left: 5px">{{$bill_info}}</span></td>
                        <td style="border-bottom: 1px dotted #3a3a3a;width:25%;font-size:18px"><span style="padding-bottom: 3px;  border-bottom: 1px solid white">Dated: </span><span style="padding-left: 5px">{{$formatted_bill_date}}</span></td>
                    </tr></thead>
                </table>
            </div>
        

        <div style="display: flex; justify-content: space-between; margin-top:150px; margin-bottom:15px">
            <div style="border: 1px solid black;padding-left:10px;padding-right:20px;font-size: 18px">
                {{-- Taka:{{number_format((float)$money_receipt_data->total_payment_in_number, 2, '.', '')}} --}}
                Taka:{{number_format((float)$amount, 2, '.', '')}}
            </div>
            <div>
                <span style="text-decoration: overline;font-size: 18px">Customer Signature</span>
            </div>
            <div>
                <span style="text-decoration: overline;font-size: 15px"> For M.R Trade International</span>
            </div>
        </div>
        </div>
    </div>
    
    <div class="print-custo">
        Date: {{ date('Y-m-d h:i A') }}
    </div>
  



    











    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">
    try {
            
            if( $(document).height() > 1100 ){
                $('.footer').css('position','inherit');
                $('.footer').css('padding-top','50px');
                $('.footer').css('clear','both');
            }else{
                $('.footer').css('position','fixed');
                $('.footer').css('bottom','0');
                $('.footer').css('clear','both');
            }

            this.print();
            
    
            } catch (e) {
                window.onload = window.print;
            }
            window.onbeforeprint = function() {
                setTimeout(function() {
                    window.close();
                }, 1500);
            }

            function numberToWords(number) {  
                var digit = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];  
                var elevenSeries = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];  
                var countingByTens = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];  
                var shortScale = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];  
        
                number = number.toString(); number = number.replace(/[\, ]/g, ''); if (number != parseFloat(number)) return 'not a number'; var x = number.indexOf('.'); if (x == -1) x = number.length; if (x > 15) return 'too big'; var n = number.split(''); var str = ''; var sk = 0; for (var i = 0; i < x; i++) { if ((x - i) % 3 == 2) { if (n[i] == '1') { str += elevenSeries[Number(n[i + 1])] + ' '; i++; sk = 1; } else if (n[i] != 0) { str += countingByTens[n[i] - 2] + ' '; sk = 1; } } else if (n[i] != 0) { str += digit[n[i]] + ' '; if ((x - i) % 3 == 0) str += 'hundred '; sk = 1; } if ((x - i) % 3 == 1) { if (sk) str += shortScale[(x - i - 1) / 3] + ' '; sk = 0; } } if (x != number.length) { var y = number.length; str += 'point '; for (var i = x + 1; i < y; i++) str += digit[n[i]] + ' '; } str = str.replace(/\number+/g, ' '); return str.trim();  
        
            }


            var amount = {{ $amount }};
            var amountInWords = numberToWords(amount) + " " + "only";
            // console.log(amountInWords);

            // Update the DOM element with the converted amount
            document.getElementById('amountInWords').innerText = amountInWords;
    

    </script>
</body>
</html>