@extends('backend.layouts.app')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css"/>
@section('content')
<style>

</style>
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-12">
            <h2 class="bg-primary  text-center" style="color:white;">M.R Trade Demo Money Receipt LIST</h2>
                @if (session()->has('delete_status'))
                <div class=" notification alert alert-danger col-md-12">
                    {{ session('delete_status') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="pull-right clearfix">

            </div>
        </div>
        <div class="card-body"> 
        <div class="row">
        <div class="col-md-12 pr-3">
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm p-2 text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th scope="col">SL</th>
                    <th scope="col">Total Payments</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Supplier Name</th>
                    <th scope="col">Account Name</th>
                    <th scope="col">Checkque Number</th>
                    <th scope="col">Checkque Date</th>
                    <th scope="col">Bill Information</th>
                    <th scope="col">Bill Date</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($get_all_demo_money_receipt as $key => $item)
                      <tr>
                        <td>{{ $key + 1 + ($get_all_demo_money_receipt->currentPage() - 1) * $get_all_demo_money_receipt->perPage() }}</td>
                        {{-- <td>{{$item->serial_no}}</td> --}}
                        <td>{{$item->total_payment_in_number}}</td> 
                        <td>{{$item->customer_account_name}}</td>
                        <td>{{$item->supplier_account_name}}</td>
                        <td>{{$item->account_name}}</td>
                        <td>{{$item->checque_number}}</td>
                        <td>{{$item->checque_date}}</td>
                        <td>{{$item->bill_info}}</td>
                        <td>{{$item->bill_date}}</td>
                        <td>
                          <a href="{{route('mrt_money_receipt_print_view.print', $item->id)}}" class="btn btn-sm btn-primary">Print</a>
                          <a href="{{route('mrt_money_receipt.delete', $item->id)}}" class="btn btn-sm btn-danger">Delete</a>
                          

                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>

              <div>
                {{ $get_all_demo_money_receipt->links() }}
              </div>
        </div>
    </div>
        </div>
    </div>
@endsection




@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');


//delete Confirmation code
$('.delete-confirm').click(function(event){
   event.preventDefault();
   var url = $(this).attr('href');
    swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover !",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    })
    .then((willDelete) => {
    if (willDelete) {
        window.location.href = url;
        swal("Your quotation has been deleted!", {
        icon: "success",
        });
    } else {
        swal("Your quotation is safe!");
    }
    });

});


});




//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}


</script>
@endsection
