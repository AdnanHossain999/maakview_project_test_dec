<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        body{
            margin: 0;
            box-sizing: border-box;
        }
        table {
            width: 100%;
           
        }

     

        table.padding th {
            padding: 0 .8rem;
        }

        table.padding td {
            padding: .8rem;
        }

        table.sm-padding td {
            padding: .5rem .7rem;
        }

        table.lg-padding td {
            padding: 1rem 1.2rem;
        }

        .border-bottom td,
        .border-bottom th {
            border: 1px solid #eceff4;
        }

       

        .bold {
            font-weight: bold
        }
        
        .customer_border{
            border: 1px dotted #000000;
            padding-top: 5px;
            margin-bottom: 0px;
        }
        .customer_information li{
            list-style-type: none;
        }

        .print-offc{
            position: absolute;
            top: 0;
            right: 0;
            font-size: 14px;
        }

        .custom-alert {
        display: none;
        position: fixed;
        top: 0;
        left: 50%;
        transform: translateX(-50%);
        background-color: #4CAF50;
        color: white;
        text-align: center;
        padding: 15px;
        z-index: 9999;
    }

    .custom-alert button {
        background-color: transparent;
        border: none;
        color: white;
        cursor: pointer;
    }

    
       
    </style>
</head>
<body>

    <div>
        <div id="custom-alert" class="custom-alert">
            <span id="custom-alert-message"></span>
            <button onclick="closeCustomAlert()">Close</button>
        </div>

        <div class="row">
            <div class="col-md-12 ">
                <table class="table p-0 m-0">
                    <tbody>
                        <tr>
                            <td class="m-0">
                                <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png" width="200" height="50" alt="Logo"> 
                            </td>
                            <td class="ml-0">
                                <p class="office_address text-right" style="font-size: 15px">
                                    www.maakview.com <br>
                                    Rahima Plaza(6th Floor),<br>
                                   <span style="display: block; padding-bottom:10px"> 82/3 Laboratory Road, Dhaka-1205 <br>Phone: +8801888-012727, +8801886-531777, +880244613763</span>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div>
            <h2 class="text-center" style="text-decoration: underline;margin-bottom: 50px;">CHALLAN</h2>
        </div>

        <div style="width:100%; margin-top:0;margin-bottom: 3px">
            <div class="customer_border mb-0 mt-0 pb-0  pt-0 d-flex justify-content-between">
                
              <ul class="customer_information mb-0 mt-0  pb-0  pt-1 pb-1" style="margin-left: 0px;padding-left: 5px">
                  {{-- <li>Invoice Number : {{$order->combined_order->code}}</li> --}}
                  <li>{{ translate('Customer Name') }}: {{ $customerName}}</li>
                   {{-- @if ($order->billing_address !== null  && !empty($order->billing_address))
                       @php
                       $user_info = json_decode($order->billing_address);
                       @endphp
                       <li >{{ translate('Customer Phone') }}: {{ $user_info->phone}}</li>
                       <li>{{ translate('Address') }}: {{ $user_info->address}},{{ $user_info->city}},{{ $user_info->state}}-{{ $user_info->postal_code}}</li> 
                   @else --}}
                    <li >{{ translate('Customer Phone') }}: {{ $customerPhone}}</li>

                    {{-- <li >{{ translate('Customer Phone') }}: {{ $productData}}</li> --}}
                    {{-- <li >{{ translate('Address') }}: @php if(!empty($user_info[0]->address_info)){echo $user_info[0]->address_info->address;} @endphp</li> --}}
                  {{-- @endif --}}
              </ul>
              <div class="pt-1 pb-1 " style="margin-left: 0; padding-right: 5px" >
                  <ul style="list-style-type:none; margin:0;padding-left:0">
                      <li >Date: {{$formatted_date}}</li>
                      <li>Sold By: ______________________</li>
                      
                        <li>Prepared By: _________________</li>
                      
                      <li>Approved By: Mr.Motiur Rahman</li>
                  </ul>
              </div>
            </div>
            
        </div>

        <table class="table table-bordered">
            <thead>
                <tr >
                    <th class=" font-weight-bold" style="color: #000000;font-size:15px;padding-bottom:0px;padding-top:0px;text-align:center"><nobr>SL</nobr></th>
                    <th class=" font-weight-bold" style="color: #000000;font-size:15px;padding-bottom:0px;padding-top:0px;text-align:center">Product Name</th>
                    
                    <th class=" font-weight-bold" style="color: #000000;font-size:15px;padding-bottom:0px;padding-top:0px;text-align:center"><nobr>QTY</nobr></th>                  
                </tr>
            </thead>
            @php
            $total_quantity_product_qty = 0;
                // foreach ($order->orderDetails as $key => $quantity_value) {
                //    $total_quantity_product_qty += $quantity_value->quantity;
                // }
            @endphp
            <tbody>
                @foreach ($products as $key => $product)
                   
                        <tr>
                            <td>{{$key + 1}}</td>
                            
                               

                           
                           
                            <td  style="padding-bottom:0px;padding-top:0px;text-align:start">
                                <span style="display: block; font-size:17px">
                                    <span style="font-size: 20px">{{ $product['productName'] }}</span> 
                                  
                                    
                                    <?php echo '<br>' ?>
                                    <?php echo 'S/N:'; ?>
                                    <?php echo '<br>' ?>
                                   
                                       
                                    {{ $product['productSerial'] }}
                                    

                                
                                </span>
                               
                            </td>

                            <td style="padding-bottom:0px;padding-top:0px;text-align:center">{{ $product['quantity'] }}</td>
                            

                            
                        </tr>
                        
                    
                @endforeach
                
                <tr>
                    <td colspan="2" class="text-right  text-bold" style="font-size: 14px">TOTAL QUANTITY</td>
                    <td class="text-center table_border">{{$totalQuantity}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <footer class="footer" style=" width: 100%;text-align: center;">
        <div class="mb-0 mt-0 pb-0  pt-0 d-flex justify-content-between">
            <ul class="mb-2" style="margin-left: 0px;padding-left: 5px">
                <li>..........................................</li>
                <li>Customer Signatures</li>
            </ul>
            <div class="mb-2" style="margin-left: 0" >
                <ul style="list-style-type:none; margin:0;padding-left:0">
                    <li>.....................................</li>
                    <li>For Maakview</li>
                </ul>
               
            </div>
        </div>
        <div class="mb-0 ml-0 mt-0 pb-0  pt-0 d-flex justify-content-between" style="background: #553eda;">
            <ul  style="list-style-type:none; padding-top:10px; margin:0; padding-left:10;">
                <li class="text-white">82/3 Laboratory Road, Dhaka-1205.</li>
            </ul>
            <div class="pt-2" >
                <ul style="list-style-type:none; ">
                    <li class="text-white">PHONE: +8801888-012727</li>
                </ul>
                
            </div>
            <div class="pt-2  mr-1" >
                <ul style="list-style-type:none;padding-right:10px; ">
                    <li class="text-white">EMAIL: maakview.info@gmail.com</li>
                </ul>
            </div>

          

           
        </div>

        {{-- date edited by Adnan --}}
        <div style="display:flex;justify-content:end;" >
            <ul style="list-style-type:none; margin:0;padding-left:0">
                <li >
                    Date: {{ date('Y-m-d h:i A') }}
                </li>
            </ul>
        </div>

        

        {{-- <div class="print-offc">
            Date: {{ date('Y-m-d h:i A') }}
        </div> --}}

    </footer>
    

    

    











    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    



    <script type="text/javascript">
        try {
            
            if( $(document).height() > 1100 ){
                $('.footer').css('position','inherit');
                $('.footer').css('padding-top','50px');
                $('.footer').css('clear','both');
            }else{
                $('.footer').css('position','fixed');
                $('.footer').css('bottom','0');
                $('.footer').css('clear','both');
            }

            this.print();
            
    
        } catch (e) {
            window.onload = window.print;
        }
        window.onbeforeprint = function() {
            setTimeout(function() {
                window.close();
            }, 1500);
        }

    </script>


  
    
   
</body>
</html>