@extends('backend.layouts.app')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css"/>
@section('content')
<style>

</style>
    <div class="aiz-titlebar text-left mt-2 mb-3">
        <div class="row align-items-center">
            <div class="col-md-12">
            <h2 class="bg-primary  text-center" style="color:white;">Maakview Demo Challan LIST</h2>
                @if (session()->has('delete_status'))
                <div class=" notification alert alert-danger col-md-12">
                    {{ session('delete_status') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="pull-right clearfix">

            </div>
        </div>
        <div class="card-body"> 
        <div class="row">
        <div class="col-md-12 pr-3">
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm p-2 text-center" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th scope="col">SL</th>
                    <th scope="col">Date</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Customer Phone</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Product Serial</th>
                    <th scope="col">Product Quantity</th>
                   
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($get_all_demo_Challan_receipt as $key => $item)
                      <tr>
                        <td>{{ $key + 1 + ($get_all_demo_Challan_receipt->currentPage() - 1) * $get_all_demo_Challan_receipt->perPage() }}</td>
                        <td>{{$item->date}}</td>
                        <td>{{$item->customer_name}}</td> 
                        <td>{{$item->customer_phone}}</td>
                        <td>{{$item->product_name}}</td>
                        <td>{{$item->product_serial}}</td>
                        <td>{{$item->product_quantity}}</td>
                        
                        <td>
                          <a href="{{route('demo_challan.delete', $item->id)}}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
{{-- 
              <div>
                {{ $get_all_demo_Challan_receipt->links() }}
              </div> --}}
        </div>
    </div>
        </div>
    </div>
@endsection




@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');


//delete Confirmation code
$('.delete-confirm').click(function(event){
   event.preventDefault();
   var url = $(this).attr('href');
    swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover !",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    })
    .then((willDelete) => {
    if (willDelete) {
        window.location.href = url;
        swal("Your quotation has been deleted!", {
        icon: "success",
        });
    } else {
        swal("Your quotation is safe!");
    }
    });

});


});




//remove notification after save data to db
removeNotification();
function removeNotification(){
  setTimeout(() => {
    $('.notification').remove();
  }, 3000);
}


</script>
@endsection
