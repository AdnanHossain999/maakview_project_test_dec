@extends('backend.layouts.app')
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
<style>
    /* ::backdrop {
  background-image: linear-gradient(
    45deg,
    magenta,
    rebeccapurple,
    dodgerblue,
    green
  );
  opacity: 0.75;
} */
</style>

<div class="card">
    
    <div class="card-body">
       
            @csrf

            <div class="card-title">
                <h4 class="text-center m-auto bg-primary text-white" style="text-decoration: underline;">Create Challan Copy</h4>
            </div>

            <div class="m-auto">
                <div class="row">
                    <div class="col-md-12 offset-md-2">
                        <div class="category_search flex-fill p-1 mb-3">
                            <div class="col-md-12 offset-md-2">
                                <div class="col-md-10 mt-2">
                                    <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                        Customer Account name:
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control serch_customer" id="customer_serch_id" placeholder="Search Customer ">
                                    </div>
                                    <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                        Pick a date:
                                    </div>
                                    <div class="col-md-3">
                                        <input type="date" name= "datepicker" id="datepicker" placeholder="Select a date"  class="form-control">
                                    </div>
                                </div>
                             
                             <div id="suggesstion-box_customer" class="search-box_popup send-data-btn" style="padding: 20px;font-size: 15px;font-weight:bold;"></div>

                             
                              
                           
                            
                            </div>
                            
                          
                        </div>
                    </div>

                    <div class="col-md-12 offset-md-1">
                        <div class="product_search flex-fill p-1 mb-3">
                            <div class="col-md-12 offset-md-2">
                                <div class="row mt-2">
                                    <div class="col-md-2 col-form-label pl-4 font-weight-bold" style="font-size: 15px">
                                        Choose Product:
                                    </div>
                                
                                    <div class="col-md-1">
                                        <button class="btn btn-success add-product-btn">+</button>
                                    </div>


                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    
{{-- 
                     <dialog id = "my-modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                    
                    
                                <!-- Modal Body -->
                                <div class="modal-body">
                                    <p>This is the modal body.</p>
                                </div>
                    
                                <!-- Modal Footer -->
                                <div class="modal-footer">
                                    <button autofocus type="button" class="btn btn-danger">Close</button>
                                </div>
                    
                            </div>
                        </div>

                     </dialog>

                     <button type="button" class="btn btn-primary">
                        Open Modal
                    </button> --}}

                   
                </div>
            </div>
            
                





               
             

             
    </div>
    
</div>
    



@endsection

@section('script')
    
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    })
</script>


<script>
  







    //This code is working

                function selectCustomerInfo(id) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('pos.customer_search') }}",
                    data: { 'search': "", 'id': id },
                    success: function (response) {
                        var customerName = response.name;
                        var customerPhone = response.phone;
                        // var customerAddress = response.address;
                        var selectedDate = $('#datepicker').val();

                        // Log data for debugging
                        // console.log('Customer Name:', customerName);
                        // console.log('Customer Phone:', customerPhone);
                        // console.log('Date:', selectedDate);
                        // console.log('Customer Address:', customerAddress);
                    

                        // Move the product data processing inside the success callback
                        var products = gatherProductData();
                        var productsParam = products !== null ? "&Pro=" + encodeURIComponent(JSON.stringify(products)) : "";
                        var routeUrl = "{{ route('demo_challan.view') }}" +
                                        "?customerName=" + encodeURIComponent(customerName) +
                                        "&P=" + encodeURIComponent(customerPhone)  + "&D=" + encodeURIComponent(selectedDate) +
                                        productsParam;

                        // Log the final routeUrl for debugging
                        // console.log('Final routeUrl:', routeUrl);

                        window.open(routeUrl, '_blank');
                    }
                });
            }


            function gatherProductData() {
            // Create an array to store product data for each row
            var products = [];

            // Iterate over each product row
            $('.product-row').each(function () {
                // Capture values from the input fields within the current row
                var productName = $(this).find('.product-name').val();
                var productSerial = $(this).find('.product-serial').val();
                // var productQuantity = $(this).find('.product-quantity').val();

                var productQuantity = (productSerial.match(/,/g) || []).length ;

                $('.product-quantity').val(productQuantity);

                // Create a product object for the current row
                var product = {
                    productName: productName,
                    productSerial: productSerial,
                    quantity: productQuantity
                };

                // Push the product to the array for this row
                products.push(product);
            });

            // Return the array of products
            return products;
        }




                $(document).ready(function () {
                var customerId;

                $(".serch_customer").on('keyup', function () {
            
                        $("#suggesstion-box_customer").html("");
                        var input_customer_value = $(this).val();
                        var search_id_value = $(this).attr('id');
                        if(search_id_value == 'supplier_search_id')
                        {
                            hit_url = "{{route('pos.inventory.supplier_search')}}";
                        }else if(search_id_value == 'customer_serch_id'){
                            hit_url = "{{route('pos.customer_search')}}";
                        }
                        
                        $.ajax({
                            type: "GET",
                            url: hit_url,
                            data: {'search':input_customer_value, 'id':""},
                            success: function (response) {
                                var data = "";
                            var search_result_data_length = response.data.length;
                                for (let i = 0; i < search_result_data_length; i++) {
                                    var customer_name = response.data[i]["name"];
                                var customer_phone = response.data[i]["phone"];
                                var phone_name = customer_name.concat(" (", customer_phone, ")")
            
            
                                
                                    data += '<li onClick="selectCustomerInfo(' + response.data[i]["id"] + ')">' + phone_name + '</li>';
                                    customerId = response.data[i]["id"];
                                    
                                }
                                $("#suggesstion-box_customer").show();
                            if(input_customer_value !== "")
                            {
                                $("#suggesstion-box_customer").html(data);
                            }
                            else
                            {
                                $("#suggesstion-box_customer").html("");
                            }
                            }
                        });
                });

            //     $(".serch_customer").on('keyup', function () {
            //     $("#suggesstion-box_customer").html("");
            //     var input_customer_value = $(this).val();
            //     var search_id_value = $(this).attr('id');
                
            //     if (search_id_value == 'supplier_search_id') {
            //         hit_url = "{{ route('pos.inventory.supplier_search') }}";
            //     } else if (search_id_value == 'customer_serch_id') {
            //         hit_url = "{{ route('pos.customer_search') }}";
            //     }
                
            //     $.ajax({
            //         type: "GET",
            //         url: hit_url,
            //         data: { 'search': input_customer_value, 'id': "" },
            //         success: function (response) {
            //             var data = "";
            //             var search_result_data_length = response.data.length;
                        
            //             for (let i = 0; i < search_result_data_length; i++) {
            //                 var customer_name = response.data[i]["name"];
            //                 var customer_phone = response.data[i]["phone"];
            //                 var customer_address = response.data[i]["address"]; // Include this line to get the address
            //                 var phone_name = customer_name.concat(" (", customer_phone, ")");

            //                 data += '<li onClick="selectCustomerInfo(' + response.data[i]["id"] + ')">' + phone_name + ' - ' + customer_address + '</li>';
            //                 customerId = response.data[i]["id"];
            //             }
                        
            //             $("#suggesstion-box_customer").show();
                        
            //             if (input_customer_value !== "") {
            //                 $("#suggesstion-box_customer").html(data);
            //             } else {
            //                 $("#suggesstion-box_customer").html("");
            //             }
            //         }
            //     });
            // });



    //             $('.send-data-btn').on('click', function () {
    //                 var products = [];

    // //             // Iterate over each product row
    //                 $('.product-row').each(function () {
    //                     // Capture values from the input fields within the current row
    //                     var productName = $(this).find('.product-name').val();
    //                     var productSerial = $(this).find('.product-serial').val();
    //                     var productQuantity = $(this).find('.product-quantity').val();

    //                     // Create a product object for the current row
    //                     var product = {
    //                         productName: productName,
    //                         productSerial: productSerial,
    //                         quantity: productQuantity
    //                     };

    //                     // Push the product to the array for this row
    //                     products.push(product);

    //                     // console.log('Product Data:', products);

                        

    //                 });

    //                 var selectedDate = $('#datepicker').val();
    //                 selectCustomerInfo(customerId,selectedDate);
    //             });

                // ... (unchanged code)

                function addProductField() {
                    var productCounter = 0;
                    var newProductField =
                        '<div class="row mt-2 product-row">' +
                        '<div class="col-md-1">' +
                        '<input type="text" class="form-control serial-number" value="' + productCounter + '" readonly>' +
                        '</div>' +
                        '<div class="col-md-2 offset-md-1 ">' +
                        '<input type="text" class="form-control product-input product-name" placeholder="Product Name">' +
                        '</div>' +
                        '<div class="col-md-2 offset-md-1 ">' +
                        '<input type="text" class="form-control product-input product-serial" placeholder="Product Serial">' +
                        '</div>' +
                        '<div class="col-md-1 offset-md-1 ">' +
                        '<input type="number" class="form-control product-input product-quantity" placeholder="Quantity" disabled>' +
                        '</div>' +
                        '</div>';

                    $('.product_search').append(newProductField);

                    


                        $('.product-serial').on('input', function () {
                        // Get the current value of the input
                        var currentValue = $(this).val();

                        // Remove existing commas from the value
                        var cleanedValue = currentValue.replace(/,/g, '');

                        // Add commas after every 14 characters
                        var formattedValue = cleanedValue.replace(/(.{14})/g, "$1,");

                        // Update the input value with the formatted value
                        $(this).val(formattedValue);
                    });


                // Increment the counter for the next serial number
                    productCounter++;
                }

                // Event handler for the "+" button click
                $('.add-product-btn').on('click', function () {
                    addProductField();
                });
            });

    
// const dialog = document.querySelector("dialog");
// const showButton = document.querySelector("dialog + button");
// const closeButton = document.querySelector("dialog button");

// // "Show the dialog" button opens the dialog modally
// showButton.addEventListener("click", () => {
//   dialog.showModal();
// });

// // "Close" button closes the dialog
// closeButton.addEventListener("click", () => {
//   dialog.close();
// });
 
   





</script>



    
@endsection