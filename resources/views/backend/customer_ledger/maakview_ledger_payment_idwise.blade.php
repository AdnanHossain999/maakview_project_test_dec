@extends('backend.layouts.app')

@section('content')
<section >

    <div class="container">
        <h3 class="text-center bg-primary bg-gradient text-white py-2">Maakview Payment History of {{getUserName($customer_id)}}</h3>
        <table class="table text-center table-bordered">
            <thead>
                <th>Payment Type</th>
                <th>Purchase Voucher</th>
                <th>Bank Name</th>
                <th>Check Number</th>
                <th>Amount of Money</th>
                <th>Date</th>
            </thead>
            <tbody>
                @foreach($customerData as $key => $data)
        
                    <tr>
                        <td>{{ $data->payment_type == 1 ? 'Cash' : 'Bank' }}</td>
                        <td>{{ $data->purchase_transaction->purchase_invoice }}</td>
                        <td>{{ $data->bank_name !== null ? getBankName($data->bank_name) : '' }}</td>
                        <td>{{ $data->check_number !== null ? $data->check_number : '' }}</td>
                        <td>{{$data->payment_value}}</td>
                        <td>{{ $data->payment_date }}</td>
                    </tr>
        
                @endforeach
            </tbody>

        </table>

    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endsection
