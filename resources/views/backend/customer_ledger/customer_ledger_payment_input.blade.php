@extends('backend.layouts.app')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    .shadow_class{
        box-shadow: 0 0 50px #ccc;
    }
</style>

@section('content')
<section >

    <div class="container">
        <h2 class="text-center bg-primary bg-gradient text-white py-2">Customer Payment Input</h2>
       <form action="{{ route('customer_ledger_payment.input') }}" method="POST">
        @csrf
        <div class="mb-3">
          <label for="customer" class="form-label">Select Cutomer</label>
          <input type="text" id="customer" class="form-control" oninput="assign_customer_search(this)" placeholder="search here">
        </div>

        <div>
            <table class="col-12 w-100">
                <tbody class="customer_search_add">

                </tbody>
            </table>
        </div>

        <div>
              <table class="table" id="customer_show_table">
                  <tbody class="customer_search_result shadow_class">
                  </tbody>
              </table>
        </div>

        <div class="mb-3">
          <label for="payment_type" class="form-label">Select Payment Type</label>
          <select class="form-select form-control" name="payment_type" id="payment_type" aria-label="Default select example">
            <option selected>Select Payment Type</option>
            <option value="1">Cash</option>
            <option value="2">Bank</option>
          </select>
        </div>

        @php
        $bankNames = [
            1 => 'AB Bank Limited',
            2 => 'Al-Arafah Islami Bank Limited',
            3 => 'Bangladesh Commerce Bank Limited',
            4 => 'Bangladesh Krishi Bank',
            5 => 'Bank Asia Limited',
            6 => 'Bengal Commercial Bank Limited',
            7 => 'BRAC Bank Limited',
            8 => 'City Bank',
            9 => 'Community Bank Bangladesh Limited',
            10 => 'Dhaka Bank Limited',
            11 => 'Dhaka Mercantile Co-Operative Bank Limited',
            12 => 'Dutch Bangla Bank',
            13 => 'Eastern Bank Limited',
            14 => 'First Security Islami Bank Limited',
            15 => 'Habib Bank Limited',
            16 => 'Islami Bank Bangladesh Limited',
            17 => 'Jamuna Bank Limited',
            18 => 'Janata Bank',
            19 => 'Meghna Bank Limited',
            20 => 'Mercantile Bank Limited',
            21 => 'Midland Bank Limited',
            22 => 'Modhumoti Bank Limited',
            23 => 'Mutual Trust Bank Limited',
            24 => 'National Bank Limited',
            25 => 'National Credit & Commerce Bank Limited',
            26 => 'NRB Bank Limited',
            27 => 'NRB Commercial Bank Limited',
            28 => 'One Bank Limited',
            29 => 'Padma Bank Limited',
            30 => 'Premier Bank Limited',
            31 => 'Prime Bank Limited',
            32 => 'Pubali Bank Limited',
            33 => 'Shahjalal Islami Bank Limited',
            34 => 'Shimanto Bank Limited',
            35 => 'Sonali Bank',
            36 => 'Southeast Bank Limited',
            37 => 'South Bangla Agriculture and Commerce Bank Limited',
            38 => 'Standard Chartered',
            39 => 'United Commercial Bank PLC',
            40 => 'Uttara Bank Limited',
        ];
    @endphp


        <div class="mb-3" id="bankDetails" style="display: none;">
            <label for="bank_details" class="form-label">Select Bank</label>
            <select class="form-select form-control" name="bank_name" id="bankDetails" aria-label="Select Bank">
                <option selected value="">Select Bank</option>
                    @foreach ($bankNames as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
            </select>

            <label for="check_number" class="form-label mt-3">Check Number</label>
            <input type="text" name="check_number" class="form-control" id="check_number">
        </div>

        {{-- voucher_code by Adnan --}}
        <div class="form-group">
            <label for="voucher_code">Select a Voucher Code:</label>
            <select class="form-control" id="voucher_code" name="voucher_code">
                <option value="">Select a Voucher Code</option>
            </select>
        </div>
        {{-- end Adnan --}}

        <label for="amount" class="form-label mt-3">Amount of Money</label>
        <input type="text" name="amount" class="form-control" id="amount">

        <label for="datepicker" class="form-label mt-3">Select a Date:</label>
        <input type="text" id="datepicker" class="form-control" name="datepicker">

        <button type="submit" class="form-control btn btn-primary btn-lg btn-block mt-3">Submit</button>
      </form>
    </div>
</section>

@endsection


@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#payment_type').on('change', function () {
            var selectedValue = $(this).val();

            if (selectedValue === '1') { // Value of "Bank"
                $('#cashDetails').show();
            } else {
                $('#cashDetails').hide();
            }

            if (selectedValue === '2') { // Value of "Bank"
                $('#bankDetails').show();
            } else {
                $('#bankDetails').hide();
            }

        });

        $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy", // Set the date format as needed
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0", // Allow selecting dates up to 100 years in the past and up to the current year
        });


    });


    function assign_customer_search(inputField) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var user_name = inputField.value;
    var name_length = user_name.length;
    console.log("Input: " + name_length);

    if (name_length > 0) {
        $.ajax({
            type: "GET",
            url: "{{route('customer_crm.user_search_purchase')}}",
            data: {
                user_name: user_name,
                name_length: name_length
            },
            success: function (response) {
                // var row = "";
                // $.each(response.data, function (index, value) {
                //     row += `<tr class="tabel_row mb-3" id=row_>
                //             <td class="pt-4"><a href="javascript:void(0)" onclick="customer_add_function(${value.id}, '${value.name}')">${value.name}</a>
                //                 <input type="hidden" class="assign_to_name" value="${value.id}" name="">
                //             </td>
                //         </tr>`;
                // });
                // $(".customer_search_result").html(row);

                                var userId = response.user_id;
                                // Set the user_id in the input field
                                $('#user_id').val(userId);

                                var row = "";
                                $.each(response.data, function (index, value) {
                                    row += `<tr class="tabel_row mb-3" id=row_>
                                        <td class="pt-4"><a href="javascript:void(0)" onclick="customer_add_function(${value.id}, '${value.name}')">${value.name}</a>
                                            <input type="hidden" class="assign_to_name" value="${value.id}" name="">
                                        </td>
                                    </tr>`;
                                });
                                $(".customer_search_result").html(row);

                                // Handle the voucher codes
                                var voucherCodes = response.voucher_codes;
                                var voucherCodeDropdown = $("#voucher_code");
                                voucherCodeDropdown.empty();
                                voucherCodeDropdown.append('<option value="">Select a Voucher Code</option>');

                                $.each(voucherCodes, function (id, purchase_invoice) {
                                    voucherCodeDropdown.append(
                                        $("<option></option>").attr("value", id).text(purchase_invoice)
                                    );
                                });
            }
        });
    } else {
        $(".customer_search_result").html("");
    }
}


function customer_add_function(user_id, user_name){

    
    console.log("Setting user_id: " + user_id);
    $("#user_id").val(user_id);


        user_row = `<tr class="remove_row">
            <td>
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close close_button" data-dismiss="alert" aria-label="close">&times;</a>
                    <input class="form-control check_user_id added_user_id" type="hidden" name="customer" id="${user_id}" value="${user_id}">
                    <strong>${user_name}</strong>
                </div>
            </td>
        </tr>`;

        
    var check = 0;
    var present_id = user_id;
    $('.check_user_id').each(function(e){
        if($(this).val() == present_id){
            check++;
        }
        console.log($(this).val());
    })
    if(check == 0 ){

        $(".customer_search_add").append(user_row);
        $("#customer").addClass("d-none");
        $("#customer_show_table").addClass("d-none");

        $(".close_button").click(function (e) {
            e.preventDefault();
            $("#customer").removeClass("d-none");
            $("#customer_show_table").removeClass("d-none");
            $("#customer").focus();

        });

    }

}

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endsection
