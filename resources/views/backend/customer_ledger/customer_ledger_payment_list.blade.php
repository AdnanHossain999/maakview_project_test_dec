@extends('backend.layouts.app')

@section('content')
<section >
    @if(session('success'))
        <div id="successAlert" class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif


    @if(session('failed'))
        <div id="failedAlert" class="alert alert-danger">
            {{ session('failed') }}
        </div>
    @endif
    <div class="container">
        <h2 class="text-center bg-primary bg-gradient text-white py-2">Customer Ledger Payment List</h2>
        <table class="table text-center table-bordered">
            <thead>
                <th>Customer Name</th>
                <th>Payment Type</th>
                <th>Bank Name</th>
                <th>Check Number</th>
                <th>Amount of Money</th>
                <th>Date</th>
                <th colspan="3">Action</th>
            </thead>
            <tbody>
                @foreach($customerLedgerPaymentData as $key => $LedgerData)
        
                    <tr>
                        <td>{{getUserName($LedgerData->customer_id)}}</td>
                        <td>{{ $LedgerData->payment_type == 1 ? 'Cash' : 'Bank' }}</td>
                        <td>{{ $LedgerData->bank_name !== null ? getBankName($LedgerData->bank_name) : '' }}</td>
                        <td>{{ $LedgerData->check_number !== null ? $LedgerData->check_number : '' }}</td>
                        <td>{{$LedgerData->payment_value}}</td>
                        <td>{{ $LedgerData->payment_date }}</td>
                        <td colspan="3">
                            <form method="POST" action="{{ route('customer_ledger_payment.delete', ['id' => $LedgerData->id]) }}">
                                @csrf
                                @method('DELETE') <!-- Use the DELETE method for RESTful design -->
                                <button type="submit" class="btn btn-danger btn-sm">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </form>
                            
                            <form method="get" action="{{ route('customer_ledger_payment.edit', ['id' => $LedgerData->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa-solid fa-pen-to-square"></i>
                                </button>
                            </form>
                        
                        </td>
                    </tr>
        
                @endforeach
            </tbody>

        </table>

    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">


    setTimeout(function() {
        $("#successAlert").alert('close');
    }, 2000);

    // Function to hide the failed alert after 2 minutes
    setTimeout(function() {
        $("#failedAlert").alert('close');
    }, 2000);



</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endsection
