@extends('backend.layouts.app')

@section('content')
<section >

    <div class="container">
        <h2 class="text-center bg-primary bg-gradient text-white py-2">Maakview Payment History</h2>
        <table class="table text-center table-bordered">
            <thead>
                <th>Customer Name</th>
                <th colspan="3">Action</th>
            </thead>
            <tbody>
                @foreach($paymentHistoryData as $key => $customerId)
        
                    <tr>
                        <td>{{getUserName($customerId)}}</td>
                        <td colspan="3">
                            <a href="{{ route('maakview_ledger_payment.view', ['id' => $customerId]) }}" class="btn btn-sm btn-primary"> <i class="fa-solid fa-eye"></i></a>
                        </td>
                    </tr>
        
                @endforeach
            </tbody>

        </table>

    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endsection
