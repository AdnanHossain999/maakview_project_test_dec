<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        .main-table-data{
            width: 100%;
            margin: 0 auto;

        }
        .table-content{
            width: 100%;
            border-collapse: collapse;
        }
        .table-content th, .table-content td{
            border: 1px solid black;
            text-align: center;
            padding: 10px;
        }
        .table-content tbody tr:hover {
            background-color: #ddd;
        }

        
    @media print{
        body{
            -webkit-print-color-adjust: exact;
        }
        .no-print{
            display: none;
        }
        

    }


            .custom-table {
                width: 100%; 
                /* border-collapse: collapse; */
              
                /* Set the table width to 100% (adjust as needed) */
            }

            .custom-table th, .custom-table td {
                padding: 10px; /* Adjust padding as needed for spacing */
                border: 1px solid #000; /* Add borders for better visualization */
            }



                .custom-table th:nth-child(1),
                .custom-table td:nth-child(1) {
                    /* Style the first (Date) column */
                    width: 15%; /* Adjust the width as needed */
                }

                /* .custom-table th:nth-child(2),
                .custom-table td:nth-child(2) {
                    /* Style the second (Description) column */
                    width: 40%; /* Adjust the width as needed */
                } */

                .custom-table th:nth-child(3),
                .custom-table td:nth-child(3) {
                    /* Style the third (Voucher No) column */
                    width: 15%; /* Adjust the width as needed */
                }

                .custom-table th:nth-child(4),
                .custom-table td:nth-child(4) {
                    /* Style the Debit column */
                    width: 10%; /* Adjust the width as needed */
                }

                .custom-table th:nth-child(5),
                .custom-table td:nth-child(5) {
                    /* Style the Credit column */
                    width: 10%; /* Adjust the width as needed */
                }

                .custom-table th:nth-child(6),
                .custom-table td:nth-child(6) {
                    /* Style the last (Balance) column */
                    width: 13%; /* Adjust the width as needed */
                }
                            
                 
           

               
            

            .first-col {
                border-left: 2px solid #000; /* Remove the left border */
            }
            
            .last-col {
                border-right: 2px solid #000; /* Remove the right border */
            }


            /* datepicker style */

            .datepicker-form {
                display: block; /* Display it by default */
            }

            @media print {
                .datepicker-form {
                    display: none; /* Hide it when printing */
                }
            }

    </style>
</head>

<body>
    <div style="text-align:center">
        <button class="btn btn-success no-print mt-1 " onclick="printFun()"><i class="fa fa-print" aria-hidden="true"></i> Print</button> 
    </div>

 

    <div id="datepickerFormContainer" class="datepicker-form">
        <form method="GET" action="{{ route('customer_ledger_sales_history_report_datepicker', ['id' => $id]) }}">
            <div style="text-align: center;margin-top:30px;margin-bottom:30px;">
                <input type="date" id="datepickerFrom" name="start_date" placeholder="From Date">
                <input type="date" id="datepickerTo" name="end_date" placeholder="To Date">
                <button type="submit" id="">Apply Filter</button>
            </div>
        </form>
    </div>
    
    
    <header>
        <div class="maak-title">
            
                
       
            
            <div style="text-align: center">
                <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png" width="200" height="60" alt="Logo"> 
                <h1 style="font-size: 25px; font-weight:bold; margin-bottom:5px">Customer Ledger</h1>
                {{-- <h1 style="font-size: 50px; font-weight:bold; margin-bottom:5px">Maakview Limited</h1> --}}
                <h2 style="font-size: 20px; font-weight:bold; margin-bottom:10px">Customer Name: {{ $customer_name }}</h2>
                <p>Rahima Plaza(6th Floor),82/3 Laboratory Road, Dhaka-1205Phone: +8801888-012727,+880244613763</p>
            </div>
        </div>
        <div class="customer-title" style="text-align: center">
            @if (!empty($allRecords))
                @php
                    $firstRecord = reset($allRecords); // Get the first element
                    $lastRecord = end($allRecords);    // Get the last element
                @endphp
                <p style="font-weight: bold;">Date From: {{ date("d-m-Y", strtotime($firstRecord['date'])) }} To {{ date("d-m-Y", strtotime($lastRecord['date'])) }} </p>
                {{-- <p>Date To: {{ date("d-m-Y", strtotime($lastRecord['date'])) }}</p> --}}
            @endif
        </div>
    </header>

    <main>
        <div style="display: flex;justify-content: center; align-items:end;" >


            <table class="table-content custom-table">
                <thead style="border: 2px solid #000;font-size: 1.2rem;">
                    <tr>
                        <th class="first-col">Date</th>
                        <th>Description</th>
                        <th style="font-size: 1rem;">Voucher No</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th class="last-col">Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $balance = 0.00;
                    @endphp

                        <tr>
                            <td style="border-left: 2px solid #000;">{{$customer_date->format('d-m-Y')}}</td>
                            <td style="text-align: left;">
                            Opening
                            </td>
                            <td></td>
                            <td>0.00</td>
                            <td>0.00</td>
                            <td style="border-right: 2px solid #000;">0.00</td>
                        </tr>
            
                    @foreach ($allRecords as $record)
                        @php
                        $date = $record['date']->format('d-m-Y');
                        $voucher = $record['voucher'];
                        $amount = $record['amount'];
                        $type = $record['type'];
                        $description = $record['description'];
            
                        if ($type === 'debit') {
                            $debitSum += $amount ;
                        } else {
                            $creditSum += $amount ;
                        }
            
                        $balance += ($type === 'debit') ? $amount : -$amount;
                        @endphp

                      
            
                        <tr>
                            <td style="border-left: 2px solid #000;" >{{ $date }}</td>
                            <td style="text-align: left;">
                               
                                    {{ $description }}
                               
                                
                            </td>
                            <td>{{ $voucher }}</td>
                            <td>{{ $type === 'debit' ? number_format($amount,2) : '0.00' }}</td>
                            <td>{{ $type === 'credit' ? number_format($amount,2): '0.00' }}</td>
                            <td style="border-right: 2px solid #000;">{{ number_format($balance,2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr style="border: 2px solid #000;font-size: 1.2rem;">
                        <td colspan="3" style="font-weight: bold; text-align: center;">Total</td>
                        <td><strong>{{ number_format($debitSum, 2)}}</strong></td>
                        <td><strong>{{ number_format($creditSum, 2) }}</strong></td>
                        <td><strong>{{ number_format($debitSum - $creditSum, 2) }}</strong></td>
                    </tr>
                </tfoot>
            </table>

        </div>


        


        <footer class="footer" style="  width: 100%;text-align: center;">
            <div class="div" style="display: flex; justify-content:space-between">
                <div class="mb-2" style="margin-left: 0; margin-top:100px" >
                    <ul style="list-style-type:none; margin:0;padding-left:0">
                        <li>.....................................................</li>
                        <li style="font-weight: bold; padding-left:12px;font-size:20px">Customer Signature</li>
                    </ul>
                </div>
                <div class="mb-2" style="margin-left: 0; margin-top:100px" >
                    <ul style="list-style-type:none; margin:0;padding-left:0">
                        <li>........................................................</li>
                        <li style="font-weight: bold; padding-left:12px;font-size:20px">Authorized Signature</li>
                    </ul>
                </div>
            </div>
           
    
            <div class="print-date" style="display: flex;justify-content:end;">
    
                <span>Date: {{ date('Y-m-d h:i A') }}</span>
                
            </div>
    
        </footer>
          
            
            


        
            
            
            
            

    </main>

    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">
        try {
            
            if( $(document).height() > 1100 ){
                if( $(".check_img").attr("id") == 'unpaid'){
                    $('.footer').css('position','fixed');
                    $('.footer').css('bottom','0');
                    $('.footer').css('clear','both');
                }else{
                    $('.footer').css('position','inherit');
                    $('.footer').css('padding-top','50px');
                    $('.footer').css('clear','both');
                }
            }else{
                $('.footer').css('position','fixed');
                $('.footer').css('bottom','0');
                $('.footer').css('clear','both');
            }

            // this.print();

            this.printFun = () => {
            window.print();
            }
            
    
        } catch (e) {
            window.onload = window.print;
        }
        window.onbeforeprint = function() {
            setTimeout(function() {
                window.close();
            }, 1500);
        }

    </script>


    {{-- datepicker script --}}

    {{-- <script>
        $(function () {
            // Initialize date pickers
            $("#datepickerFrom").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#datepickerTo").datepicker({ dateFormat: 'yy-mm-dd' });
    
            // Handle the filter button click
            $("#filterButton").click(function () {
                var fromDate = $("#datepickerFrom").val();
                var toDate = $("#datepickerTo").val();
    
                // Redirect to a URL that passes the selected dates as query parameters
                window.location.href = "{{ route('customer_ledger_sales_history_report_datepicker.view') }}?from=" + fromDate + "&to=" + toDate;
            });
        });
    </script> --}}

    {{-- end datepicker script --}}
</body>
</html>