@extends('backend.layouts.app')

@section('content')
<section >

    <div class="container">
        <h2 class="text-center bg-primary bg-gradient text-white py-2">Customer Payment History</h2>
        @if (session()->has('status'))
            <div class=" notification alert alert-danger col-md-12">
                {{ session('status') }}
            </div>
        @endif
        <table class="table text-center table-bordered">
            <thead>
                <th>Customer Name</th>
                <th colspan="3">Action</th>
            </thead>
            <tbody>
                @foreach($customerPaymentHistoryData as $key => $customerId)
        
                    <tr>
                        <td>{{getUserName($customerId)}}</td>
                        <td colspan="3">
                            <a href="{{ route('customer_payment.view', ['id' => $customerId]) }}" class="btn btn-sm btn-primary"> <i class="fa-solid fa-eye"></i></a>
                        </td>
                    </tr>
        
                @endforeach
            </tbody>

        </table>

    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">

    $(document).ready(function () {
        var $notification = $('.notification');
  
        setTimeout(function() {
        $notification.fadeOut('slow', function() {
            $notification.remove();
        });
        }, 2000); 
    });

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endsection
