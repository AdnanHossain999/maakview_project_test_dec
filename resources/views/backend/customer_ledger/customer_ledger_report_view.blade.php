<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
    <style>
        .main-table-data{
            width: 80%;
            margin: 0 auto;

        }
        .table-content{
            width: 100%;
            border-collapse: collapse;
        }
        .table-content th, .table-content td{
            border: 1px solid black;
            text-align: center;
            padding: 10px;
        }
        .table-content tbody tr:hover {
            background-color: #ddd;
        }

        
    @media print{
        body{
            -webkit-print-color-adjust: exact;
        }
        .no-print{
            display: none;
        }
        

    }
    </style>
</head>
<body>
<div style="text-align:center">
    <button class="btn btn-success no-print mt-1 " onclick="printFun()"><i class="fa fa-print" aria-hidden="true"></i> Print</button> 
</div>

<header>
    <div class="maak-title">
        <div style="text-align: center">
            <img class="m-0 p-0" src="{{asset('logo')}}/maakview.png" width="200" height="60" alt="Logo"> 
            <p style="font-size: 20px; font-weight:bold; margin-bottom:0px">Maakview Limited</p>
            <p>Rahima Plaza(6th Floor),82/3 Laboratory Road, Dhaka-1205Phone: +8801888-012727, +8801886-531777, +880244613763</p>
        </div>
    </div>
    <div class="customer-title" style="text-align: center">
        <p style="font-size: 20px; font-weight:bold; margin-bottom:0px">{{$get_customer_name->name}}</p>
        <p  style=" margin-bottom:0px">{{$get_customer_address->address}}, Bangladesh</p>
        <p>Date From:  </p>
    </div>
</header>

<main>
    <div class="main-table-data" >
        <table class="table-content">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Particulars</th>
                    
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Balanace</th>
                </tr>
            </thead>
            <tbody>
                @php
                    //debit -> comapny cells to customer + company payment to customer
                    // $debit_values = ((double)$customer_total_sale_history->total_grand_total - (double)$customer_total_sale_history->total_special_discount ) + (double) $maakview_payment_to_customer->total_maakview_payment_value;

                                $debit_values = ((double)$customer_total_sale_history->total_grand_total - (double)$customer_total_sale_history->total_special_discount );
                                
                                // - (double)$customer_total_payment->total_payment_value;
                             

                    // (double) $maakview_payment_to_customer->total_maakview_payment_value
                    //credit -> customer cells to company(purchase) + customer payment to company
                    // $credit_values = (double)$get_total_purchase_amount_for_this_customer->total_purchase_amount + (double)$customer_total_payment->total_payment_value;


                                $credit_values = (double)$get_total_purchase_amount_for_this_customer->total_purchase_amount;

                                // - (double) $maakview_payment_to_customer->total_maakview_payment_value ;

                                $total_balance = $debit_values - $credit_values ;
                @endphp



                <tr>
                    {{-- <td>{{date('d-M-Y', strtotime('now'))}}</td> --}}

                    
                    <td>{{$user_create_data->created_at->format('d-m-Y') }}</td>
                   

                    <td>Opening Balance</td>
                   
                    <td>{{$debit_values}}</td>
                    
                    <td>{{$credit_values}}</td>
                 
                    <td>{{$total_balance}}</td>
                </tr>
            </tbody>
        </table>
        <div class="summary" style="margin-top:40px; ">
            <h3 style="text-align:center;text-decoration:underline">Ledger Summary</h3>
            <div class="sales-purchase-summary" style="display: flex;justify-content:space-between">
                <div class="sales">
                    <p style="font-size: 20px; padding:10px; font-weight:bold">Sales</p>
                    <p style="line-height: 1;">Current Month Sale:{{(double)$current_month_total_sale->total_grand_total - (double)$current_month_total_sale->total_special_discount}}</p>
                    <p style="line-height: 1;">Last Month Sale:{{(double) $last_month_total_sale->total_grand_total - (double) $last_month_total_sale->total_special_discount}}</p>
                    <p style="line-height: 1;">Total Sale:{{(double)$customer_total_sale_history->total_grand_total - (double)$customer_total_sale_history->total_special_discount }}</p>
                </div>
                <div class="purchase">
                    <p style="font-size: 20px; padding:10px; font-weight:bold">Purchase</p>
                    <p style="line-height: 1;">Current Month Purchase: {{$current_month_total_purchase->total_purchase_amount}}</p>
                    <p style="line-height: 1;">Last Month Purchase: {{$last_month_total_purchase->total_purchase_amount}}</p>
                    <p style="line-height: 1;">Total Purchase:{{(double)$get_total_purchase_amount_for_this_customer->total_purchase_amount}}</p>
                </div>
            </div>
        </div>
        <div class="div" style="display: flex; justify-content:space-between">
            <div class="mb-2" style="margin-left: 0; margin-top:400px" >
                <ul style="list-style-type:none; margin:0;padding-left:0">
                    <li>........................................</li>
                    <li>Customer Signature </li>
                </ul>
            </div>
            <div class="mb-2" style="margin-left: 0; margin-top:400px" >
                <ul style="list-style-type:none; margin:0;padding-left:0">
                    <li>........................................</li>
                    <li>Authorized Signature </li>
                </ul>
            </div>
        </div>
    </div>
    
</main>

    




    











    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="text/javascript">
     let printFun = () => {
            window.print();
            
        }

    </script>
</body>
</html>