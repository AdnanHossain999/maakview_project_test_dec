@extends('backend.layouts.app')

@section('content')
<section >

    <div class="container">
        <h3 class="text-center bg-primary bg-gradient text-white py-2">Payment History of {{getUserName($customer_id)}}</h3>
        <table class="table text-center table-bordered">
            <thead>
                <th>Date</th>
                <th>Payment Type</th>
                <th>Sales Voucher</th>
                <th>Bank Name</th>
                <th>Check Number</th>
                <th>Amount of Money</th>
                
            </thead>
            <tbody>
                @foreach($customerData as $key => $data)
        
                    <tr>
                        <td>{{ $data->payment_date }}</td>
                        <td>{{ $data->payment_type == 1 ? 'Cash' : 'Bank' }}</td>
                        
                         
                         <td>{{ $data->combinedOrder->code }}</td>

                         {{-- @foreach($data->combinedOrders as $combinedOrder)
                            <td>{{ $combinedOrder->code }}</td>
                        @endforeach --}}
                       
                        
                        <td>{{ $data->bank_name !== null ? getBankName($data->bank_name) : '' }}</td>
                        <td>{{ $data->check_number !== null ? $data->check_number : '' }}</td>
                        <td>{{$data->payment_value}}</td>
                       
                    </tr>
        
                @endforeach
            </tbody>

        </table>

    </div>
</section>

@endsection


@section('script')
<script type="text/javascript">

</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endsection
