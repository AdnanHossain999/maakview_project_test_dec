<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Sheet</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f8f9fa;
            /* margin: 20px; */
        }

        .container {
           max-width: 800px;
           margin: 0 auto;
        }

        .select-container {
            margin-top: 20px;
        }

        .select-container select {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ced4da;
            border-radius: 4px;
            box-sizing: border-box;
        }

        #submit_button {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            background-color: #6610f2;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        #submit_button:hover {
        background-color: #6a4fe1;
        }

        .no-print button {
            width: 10%;
            padding: 10px;
            margin-bottom: 20px;
            background-color: rgba(49, 231, 3, 0.747);
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        /* h1 {
        font-size: 24px;
        } */

        .bg-primary {
            background-color: #6610f2;
            color: #fff;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 20px;
        }

        .text-center {
        text-align: center;
        }

        .text-white {
        color: #fff;
        }

        .row {
        display: flex;
        justify-content: center;       
        flex-wrap: wrap;
        }

        .col-md-4 {
        flex-basis: calc(33.333% - 10px);
        margin-bottom: 10px;
        }
  
        .col-md-2 {
        flex-basis: calc(16.666% - 10px);
        margin-bottom: 10px;
        }
        
    
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
            
        }
    
        th, td {
            border: 1px solid #000000;
            padding: 2px;
            text-align: center;
           
        }
    
        th {
            background-color: #f2f2f2;
            font-size: 16px;
        }
    
        .total-row {
            font-weight: bold;
         
            font-size: 18px;
        }

        table tr, td:last-child {
            padding: 10px;
        }

        .footer {
            display: none;
        }

</style>

 <style media="print">

    .no-print {
        display: none; 
    } 

    .print-footer{
        display : block;
    }
   
</style> 
</head>
<body>
    
    <form class="no-print" action="{{ route('employee.employee_salary_sheet') }}" method="POST">

        @csrf

        <div class="no-print; col-md-2;" style="display: flex;justify-content:center;">
            <button type="button" class="btn btn-primary" onclick="window.print()">Print</button>
        </div>

        <div class="container">
           <h1 class="bg-primary text-center text-white no-print">SELECT MONTH AND YEAR FOR SALARY SHEET</h1>
        </div>

        <div class="row">
            <div class="col-md-6 select-container" style=" margin-right: 20px;">
                <select id="month" class="form-control" name="month">
                    <option value="">Select a month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">Jcontainer
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
            </div>
            <div class="col-md-6 select-container" style="margin-right: 20px;">
                <select id="year" class="form-control" name="year">
                    <option value="">Select a Year</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                    <option value="2029">2029</option>
                    <option value="2030">2030</option>
  
                  </select>
            </div>
            <div class="col-md-1" style="display:flex; align-items: center; margin-top:23px;" >
                <input id="submit_button" type="submit" class="btn btn-primary">
            </div>
        </div>

    </form>
   

    <div>
        <div style="display: flex; justify-content: center;">
            <a href="{{ route('admin.dashboard') }}">
                <img src="{{ asset('logo/maakview.png') }}" width="200" height="50" alt="Logo">
            </a>        
        </div>
        <h1 class="" style="margin-top:20px; display: flex; justify-content: center;">
            Salary Sheet
            @if(isset($selectedMonth) && isset($selectedYear))
                Month Of {{ date('F', mktime(0, 0, 0, $selectedMonth, 1)) }} {{ $selectedYear }}
            @endif
        </h1>

    </div>

        

    <div>      
        <table>
                <thead style="vertical-align: top;">
                    <tr>
                        <th>S.N.</th>
                        <th >Name</th>
                        <th>Designation</th>
                        <th>Joining Date</th>
                        <th>Employee Status</th>
                        <th>Delay</th>
                        <th>Casual Leave</th>                       
                        {{-- <th>Employee Salary</th> --}}
                        <th>Probation Period Salary</th>
                        <th>Basic Salary TS. 65%</th>
                        <th>House Rent Allow. TS. 15%</th>
                        <th>Medical Allow. TS. 10%</th>
                        <th>Transport Allow. TS. 10%</th>
                        <th>Festival Allowance</th>
                        <th>Advance Payment</th>
                        <th>Deduction Amount</th>
                        <th>After Advance & Deduction Salary(Net Pay)</th>
                        <th>Total Salary</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $totalEmployeeSalary = $totalProbationSalary = $totalFestivalAllowance = $totalAdvancePayment = $totalDeduction = $totalNetPay = $totalTotalSalary = 0;
                @endphp
                    @foreach($employees as $key => $employee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td style="text-align: left; white-space: nowrap; margin: 2px;">{{ $employee->name }}</td>
                            <td>{{ $employee->designation }}</td>
                            <td>{{ \Carbon\Carbon::parse($employee->employe_date_of_joining)->format('d-m-Y') }}</td>
                            <td>{{ $employee->employee_status}}</td>
                           

                            <td>{{ $employee->automateAttendances->pluck('total_un_approve_dellay')->sum() }}</td>
                          
                            <td>{{  $employee->automateAttendances->pluck('total_approve_leave')->sum() }}</td>             
                            {{-- <td>{{ $employee->employee_status === 'permanent' ? $employee->employee_salary : '0' }}</td> --}}
                            <td>{{ in_array($employee->employee_status, ['probation', 'intern']) ? $employee->employee_salary : '0' }}</td>
                            <td>{{ $employee->employee_status === 'permanent' ? $employee->employee_salary * 0.65 : '0' }}</td>
                            <td>{{ $employee->employee_status === 'permanent' ? $employee->employee_salary * 0.15 : '0' }}</td>
                            <td>{{ $employee->employee_status === 'permanent' ? $employee->employee_salary * 0.10 : '0' }}</td>
                            <td>{{ $employee->employee_status === 'permanent' ? $employee->employee_salary * 0.10 : '0' }}</td>
                            
                            <td>{{ $employee->automateAttendances->first()->payslip->festibal_allowance ?? '0' }}</td>
                            <td>{{ $employee->automateAttendances->first()->payslip->advance_adjust ?? '0' }}</td>
                            <td>{{ $employee->automateAttendances->first()->payslip->total_deduction ?? '0' }}</td>
                            <td>{{ number_format($employee->automateAttendances->first()->payslip->total_net_pay ?? 0, 2) }}</td>
                
                            
                            <td>
                                @php
                                    $permanentSalary = $employee->employee_status === 'permanent' ? $employee->employee_salary : 0;
                                @endphp
                            
                                {{ $permanentSalary ? number_format($permanentSalary, 2) : '' }}
                            
                                @if (in_array($employee->employee_status, ['probation', 'intern']))
                                    {{ $employee->employee_salary ? number_format($employee->employee_salary, 2) : '' }}
                                @endif
                            </td>
                         </tr>
                         
                         @php
                                $totalEmployeeSalary += $employee->employee_status === 'permanent' ? $employee->employee_salary : 0;
                                $totalProbationSalary += in_array($employee->employee_status, ['probation', 'intern']) ? $employee->employee_salary : 0;
                                $totalFestivalAllowance += $employee->automateAttendances->first()->payslip->festibal_allowance ?? 0;
                                $totalAdvancePayment += $employee->automateAttendances->first()->payslip->advance_adjust ?? 0;
                                $totalDeduction += $employee->automateAttendances->first()->payslip->total_deduction ?? 0;
                                $totalNetPay += $employee->automateAttendances->first()->payslip->total_net_pay ?? 0;
                                $totalTotalSalary += $employee->employee_status === 'permanent' ? $employee->employee_salary : (in_array($employee->employee_status, ['probation', 'intern']) ? $employee->employee_salary : 0);
                            @endphp
                    @endforeach
                    <tr class="total-row">
                        <td colspan="7">Total</td>
                        {{-- <td>{{ $totalEmployeeSalary }}</td> --}}
                        <td>{{ $totalProbationSalary}}</td>
                        <td>{{ $totalEmployeeSalary * 0.65 }}</td>
                        <td>{{ $totalEmployeeSalary * 0.15 }}</td>
                        <td>{{ $totalEmployeeSalary * 0.10 }}</td>
                        <td>{{ $totalEmployeeSalary * 0.10 }}</td>
                        {{-- <td></td>
                        <td></td>  --}}
                        <td>{{ $totalFestivalAllowance }}</td>
                        <td>{{ $totalAdvancePayment }}</td>
                        <td>{{ $totalDeduction }}</td>
                        <td>{{ number_format($totalNetPay, 2) }}</td>
                        <td>{{ number_format($totalEmployeeSalary + $totalProbationSalary, 2) }}</td>
                    </tr>
                </tbody>
        </table>

    </div>



    <footer class="footer print-footer" style=" width: 100%;text-align: center;">
        <div  style="margin-bottom: 0;margin-top:40px; padding-bottom:0; display:flex; justify-content:space-between;">
            <ul  style="margin-left: 0px;padding-left: 5px; list-style-type:none;">
                <li>..........................................</li>
                <li>Prepared By</li>
                <li style="margin-top: 10px">Date: {{ date('d-m-y h:i A') }}</li>
            </ul>
            <ul style="margin-center: 0px;padding-left: 5px; list-style-type:none;">
                <li>..........................................</li>
                <li>Accounts Officer</li>                
            </ul>
           
                <ul style="list-style-type:none; margin-right:0px; padding-left:5px;">
                    <li>.....................................</li>
                    <li>Approved by C.E.O</li>
                </ul>            
        </div>
             
    </footer>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    



    <script type="text/javascript">
        try {
            
            if( $(document).height() > 1100 ){
                $('.footer').css('position','inherit');
                $('.footer').css('padding-top','50px');
                $('.footer').css('clear','both');
            }else{
                $('.footer').css('position','fixed');
                $('.footer').css('bottom','0');
                $('.footer').css('clear','both');
            }

        
            
    
        } catch (e) {
            window.onload = window.print;
        }
        window.onbeforeprint = function() {
            setTimeout(function() {
                window.close();
            }, 1500);
        }

    </script>

</body>
</html> 